package br.com.geppetto.fdl.desktop;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import br.com.geppetto.fdl.FDLMain;

public class DesktopLauncher {
	private static final boolean REBUILD_ATLAS = false;
	
	private static final boolean REBUILD_WORDS = false;
	
	private static void delete(String name) {
		File file = new File("/assetsToFDL/images");
		File pack = new File(file, name + ".pack");
		File png = new File(file, name + ".png");
		pack.delete(); png.delete();
	}
	
	public static void main (String[] arg) {
		if(REBUILD_ATLAS) {
			Settings settings = new Settings();
			settings.pot = true;
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.paddingX = 2;
			settings.paddingY = 2;
			settings.debug = false;
			
			delete("nearest");
			delete("linear");
			delete("storyboard");
			
			settings.filterMag = TextureFilter.Nearest;
			settings.filterMin = TextureFilter.Nearest;
			
			TexturePacker.process(settings, "assets-raw/images/nearest",
					"/assetsToFDL/images", "nearest.pack");
			
			TexturePacker.process(settings, "assets-raw/images/storyboard", 
					"/assetsToFDL/images", "storyboard.pack");
			
			settings.filterMag = TextureFilter.Linear;
			settings.filterMin = TextureFilter.Linear;
			
			TexturePacker.process(settings, "assets-raw/images/linear",
					"/assetsToFDL/images", "linear.pack");
			
			System.out.print("SPRITE SHEET DONE!");
		}
		if(REBUILD_WORDS) {
			try {
				{
					File stagesDir = new File("/assetsToFDL/mp3/stages");
					String[] stages = stagesDir.list();
					File stagesFile = new File("/assetsToFDL/mp3/stages.txt");
					PrintWriter pw = new PrintWriter(stagesFile);
					boolean first = true;
					for(String stage : stages) {
						if(first) {
							pw.print(stage);
							first = false;
						} else {
							pw.print("\n" + stage);
						}
					}
					pw.flush();
					pw.close();
				}
				
				
				File wordsDir = new File("/assetsToFDL/mp3/words");
				String[] words = wordsDir.list();
				File wordsFile = new File("/assetsToFDL/mp3/words.txt");
				wordsFile.createNewFile();
				PrintWriter pw = new PrintWriter(wordsFile);
				boolean first = true;
				for(String filename : words) {
					String name = filename.substring(0, filename.length() - 4);
					
					// I need this because android doesn't accept special chars
					String regex = ".*(\\w\\d).*";
					while(name.matches(regex)) {
						String special = name.replaceAll(regex, "$1");
						int indexOfSpecial = name.indexOf(special);
						
						switch(special) {
							case "A1":
								special = "Á";
								break;
							case "A2":
								special = "Â";
								break;
							case "A3":
								special = "Ã";
								break;
							case "a1":
								special = "á";
								break;
							case "a2":
								special = "â";
								break;
							case "a3":
								special = "ã";
								break;
							case "E1":
								special = "É";
								break;
							case "E2":
								special = "Ê";
								break;
							case "e1":
								special = "é";
								break;
							case "e2":
								special = "ê";
								break;
							case "I1":
								special = "Í";
								break;
							case "i1":
								special = "í";
								break;
							case "O1":
								special = "Ó";
								break;
							case "O2":
								special = "Ô";
								break;
							case "O3":
								special = "Õ";
								break;
							case "o1":
								special = "ó";
								break;
							case "o2":
								special = "ô";
								break;
							case "o3":
								special = "õ";
								break;
							case "U1":
								special = "Ú";
								break;	
							case "u1":
								special = "ú";
								break;
							case "C1":
								special = "Ç";
								break;	
							case "c1":
								special = "ç";
								break;
						}
						name = name.substring(0, indexOfSpecial) + special 
								+ name.substring(indexOfSpecial + 2, name.length());
					}
					/////////////////////////
					if(first) {
						pw.print(filename + ";" + name);
						first = false;
					} else {
						pw.print("\n" + filename + ";" + name);
					}
				}
				pw.flush();
				pw.close();
				
				System.out.print("WORDS DONE!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(REBUILD_ATLAS || REBUILD_WORDS) return;
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Fila das Letras";
		//config.width = 339;
		//config.height = 508;
		config.width = 450;
		config.height = 625;
		new LwjglApplication(new FDLMain(), config);
	}
}
