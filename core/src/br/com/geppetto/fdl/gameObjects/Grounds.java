package br.com.geppetto.fdl.gameObjects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.updatable.Updatable;

public class Grounds implements Initializable, Renderable, Updatable{
	private StageFactory factory;
	
	private List<Sprite> grounds;
	
	public Grounds(StageFactory factory) {
		this.factory = factory;
	}
	
	@Override
	public void init() {
		int lines = MathUtils.ceil(factory.getStageWorld().length / 4f) + 1;
		int cols = MathUtils.ceil(factory.getStageWorld()[0].length / 4f) + 1;
		grounds = new ArrayList<>();
		for(int line = 0; line <= lines; line++) {
			for(int col = 0; col <= cols; col++) {
				Sprite ground = factory.buildGround();
				ground.setX(col * StageFactory.GROUND_SPACE.x * 4f - StageFactory.GROUND_SPACE.x - StageFactory.GROUND_OFFSET.x);
				ground.setY(line * StageFactory.GROUND_SPACE.y * 4f - StageFactory.GROUND_SPACE.y - StageFactory.GROUND_OFFSET.y);
				grounds.add(ground);
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		for(Sprite g : grounds)
			g.render(batch);
	}

	@Override
	public void update(float delta) {
		factory.updateGroundDH(delta);
	}
}
