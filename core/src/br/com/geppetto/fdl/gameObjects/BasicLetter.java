package br.com.geppetto.fdl.gameObjects;

import java.util.Arrays;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.fdl.gameObjects.avatar.AvatarHelper;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class BasicLetter implements Initializable, Updatable, Renderable, DebugRenderable {
	private CollidableSprite child;
	private final Color color = new Color(.75f, 1f, .75f, 1f);
	
	private float velocity;
	private AvatarHelper helper;
	private Label letter;
	private Direction direction;
	private int avatarCode;
	
	private float alpha;
	
	private final Updatable normalBehavior = new Updatable() {
		@Override
		public void update(float delta) {
			//boolean updateNormally = true;
			child.addPositionX(direction.x * delta * velocity);
			child.addPositionY(direction.y * delta * velocity);
			arrangeLetter();
			helper.update(delta);
			
			alpha = Math.min(alpha + delta, 1f);
			float s = 2f - alpha;
			child.setImgScale(s, s);
		}
	};
	
	private Updatable curr = normalBehavior;
	
	public BasicLetter(String letter, float velocity, int avatarCode) {
		this.letter = buildLetter(letter);
		this.velocity = velocity;
		this.avatarCode = avatarCode;
	}
	
	public BasicLetter(int avatarCode) {
		this("F", 0f, avatarCode);
	}
	
	private Label buildLetter(String letter) {
		return new Label(Assets.INSTANCE.fonts.buttonBig, 
				letter, 0f, 0f, 3f);
	}
	
	public void setLetter(String letter) {
		this.letter.setText(letter);
	}
	
	public String getLetter() {
		return letter.getText();
	}
	
	@Override
	public void init() {
		child = Factory.INSTANCE.buildAvatarScope();
		helper = new AvatarHelper(avatarCode, child);
		helper.init();
		direction = Direction.NORTH;
		setDirection(Direction.values()[MathUtils.random(Direction.values().length - 1)]);
		alpha = 0f;
		letter.setColor(color.r, color.g, color.b, color.a);
	}
	
	public void setDiffRandomDirection() {
		setDirection(direction.plus(MathUtils.random(1, Direction.values().length - 1)));
	}
	
	public void setDirection(Direction direction) {
		helper.updateDirection(this.direction, direction);
		this.direction = direction;
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	private void arrangeLetter() {
		letter.setPosition(child.getImgCenterX() - letter.getWidth() / 2f,
				child.getImgY() + (child.getImgHeight() * .75f));
	}
	
	public float getVelocity() {
		return velocity;
	}
	
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
	@Override
	public void update(float delta) {
		curr.update(delta);
	}
	
	public float getX() {
		return child.getX();
	}

	public float getY() {
		return child.getY();
	}
	
	@Override
	public void render(SpriteBatch batch) {
		//letter.render(batch);
		batch.setColor(1f, 1f, 1f, alpha);
		child.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
		letter.render(batch);
	}
	
	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		child.renderDebug(shapeRenderer);
	}

	public float getWidth() {
		return child.getWidth();
	}

	public float getHeight() {
		return child.getHeight();
	}
	
	public void oposity() {
		setDirection(direction.oposite());
	}
	
	public void updatePosition(float x, float y) {
		child.updatePositionX(x);
		child.updatePositionY(y);
		arrangeLetter();
	}
	
	public void updatePositionX(float x) {
		child.updatePositionX(x);
		arrangeLetter();
	}
	
	public void updatePositionY(float y) {
		child.updatePositionY(y);
		arrangeLetter();
	}
	
	public boolean collided(CollidableSprite c) {
		return child.collided(c);
	}
	
	public boolean isContained(Rectangle rectangle) {
		return child.isContained(rectangle);
	}
	
	public void onElimination(final Runnable runnable) {
		curr = new Updatable() {
			@Override
			public void update(float delta) {
				alpha = Math.max(alpha - delta, 0f);
				letter.setColor(1f, 1f, 1f, alpha);
				if(alpha <= 0f) {
					curr = normalBehavior;
					runnable.run();
				}
			}
		};
	}
	
	public CollidableSprite getCollidableLetter() {
		return child;
	}
	
	public int getAvatarCode() {
		return avatarCode;
	}
}
