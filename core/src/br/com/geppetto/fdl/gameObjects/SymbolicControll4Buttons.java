package br.com.geppetto.fdl.gameObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.Sprite;


public class SymbolicControll4Buttons extends SymbolicControll {
	private Sprite up;
	private Sprite down;
	
	private float upTime;
	private float downTime;
	@Override
	public void init() {
		super.init();
		up = Factory.INSTANCE.buildVerArrow(false);
		down = Factory.INSTANCE.buildVerArrow(true);
		float centerX = Constants.VIEWPORT_WIDTH / 2f;
		down.setCenterX(centerX);
		down.setY(0f);
		left.setY(down.getHeight() - left.getHeight() / 2f);
		right.setY(down.getHeight() - left.getHeight() / 2f);
		up.setCenterX(centerX);
		up.setY(down.getHeight());
	}
	
	@Override
	public void render(SpriteBatch batch) {
		prepareForButton(batch, upTime);
		up.render(batch);
		prepareForButton(batch, downTime);
		down.render(batch);
		super.render(batch);
	}
	
	@Override
	public void pressUp() {
		upTime = MAX_TIME;
	}
	
	@Override
	public void pressDown() {
		downTime = MAX_TIME;
	}
	
	@Override
	public boolean clickedOnUp(float x, float y) {
		return up.isInBounds(x, y);
	}
	
	@Override
	public boolean clickedOnDown(float x, float y) {
		return down.isInBounds(x, y);
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		upTime = Math.max(upTime - delta, 0f);
		downTime = Math.max(downTime - delta, 0f);
	}
}
