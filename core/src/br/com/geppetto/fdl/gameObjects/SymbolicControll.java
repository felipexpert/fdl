package br.com.geppetto.fdl.gameObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.screen.Initializable;

public class SymbolicControll implements Initializable, Renderable{
	protected Sprite left;
	protected Sprite right;
	private Sprite zoomIn;
	private Sprite zoomOut;
	private Sprite zoomDefault;
	private Sprite pause;
	private Sprite close;
	
	public static final float MAX_TIME = 0.25f;
	
	private float rightTime;
	private float leftTime;
	private float inTime;
	private float outTime;
	private float defaultTime;
	
	@Override
	public void init() {
		{
			left = Factory.INSTANCE.buildHorArrow(true);
			right = Factory.INSTANCE.buildHorArrow(false);
			float leftX = 0f;
			float rightX = Constants.VIEWPORT_WIDTH / 2f;
			float y = 0f;
			left.setX(leftX);
			left.setY(y);
			right.setX(rightX);
			right.setY(y);
		}
		{
			zoomIn = Factory.INSTANCE.buildZoomIn();
			zoomOut = Factory.INSTANCE.buildZoomOut();
			zoomDefault = Factory.INSTANCE.buildZoomDefault();
			float zoomPadding = .25f;
			float x = Constants.VIEWPORT_WIDTH - (zoomIn.getWidth() + zoomPadding);
			float inY = Constants.VIEWPORT_HEIGHT - (zoomIn.getHeight() + zoomPadding);
			float outY = inY - (zoomIn.getHeight() + zoomPadding);
			float defaultY = outY - (zoomOut.getHeight() + zoomPadding);
			zoomIn.setX(x);
			zoomIn.setY(inY);
			zoomOut.setX(x);
			zoomOut.setY(outY);
			zoomDefault.setX(x);
			zoomDefault.setY(defaultY);
		}
		{
			close = Factory.INSTANCE.buildClose();
			close.setX(.25f);
			close.setY(Constants.VIEWPORT_HEIGHT - (close.getHeight() + .25f));
			pause = Factory.INSTANCE.buildPause();
			pause.setX(close.getX() + close.getWidth() + .25f);
			pause.setY(close.getY());
		}
	}
	
	public void update(float delta) {
		rightTime = Math.max(rightTime - delta, 0f);
		leftTime = Math.max(leftTime - delta, 0f);
		inTime =  Math.max(inTime - delta, 0f);
		outTime =  Math.max(outTime - delta, 0f);
		defaultTime = Math.max(defaultTime - delta, 0f);
	}
	
	public void pressRight() {
		rightTime = MAX_TIME;
	}
	
	public void pressLeft() {
		leftTime = MAX_TIME;
	}
	
	public void pressUp() {}
	
	public void pressDown() {}
	
	public void zoomIn() {
		inTime = MAX_TIME;
	}
	
	public void zoomOut() {
		outTime = MAX_TIME;
	}
	
	public void zoomDefault() {
		defaultTime = MAX_TIME;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		prepareForButton(batch, leftTime);
		left.render(batch);
		prepareForButton(batch, rightTime);
		right.render(batch);
		prepareForButton(batch, inTime);
		zoomIn.render(batch);
		prepareForButton(batch, outTime);
		zoomOut.render(batch);
		prepareForButton(batch, defaultTime);
		zoomDefault.render(batch);
		batch.setColor(1f, 1f, 1f, .375f);
		pause.render(batch);
		close.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}
	
	protected void prepareForButton(SpriteBatch batch, float time) {
		if(time == 0f)
			batch.setColor(1f, 1f, 1f, .375f);
		else
			batch.setColor(1f, 1f, 1f, .675f);
	}
	
	public boolean clickedOnLeft(float x, float y) {
		return left.isInBounds(x, y);
	}
	
	public boolean clickedOnRight(float x, float y) {
		return right.isInBounds(x, y);
	}
	
	public boolean clickedOnUp(float x, float y) {
		return false;
	}
	
	public boolean clickedOnDown(float x, float y) {
		return false;
	}
	
	public boolean clickedOnZoomIn(float x, float y) {
		return zoomIn.isInBounds(x, y);
	}
	
	public boolean clickOnZoomOut(float x, float y) {
		return zoomOut.isInBounds(x, y);
	}
	
	public boolean clickOnZoomDefault(float x, float y) {
		return zoomDefault.isInBounds(x, y);
	}
	
	public boolean clickedOnPause(float x, float y) {
		return pause.isInBounds(x, y);
	}
	
	public boolean clickedOnClose(float x, float y) {
		return close.isInBounds(x, y);
	}
}
