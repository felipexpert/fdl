package br.com.geppetto.fdl.gameObjects.factory;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.utilities.asset.FontWrapper;
import br.com.geppetto.utilities.menuObjects.Button;

public class ButtonFactory {
	public static final ButtonFactory INSTANCE = new ButtonFactory();
	
	private ButtonFactory() {}
	
	public Button genTypeLetters(FontWrapper font, String text) {
		Button b = new Button(Factory.INSTANCE.buildButtonLayer(),
				font,
				text, 0f, 0f, .25f);
		b.setColor(.125f, .125f, .375f, 1f);
		return b;
	}
	
	public Button genTypeLetters(FontWrapper font, String text, float wrapWidth) {
		Button b = new Button(Factory.INSTANCE.buildButtonLayer(),
				font,
				text, 0f, 0f, .5f, wrapWidth);
		b.setColor(.125f, .125f, .375f, 1f);
		return b;
	}
	
	public Button genBackButton() {
		Button back = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonBig, "VOLTAR");
		//back.setPosition(Constants.VIEWPORT_WIDTH - back.getWidth(), 0f);
		return back;
	}
}
