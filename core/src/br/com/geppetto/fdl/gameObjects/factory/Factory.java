package br.com.geppetto.fdl.gameObjects.factory;

import java.util.Map;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.BemVindoStage;
import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.core.stage.storyboard.StoryboardPage;
import br.com.geppetto.fdl.gameObjects.avatar.AvatarUtility;
import br.com.geppetto.fdl.noncore.GamePresentationController;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.fdl.persistence.UserAccount;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.asset.AnimationWrapper;
import br.com.geppetto.utilities.asset.RegionWrapper;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.digit.Digits;
import br.com.geppetto.utilities.helper.AnimatedDrawableHelper;
import br.com.geppetto.utilities.helper.DrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;
import br.com.geppetto.utilities.menuObjects.Label;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Factory {
	public static final Factory INSTANCE = new Factory();
	
	private Factory() {}
	
	private final NonAnimatedDrawableHelper buttonLayerDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.button.btnLayer);
	private final NonAnimatedDrawableHelper gearDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.gear);
	private final Vector2 gearSize = new Vector2(2.5f, 2.5f);
	private final NonAnimatedDrawableHelper garbageDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.garbage);
	private final Vector2 garbageSize = new Vector2(1.75f, 2f);
	private final Vector2 _0X0 = new Vector2(0f, 0f),
			 					 _1X1 = new Vector2(1f, 1f);
	private final NonAnimatedDrawableHelper popUpDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.popUp);
	private final Vector2 powerUp = new Vector2(StageFactory.GROUND_SPACE.x + .5f,
			StageFactory.GROUND_SPACE.y + .5f);
	private final NonAnimatedDrawableHelper arrowHorDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.arrowHor);
	private final NonAnimatedDrawableHelper arrowVerDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.arrowVer);
	private final Vector2 arrowHorSize = new Vector2(Constants.VIEWPORT_WIDTH / 2f,
			3.5f);
	private final Vector2 arrowVerSize = new Vector2(arrowHorSize.y,
			arrowHorSize.x);
	private final NonAnimatedDrawableHelper zoomInDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.zoomIn);
	private final NonAnimatedDrawableHelper zoomOutDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.zoomOut);
	private final NonAnimatedDrawableHelper zoomDefaultDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.zoomDefault);
	private final NonAnimatedDrawableHelper pauseDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.pause);
	private final NonAnimatedDrawableHelper closeDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.close);
	private final Vector2 zoomSize = new Vector2(2f, 2f);
	private final NonAnimatedDrawableHelper hudBackgroundDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.hudBackground);
	private final Vector2 hudSize = new Vector2(Constants.VIEWPORT_WIDTH,
			Constants.HUD_VIEWPORT_HEIGHT);
	private final NonAnimatedDrawableHelper accLayerDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.accLayer);
	private final NonAnimatedDrawableHelper exitDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.exit);
	private final Vector2 exitSize = new Vector2(1.75f,
			2.5f);
	private final Vector2 faceSize = new Vector2(4 * 24f / 32f, 4f);
	private final Vector2 thumbnailSize = new Vector2(4f * 100f / 60f, 4f);
	private final Vector2 thumbnailBGSize = new Vector2(thumbnailSize.x + .75f, thumbnailSize.y + .75f);
	private final NonAnimatedDrawableHelper whiteBGDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.menu.whiteBG);
	private final Vector2 screenCenter = new Vector2(0f,
			Constants.VIEWPORT_HEIGHT / 2f);
	private final Vector2 storyboardSize = new Vector2(Constants.VIEWPORT_WIDTH,
			Constants.VIEWPORT_WIDTH * 60f / 100f);
	public Sprite buildButtonLayer() {
		Sprite button = new Sprite(new Vector2(), 
				buttonLayerDH, 
				new Vector2(), 
				_0X0, 
				_1X1);
		return button;
	}
	
	public Sprite buildGear() {
		return new Sprite(new Vector2(),
				gearDH, 
				gearSize, 
				_0X0, _1X1);
	}
	
	public Sprite buildGarbage() {
		return new Sprite(new Vector2(),
				garbageDH,
				garbageSize, 
				_0X0, _1X1);
	}
	
	public Sprite buildPopUp() {
		return new Sprite(new Vector2(), 
				popUpDH, 
				new Vector2(), 
				_0X0,
				_1X1);
	}
	
	public CollidableSprite buildAvatarScope() {
		float height = .75f;
		float spriteHeight = 2.5f;
		return new CollidableSprite(new Rectangle(0f, 0f, .75f, height), 
				new Vector2(0f, spriteHeight * .65f - height), 
				null, 
				new Vector2(spriteHeight * 24f / 32f, spriteHeight), 
				_0X0, 
				new Vector2(1f, 1f));
	}
	
	public Sprite buildAvatarFace(DrawableHelper dh) {
		return new Sprite(new Vector2(),
				dh,
				faceSize,
				_0X0,
				_1X1);
	}
	
	public CollidableSprite buildPowerUpScope() {
		return new CollidableSprite(new Rectangle(0f, 0f, powerUp.x, powerUp.y),
				_0X0, null, powerUp, _0X0, _1X1);
	}
	
	public Sprite buildHorArrow(boolean flipX) {
		Sprite s = new Sprite(new Vector2(), 
				arrowHorDH, arrowHorSize, _0X0, _1X1);
		s.setFlipX(flipX);
		return s;
	}
	
	public Sprite buildVerArrow(boolean flipY) {
		Sprite s = new Sprite(new Vector2(), 
				arrowVerDH, arrowVerSize, _0X0, _1X1);
		s.setFlipY(flipY);
		return s;
	}
	
	public Sprite buildZoomIn() {
		return new Sprite(new Vector2(), 
				zoomInDH, zoomSize, _0X0, _1X1);
	}
	
	public Sprite buildZoomOut() {
		return new Sprite(new Vector2(), 
				zoomOutDH, zoomSize, _0X0, _1X1);
	}
	
	public Sprite buildZoomDefault() {
		return new Sprite(new Vector2(), 
				zoomDefaultDH, zoomSize, _0X0, _1X1);
	}
	
	public Sprite buildPause() {
		return new Sprite(new Vector2(), 
				pauseDH, zoomSize, _0X0, _1X1);
	}
	
	public Sprite buildClose() {
		return new Sprite(new Vector2(), 
				closeDH, zoomSize, _0X0, _1X1);
	}
	
	public Digits buildTimerDigits() {
		return new Digits(Assets.INSTANCE.digits, .25f, .75f, 1.5f);
	}
	
	public Digits buildWordsDigits() {
		return new Digits(Assets.INSTANCE.digits, .375f, 1.5f, 2.5f);
	}
	
	public Sprite buildHUDBackground() {
		return new Sprite(_0X0, hudBackgroundDH, hudSize, _0X0, _1X1);
	}
	
	public Sprite buildAccLayer() {
		Sprite button = new Sprite(new Vector2(), 
				accLayerDH, 
				new Vector2(), 
				_0X0, 
				_1X1);
		return button;
	}
	
	public GamePresentationController buildPresentationController(boolean staticCamera) {
		//UserPersistence.INSTANCE.getLastUserAccount().getLastStagePlayed();
		GamePresentationController p = new GamePresentationController(ConfigPersistence.INSTANCE.getLastStagePlayed());
		OrthographicCamera c = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT); 
		if(staticCamera)
			p.setCamera(c);
		return p;
	}
	
	public Label buildScreenTitles(String text) {
		Label label = new Label(Assets.INSTANCE.fonts.title, 
				text, 0f, 0f, Constants.VIEWPORT_WIDTH);
		//label.setColor(15f / 255f, 109f / 255f, 1f  / 255f, 1f);
		label.setColor(254f / 255f, 
				255f / 255f, 
				201f  / 255f, 1f);
		float width = label.getWidth();
		float height = label.getHeight();
		label.setPosition(Constants.VIEWPORT_WIDTH / 2f - width / 2f,
				Constants.VIEWPORT_HEIGHT - height - 1f);
		return label;
	}
	
	public Label buildCommonLabel(String text, float wrapWidth) {
		Label l = new Label(Assets.INSTANCE.fonts.label, 
				text, 0f, 0f, wrapWidth);
		l.setColor(.75f, 1f, .75f, 1f);
		return l;
	}
	
	public Label buildNameLabel(String text, float wrapWidth) {
		Label l = new Label(Assets.INSTANCE.fonts.game, 
				text, 0f, 0f, wrapWidth);
		l.setColor(1f, .7f, .2f, 1f);
		return l;
	}
	
	public Label buildCreditsLabel(String text, float wrapWidth) {
		Label l = new Label(Assets.INSTANCE.fonts.credits, 
				text, 0f, 0f, wrapWidth);
		l.setColor(.125f, .125f, .375f, 1f);
		return l;
	}
	
	public Label buildStoryboardLabel(String text, float wrapWidth) {
		Label l = buildCreditsLabel(text, wrapWidth);
		l.setColor(1f, 1f, 1f, 1f);
		return l;
	}
	
	public Sprite buildExit() {
		return new Sprite(new Vector2(.25f, .25f), 
				exitDH, exitSize, _0X0, _1X1);
	}
	
	public Sprite buildThumbnail(RegionWrapper rw) {
		return new Sprite(new Vector2(), 
				new NonAnimatedDrawableHelper(rw), 
				thumbnailSize, _0X0, _1X1);
	}
	
	public Sprite buildThumbnailBG() {
		return new Sprite(new Vector2(), 
				whiteBGDH, 
				thumbnailBGSize, _0X0, _1X1);
	}
	
	public Sprite buildThumbnail(AnimationWrapper aw) {
		return new Sprite(new Vector2(), 
				new AnimatedDrawableHelper(aw), 
				thumbnailSize, _0X0, _1X1);
	}
	
	public CollidableSprite buildDefaultWall(DrawableHelper dh) {
		return new CollidableSprite(new Rectangle(0f, 0f, StageFactory.WALL_WIDTH, StageFactory.WALL_HEIGHT),
				StageFactory.DEFAULT_WALL_IMG_OFFSET, dh,
				StageFactory.DEFAULT_WALL_IWS, _0X0, _1X1);
	}
	
	public CollidableSprite buildWall(DrawableHelper dh, Vector2 imgOffset, Vector2 iws) {
		return new CollidableSprite(new Rectangle(0f, 0f, StageFactory.WALL_WIDTH, StageFactory.WALL_HEIGHT),
				imgOffset, dh,
				iws, _0X0, _1X1);
	}
	
	public Sprite buildGround(DrawableHelper dh) {
		return new Sprite(new Vector2(), 
				dh, 
				StageFactory.GROUND_IWS, _0X0, _1X1);
	}
	
	private Sprite buildStoryboard(RegionWrapper rw) {
		return new Sprite(screenCenter, 
				new NonAnimatedDrawableHelper(rw),
				storyboardSize, 
				_0X0, 
				_1X1);
	}
	
	public StoryboardPage buildPage(RegionWrapper rw, String message) {
		StoryboardPage p = new StoryboardPage();
		p.setPage(buildStoryboard(rw));
		p.setText(message);
		return p;
	}
}
