package br.com.geppetto.fdl.gameObjects.mobile;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class PowerUp implements Initializable, Updatable, Renderable, DebugRenderable {
	private static final float CHANGE_TIME = 1f;
	private static final NonAnimatedDrawableHelper ROLLER_DH = 
		new NonAnimatedDrawableHelper(Assets.INSTANCE.powerUp.roller);
	private static final NonAnimatedDrawableHelper SLIPPER_DH = 
		new NonAnimatedDrawableHelper(Assets.INSTANCE.powerUp.slipper);
	private CollidableSprite item;
	private boolean roller;
	private float time;
	
	@Override
	public void init() {
		item = Factory.INSTANCE.buildPowerUpScope();
		roller = true;
		item.setDrawableHelper(ROLLER_DH);
	}

	@Override
	public void update(float delta) {
		time += delta;
		if(time >= CHANGE_TIME) {
			time -= CHANGE_TIME;
			roller = !roller;
			if(roller) {
				item.setDrawableHelper(ROLLER_DH);
			} else {
				item.setDrawableHelper(SLIPPER_DH);
			}
		}
	}
	
	public boolean collided(CollidableSprite c) {
		return item.collided(c);
	}
	
	public boolean isRoller() {
		return roller;
	}
	
	public void updatePosition(float x, float y) {
		item.updateCenterX(x);
		item.updateCenterY(y);
	}

	@Override
	public void render(SpriteBatch batch) {
		item.render(batch);
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		item.renderDebug(shapeRenderer);
	}
}
