package br.com.geppetto.fdl.gameObjects.mobile;

import br.com.geppetto.fdl.core.Map;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class PowerUpController implements Initializable, Updatable, Renderable, DebugRenderable{
	private static final float CHANGE_PLACE_TIME = 30f;
	private float time;
	private PowerUp powerUp;
	private Array<Vector2> spawns;
	private Avatar avatar;
	private Map map;
	
	private Vector2 lastSpawn;
	
	public PowerUpController(Avatar avatar, Map map) {
		this.avatar = avatar;
		this.map = map;
	}

	@Override
	public void init() {
		spawns = map.getPowerUpSpawns();
		powerUp = new PowerUp();
		powerUp.init();
		changePlace();
	}

	@Override
	public void update(float delta) {
		if(powerUp.collided(avatar.getAvatar())) {
			if(powerUp.isRoller()) {
				avatar.increaseVel();
			} else {
				avatar.decreaseVel();
			}
			changePlace();
		}
		powerUp.update(delta);
		time += delta;
		if(time >= CHANGE_PLACE_TIME) {
			changePlace();
		}
	}
	
	private void changePlace() {
		time = 0f;
		Vector2 random = null;
		do {
			random = spawns.random();
		} while(random == lastSpawn);
		Vector2 spawn = random;
		lastSpawn = random;
		powerUp.updatePosition(spawn.x, spawn.y);
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		powerUp.renderDebug(shapeRenderer);
	}

	@Override
	public void render(SpriteBatch batch) {
		powerUp.render(batch);
	}
}
