package br.com.geppetto.fdl.gameObjects;

import java.util.ArrayList;
import java.util.List;

import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.fdl.gameObjects.letter.Letter;
import br.com.geppetto.fdl.gameObjects.letter.LettersController;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.math.Utils;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public class Walls implements Initializable, Updatable, Renderable, DebugRenderable{
	private StageFactory stageFactory;
	private DeepnessHelper deepnessHelper;
	private List<Sprite> justDraw;
	private List<Sprite> justDrawBottom;
	private List<CollidableSprite> walls;
	private Rectangle internal;
	private Avatar avatar;
	private LettersController letters;
	
	public Walls(StageFactory stageFactory, DeepnessHelper deepnessHelper, Avatar avatar, LettersController letters) {
		this.stageFactory = stageFactory;
		this.deepnessHelper = deepnessHelper;
		this.avatar = avatar;
		this.letters = letters;
	}
	
	@Override
	public void init() {
		walls = new ArrayList<>();
		justDraw = new ArrayList<>();
		justDrawBottom = new ArrayList<>();
		initInternal();
		initEdges();
	}
	
	private void initInternal() {
		char[][] stage = stageFactory.getStageWorld();
		for(int line = 0; line < stage.length; line++) {
			for(int col = 0; col < stage[0].length; col++) {
				if(stage[line][col] == '#') {
					float centerX = (col + 1) * StageFactory.GROUND_SPACE.x + StageFactory.GROUND_SPACE.x / 2f;
					float centerY = (line + 1) * StageFactory.GROUND_SPACE.y + StageFactory.GROUND_SPACE.y / 2f;
					CollidableSprite wall = stageFactory.buildWall();
					wall.updateCenterX(centerX);
					wall.updateCenterY(centerY);
					walls.add(wall);
					deepnessHelper.addAnalybable(wall.getSprite());
				}
			}
		}
		internal = new Rectangle(StageFactory.GROUND_SPACE.x, 
				StageFactory.GROUND_SPACE.y, 
				StageFactory.GROUND_SPACE.x * stage[0].length, 
				StageFactory.GROUND_SPACE.y * stage.length);
	}
	
	private void initEdges() {
		char[][] stage = stageFactory.getStageWorld();
		for(int col = 0; col < stage[0].length + 2; col++) {
			float centerX = col * StageFactory.GROUND_SPACE.x + StageFactory.GROUND_SPACE.x / 2f;
			float centerY2 = (stage.length + 1) * StageFactory.GROUND_SPACE.y + StageFactory.GROUND_SPACE.y / 2f;
			CollidableSprite wall2 = stageFactory.buildWall();
			wall2.updateImgCenterX(centerX);
			wall2.updateCenterY(centerY2);
			justDraw.add(wall2.getSprite());
		}
		
		for(int line = stage.length - 1; line >= 0; line--) {
			float centerY = (line + 1) * StageFactory.GROUND_SPACE.y + StageFactory.GROUND_SPACE.y / 2f;
			float centerX1 = StageFactory.GROUND_SPACE.x / 2f;
			float centerX2 = (stage[0].length + 1) * StageFactory.GROUND_SPACE.x + StageFactory.GROUND_SPACE.x / 2f;
			CollidableSprite wall1 = stageFactory.buildWall();
			wall1.updateCenterX(centerX1);
			wall1.updateCenterY(centerY);
			CollidableSprite wall2 = stageFactory.buildWall();
			wall2.updateCenterX(centerX2);
			wall2.updateCenterY(centerY);
			justDraw.add(wall1.getSprite());
			justDraw.add(wall2.getSprite());
		}
		
		for(int col = 0; col < stage[0].length + 2; col++) {
			float centerX = col * StageFactory.GROUND_SPACE.x + StageFactory.GROUND_SPACE.x / 2f;
			float centerY1 = StageFactory.GROUND_SPACE.y / 2f;
			CollidableSprite wall1 = stageFactory.buildWall();
			wall1.updateCenterX(centerX);
			wall1.updateCenterY(centerY1);
			justDrawBottom.add(wall1.getSprite());
		}
	}
	
	@Override
	public void update(float delta) {
		CollidableSprite avat = avatar.getAvatar();
		stageFactory.updateWallDH(delta);
		if(!Utils.ensureInside(avat, internal)) {
			avatar.reverse();
		} else {
			for(CollidableSprite w : walls) {
				if(!Utils.ensureNoTouching(avat, w)) {
					avatar.reverse();
					break;
				}
			}
		}
		/*if(!avatar.isContained(internal)) {
			avatar.reverse();
		} else {
			for(CollidableSprite w : walls) {
				if(avatar.colided(w)) {
					avatar.reverse();
					break;
				}
			}
		}*/
		/*CollidableSprite lett = letter.getLetter();
		if(!Utils.ensureInside(lett, internal)) {
			letter.turn();
		} else {
			for(CollidableSprite w : walls) {
				if(!Utils.ensureNoTouching(lett, w)) {
					letter.turn();
					break;
				}
			}
		}*/
		Array<Letter> letters = this.letters.getLetters();
		for(int i = 0; i < letters.size; i++) {
			Letter letter = letters.get(i);
			if(!Utils.ensureInside(letter.getCollidableLetter(), internal)) {
				letter.turn();
			} else {
				for(CollidableSprite w : walls) {
					if(!Utils.ensureNoTouching(letter.getCollidableLetter(), w)) {
						letter.turn();
						break;
					}
				}
			}
			/*if(!letter.isContained(internal)) {
				letter.turn();
			} else {
				for(CollidableSprite w : walls) {
					if(letter.colided(w)) {
						letter.turn();
						break;
					}
				}
			}*/
		}
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		for(CollidableSprite w : walls)
			w.renderDebug(shapeRenderer);
	}

	@Override
	public void render(SpriteBatch batch) {
		for(Sprite w : justDraw)
			w.render(batch);
	}
	
	public void renderBottomEdge(SpriteBatch batch) {
		for(Sprite w : justDrawBottom)
			w.render(batch);
	}
}
