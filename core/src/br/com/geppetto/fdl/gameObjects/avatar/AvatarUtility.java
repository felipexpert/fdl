package br.com.geppetto.fdl.gameObjects.avatar;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.TreeSet;

import com.badlogic.gdx.Gdx;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.Avatares;
import br.com.geppetto.utilities.asset.AnimationWrapper;
import br.com.geppetto.utilities.asset.RegionWrapper;
import br.com.geppetto.utilities.helper.AnimatedDrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;


public class AvatarUtility {
	private AvatarUtility() {}
	
	public static final float DISTANCE_OFFSET = .5f;
	
	public static NonAnimatedDrawableHelper getFace(int avatar) {
		return getNonAnimated("avatar" + avatar);
	}
	
	public static AnimatedDrawableHelper getL(int avatar) {
		return getAnimated("avatar" + avatar + "L");
	}
	
	public static AnimatedDrawableHelper getF(int avatar) {
		return getAnimated("avatar" + avatar + "F");
	}
	
	public static AnimatedDrawableHelper getB(int avatar) {
		return getAnimated("avatar" + avatar + "B");
	}
	
	private static NonAnimatedDrawableHelper getNonAnimated(String name) {
		try {
			Field field = Avatares.class.getField(name);
			return new NonAnimatedDrawableHelper
					((RegionWrapper)field.get(Assets.INSTANCE.avatares));
		} catch (Exception e) {
			Gdx.app.exit();
			return null;
		}
	}
	
	private static AnimatedDrawableHelper getAnimated(String name) {
		try {
			Field field = Avatares.class.getField(name);
			return new AnimatedDrawableHelper
					((AnimationWrapper)field.get(Assets.INSTANCE.avatares));
		} catch (Exception e) {
			Gdx.app.exit();
			return null;
		}
	}
	
	public static Set<Integer> avatares() {
		Set<Integer> avatares = new TreeSet<>();
		Field[] fields = Avatares.class.getFields();
		for(Field f : fields) {
			String name = f.getName();
			Integer i = Integer.valueOf(name.replaceAll("^\\w+(\\d+)\\w*$", "$1"));
			avatares.add(i);
		}
		
		return avatares;
	}
}
