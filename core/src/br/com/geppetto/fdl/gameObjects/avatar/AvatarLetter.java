package br.com.geppetto.fdl.gameObjects.avatar;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.fdl.gameObjects.BasicLetter;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.CoordnateAnalyzable;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

public class AvatarLetter implements Initializable, Follower, Updatable, Renderable, DebugRenderable, CoordnateAnalyzable, LetterTarget{
	private LetterTarget target;
	private BasicLetter basicLetter;
	private int avatarCode;
	//private CollidableSprite child;
	//private AvatarHelper helper;
	//private Label letter;
	//private Direction direction;
	
	//private float alpha;
	
	public AvatarLetter(LetterTarget target, int avatarCode) {
		this.target = target;
		this.avatarCode = avatarCode;
	}
	
	public AvatarLetter(int avatarCode) {
		this(null, avatarCode);
	}
	
	public void setLetter(String letter) {
		basicLetter.setLetter(letter);
	}
	
	public String getLetter() {
		return basicLetter.getLetter();
	}
	
	@Override
	public void init() {
		basicLetter = new BasicLetter(avatarCode);
		basicLetter.init();
		if(target != null)
			follow(target);
	}
	
	@Override
	public void follow(LetterTarget target) {
		this.target = target;
		if(target.getDirection().horizontal)
			putBehindHorizontalTarget();
		else //Followable 
			putBehindVerticalTarget();
	}
	
	private void putBehindVerticalTarget() {
		this.setDirection(target.getDirection());
		switch(target.getDirection()) {
			case NORTH:
				basicLetter.updatePosition(target.getX(),
				target.getY() - basicLetter.getHeight() - AvatarUtility.DISTANCE_OFFSET);
				break;
			default:
				basicLetter.updatePosition(target.getX(), 
				target.getY() + target.getHeight() + AvatarUtility.DISTANCE_OFFSET);
		}
	}
	
	private void putBehindHorizontalTarget() {
		this.setDirection(target.getDirection());
		switch(target.getDirection()) {
			case EAST:
				basicLetter.updatePosition(
				target.getX() - basicLetter.getWidth() - AvatarUtility.DISTANCE_OFFSET,
				target.getY());
				break;
			default:
				basicLetter.updatePosition(
				target.getX() + target.getWidth() + AvatarUtility.DISTANCE_OFFSET,
				target.getY());
				
		}
	}
	
	public void setDirection(Direction direction) {
		basicLetter.setDirection(direction);
	}
	
	public Direction getDirection() {
		return basicLetter.getDirection();
	}
	
	private void arrangeChild() {
		if(target.getDirection() != this.getDirection()) {
			if(target.getDirection().horizontal) {
				if(this.getDirection() == Direction.NORTH) {
					float verticalDif = basicLetter.getY() - target.getY();
					if(verticalDif >= 0f) {
						//this.setDirection(target.getDirection());
						putBehindHorizontalTarget();
						//updateNormally = false;
					}
				} else {
					float verticalDif = basicLetter.getY() - target.getY();
					if(verticalDif <= 0f) {
						//this.setDirection(target.getDirection());
						putBehindHorizontalTarget();
						//updateNormally = false;
					}		
				}
			} else {
				if(this.getDirection().equals(Direction.EAST)) {
					float horizontalDif = basicLetter.getX() - target.getX();
					if(horizontalDif >= 0f) {
						//this.setDirection(target.getDirection());
						putBehindVerticalTarget();
						//updateNormally = false;
					}
				} else {
					float horizontalDif = basicLetter.getX() - target.getX();
					if(horizontalDif <= 0f) {
						//this.setDirection(target.getDirection());
						putBehindVerticalTarget();
						//updateNormally = false;
					}		
				}
			}
			
			//if(updateNormally) {
			//	super.update(delta); //to update normally
			//}
		} 
	}

	@Override
	public void update(float delta) {
		//boolean updateNormally = true;
		basicLetter.setVelocity(target.getVelocity());
		basicLetter.update(delta);
		arrangeChild();
	}
	
	public LetterTarget getTarget() {
		return target;
	}

	@Override
	public float getX() {
		return basicLetter.getX();
	}

	@Override
	public float getY() {
		return basicLetter.getY();
	}
	
	public void setTarget(LetterTarget target) {
		this.target = target;
	}

	@Override
	public void render(SpriteBatch batch) {
		basicLetter.render(batch);
	}
	
	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		basicLetter.renderDebug(shapeRenderer);
	}

	@Override
	public float getWidth() {
		return basicLetter.getWidth();
	}

	@Override
	public float getHeight() {
		return basicLetter.getHeight();
	}

	@Override
	public float getVelocity() {
		return target.getVelocity();
	}
	
	public void oposity() {
		basicLetter.oposity();
	}
	
	public void updatePosition(float x, float y) {
		basicLetter.updatePosition(x, y);
	}
	
	public void onElimination(Runnable runnable) {
		basicLetter.onElimination(runnable);
	}
	
	public boolean collided(CollidableSprite c) {
		return basicLetter.collided(c);
	}
}
