package br.com.geppetto.fdl.gameObjects.avatar;

import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.Sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WalkingController {
	private Sprite avatar;
	private TextureRegion walkingControll;
	private int walkingFrame;
	
	public WalkingController(Sprite avatar) {
		this.avatar = avatar;
	}
	
	public void update() {
		TextureRegion r = avatar.getDrawableHelper().getTextureRegion();
		if(r != walkingControll) {
			walkingFrame++;
			if(walkingFrame == 12) {
				walkingFrame = 0;
				walkingControll = r;
				SoundManager.INSTANCE.playWalk();
			}
		}
	}
}
