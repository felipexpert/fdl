package br.com.geppetto.fdl.gameObjects.avatar;

import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.fdl.core.Map;
import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.gameObjects.letter.LettersController;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.math.Utils;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Avatar implements Initializable, Updatable, DebugRenderable, LetterTarget {
	private static final String TAG = Avatar.class.getSimpleName();
	private static final float START_VELOCITY = 5f;
	private static final float MAX_VELOCITY = 6.5f;
	private static final float MIN_VELOCITY = 4.5f;
	private static final float INCREASE_VEL = .25f;
	private static final float TURNING_DELAY_OFFSET = 1.25f;
	private static final int MAX_HITS = 15;
	private static final float MAX_TIME_HIT_CONTROL = 3f;
	
	private int hitControll;
	private float timeHitControll;
	
	private Map map;
	
	private AvatarHelper avatarHelper;
	private CollidableSprite avatar;
	private Direction direction;
	private int avatarCode;
	private float velocity;
	private DeepnessHelper deepnessHelper;
	private Array<AvatarLetter> letters;
	private Array<AvatarLetter> discarted;
	private Direction turnTo;
	private Vector2 pointLastTurning;
	private Array<String> lettersOrder;
	
	private LettersController lettersController;
	
	private SymbolicControll symbolicControll;
	
	private WalkingController walkingController;
	
	public Avatar(int avatarCode, DeepnessHelper deepnessHelper, Map map, SymbolicControll symbolicControll) {
		this.avatarCode = avatarCode;
		this.deepnessHelper = deepnessHelper;
		this.map = map;
		this.symbolicControll = symbolicControll;
	}
	
	@Override
	public void init() {
		letters = new Array<>();
		discarted = new Array<>();
		lettersOrder = new Array<>();
		pointLastTurning = new Vector2();
		avatar = Factory.INSTANCE.buildAvatarScope();
		
		avatarHelper = new AvatarHelper(avatarCode, avatar);
		avatarHelper.init();
		
		// Little cheating to prepare avatarHelper before initializing 
		this.direction = Direction.NORTH;
		forceDirection(Direction.SOUTH);
		
		avatar.updateCenterX(map.getAvatarSpawn().x);
		avatar.updateCenterY(map.getAvatarSpawn().y);
		velocity = START_VELOCITY;
		deepnessHelper.addAnalybable(avatar.getSprite());
		
		walkingController = new WalkingController(avatar.getSprite());
		
		Gdx.app.log(TAG, "Avatar has been initialized!");
	}
	
	public void increaseVel() {
		velocity = Math.min(velocity + INCREASE_VEL, MAX_VELOCITY);
		SoundManager.INSTANCE.playRoller();
	}
	
	public void decreaseVel() {
		velocity = Math.max(velocity - INCREASE_VEL, MIN_VELOCITY);
		SoundManager.INSTANCE.playSlipper();
	}
	
	public boolean isContained(Rectangle rectangle) {
		return avatar.isContained(rectangle);
	}
	
	public void reverse() {
		SoundManager.INSTANCE.playReverse();
		hitControll++;
		pointLastTurning.x = 9999f;
		pointLastTurning.y = 9999f;
		if(letters.size == 0) {
			forceDirection(direction.oposite());
			return;
		}
		{
			float prevX = getX();
			float prevY = getY();
			Direction prevDir = direction;
			avatar.updatePositionX(letters.peek().getX());
			avatar.updatePositionY(letters.peek().getY());
			forceDirection(letters.peek().getDirection().oposite());
			for(int i = 0; i < letters.size; i++) {
				float pX = letters.get(i).getX();
				float pY = letters.get(i).getY();
				Direction pD = letters.get(i).getDirection();
				letters.get(i).updatePosition(prevX, prevY);
				letters.get(i).setDirection(prevDir);
				prevX = pX;
				prevY = pY;
				prevDir = pD;
			}
		}
		
		letters.reverse();
		
		letters.first().setTarget(this);
		for(int i = 1; i < letters.size; i++)
			letters.get(i).setTarget(letters.get(i-1));
		for(AvatarLetter l : letters)
			l.oposity();
	
		arrangeLetters();
		if(hitControll == MAX_HITS) {
			looseLettersFrom(letters.size - 1);
			resetHits();
		}
	}
	
	private void arrangeLetters() {
		for(int i = 0; i < lettersOrder.size; i++) {
			letters.get(i).setLetter(lettersOrder.get(i));
		}
	}
	
	public boolean collided(CollidableSprite c) {
		return avatar.collided(c);
	}
	
	public void turn(Direction direction) {
		switch(direction) {
		case NORTH:
			symbolicControll.pressUp();
			break;
		case EAST:
			symbolicControll.pressRight();
			break;
		case SOUTH:
			symbolicControll.pressDown();
			break;
		case WEST:
			symbolicControll.pressLeft();
			break;
		}
		
		if(direction.horizontal != this.direction.horizontal)
			turnTo(direction);
	}
	
	public void turnRight() {
		symbolicControll.pressRight();
		turnTo(direction.plus(1));
	}
	
	public void turnLeft() {
		symbolicControll.pressLeft();
		turnTo(direction.plus(-1));
	}
	
	public Sprite getSprite() {
		return avatar.getSprite();
	}
	
	private void forceDirection(Direction direction) {
		avatarHelper.updateDirection(this.direction, direction);
		this.direction = direction;
	}
	
	private void turnTo(Direction direction) {
		if(turnTo == null) turnTo = direction;
	}
	
	private void checkDirection() {
		if(
			 turnTo != null && turnTo != direction &&
			 ((turnTo.horizontal && Math.abs(avatar.getY() - pointLastTurning.y) >= (avatar.getHeight() + AvatarUtility.DISTANCE_OFFSET) * TURNING_DELAY_OFFSET) ||
		     (!turnTo.horizontal && Math.abs(avatar.getX() - pointLastTurning.x) >= (avatar.getWidth() + AvatarUtility.DISTANCE_OFFSET) * TURNING_DELAY_OFFSET))
		  ) {
			pointLastTurning.x = avatar.getX();
			pointLastTurning.y = avatar.getY();
			forceDirection(turnTo);
			turnTo = null;
		} 
	}
	
	public void setLettersController(LettersController lettersController) {
		this.lettersController = lettersController;
	}
	
	protected void updatePosition(float x, float y) {
		avatar.updatePositionX(x);
		avatar.updatePositionY(y);
	}
	
	@Override
	public void update(float delta) {
		checkDirection();
		for(AvatarLetter d : discarted)
			d.update(delta);
		avatarHelper.update(delta);
		avatar.addPositionX(direction.x * velocity * delta);
		avatar.addPositionY(direction.y * velocity * delta);

		for(AvatarLetter l : letters)
			l.update(delta);
		
		for(int i = 2; i < letters.size; i++) {
			if(letters.get(i).collided(avatar)) {
				looseLettersFrom(i);
				SoundManager.INSTANCE.playTouchedQueue();
				break;
			}
		}
		checkHits(delta);
		
		walkingController.update();
	}
	
	private void checkHits(float delta) {
		if(hitControll > 0) {
			timeHitControll += delta;
			if(timeHitControll >= MAX_TIME_HIT_CONTROL) {
				resetHits();
			}
		}
	}
	
	private void resetHits() {
		timeHitControll = 0f;
		hitControll = 0;
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		for(AvatarLetter l : letters)
			l.renderDebug(shapeRenderer);
		avatarHelper.renderDebug(shapeRenderer);
	}
	
	public CollidableSprite getAvatar() {
		return avatar;
	}
	
	public Direction getDirection() {
		return direction;
	}

	@Override
	public float getX() {
		return avatar.getX();
	}

	@Override
	public float getY() {
		return avatar.getY();
	}

	@Override
	public float getWidth() {
		return avatar.getWidth();
	}

	@Override
	public float getHeight() {
		return avatar.getHeight();
	}

	@Override
	public float getVelocity() {
		return velocity;
	}
	
	/*public Array<String> getLetters() {
		return lettersOrder;
	}*/
	
	public void addLetter(String letter, int avatarCode) {
		lettersOrder.add(letter);
		AvatarLetter l = new AvatarLetter(avatarCode);
		l.init();
		if(letters.size > 0) {
			l.follow(letters.peek());
			//letters.get(0).setTarget(l);
			//letters.insert(0, l);
		} else {
			l.follow(this);
		}
		letters.add(l);
		arrangeLetters();
		deepnessHelper.addAnalybable(l);
		informLettersChanged();
	}
	
	private void looseLettersFrom(int index) {
		for(int i = lettersOrder.size - 1; i >= index; i--) {
			final AvatarLetter al = letters.get(i);
			lettersOrder.removeIndex(i);
			letters.removeIndex(i);
			discarted.add(al);
			al.onElimination(new Runnable() {
				@Override
				public void run() {
					discarted.removeValue(al, true);
					deepnessHelper.removeAnalyzable(al);
				}
			});
		}
		informLettersChanged();
	}
	
	public void looseLetters() {
		looseLettersFrom(0);
	}
	
	private void informLettersChanged() {
		lettersController.setCurrLetter(lettersOrder.size);
	}
	
	public int getAvatarCode() {
		return avatarCode;
	}
}
