package br.com.geppetto.fdl.gameObjects.avatar;

import br.com.geppetto.utilities.CollidableSprite;

public interface Follower {
	void follow(LetterTarget target);
}