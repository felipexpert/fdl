package br.com.geppetto.fdl.gameObjects.avatar;

import br.com.geppetto.fdl.core.Direction;

public interface LetterTarget {
	Direction getDirection();
	float getX();
	float getY();
	float getWidth();
	float getHeight();
	float getVelocity();
}
