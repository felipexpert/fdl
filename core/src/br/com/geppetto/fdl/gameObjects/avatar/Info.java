package br.com.geppetto.fdl.gameObjects.avatar;

import com.badlogic.gdx.math.Vector2;

import br.com.geppetto.fdl.core.Direction;

public class Info {
	public Vector2 pos;
	public Direction newDir;
}
