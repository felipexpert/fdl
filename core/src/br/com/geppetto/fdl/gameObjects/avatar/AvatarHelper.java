package br.com.geppetto.fdl.gameObjects.avatar;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.helper.AnimatedDrawableHelper;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

public class AvatarHelper implements Initializable, DebugRenderable, Updatable {
	protected CollidableSprite avatar; 
	private AnimatedDrawableHelper L;
	private AnimatedDrawableHelper F;
	private AnimatedDrawableHelper B;
	
	private int avatarCode;
	
	public AvatarHelper(int avatarCode, CollidableSprite avatar) {
		this.avatarCode = avatarCode;
		this.avatar = avatar;
	}
	
	@Override
	public void init() {
		L = AvatarUtility.getL(avatarCode);
		F = AvatarUtility.getF(avatarCode);
		B = AvatarUtility.getB(avatarCode);
	}
	
	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		avatar.renderDebug(shapeRenderer);
	}
	
	public void updateDirection(Direction oldDir, Direction newDir) {
		switch (newDir) {
		case NORTH:
			avatar.setDrawableHelper(B);
			avatar.setFlipX(false);
			B.setTime(prepareOldDir(oldDir));
			break;
		case EAST:
			avatar.setDrawableHelper(L);
			avatar.setFlipX(true);
			L.setTime(prepareOldDir(oldDir));
			break;
		case SOUTH:
			avatar.setDrawableHelper(F);
			avatar.setFlipX(false);
			F.setTime(prepareOldDir(oldDir));
			break;
		case WEST:
			avatar.setDrawableHelper(L);
			avatar.setFlipX(false);
			L.setTime(prepareOldDir(oldDir));
			break;
		}
	}
	
	private float prepareOldDir(Direction oldDir) {
		float time = 0f;
		switch (oldDir) {
		case NORTH:
			time = B.getTime();
			B.setTime(0f);
			break;
		case EAST:
		case WEST:
			time = L.getTime();
			L.setTime(0f);
			break;
		case SOUTH:
			time = F.getTime();
			F.setTime(0f);
			break;
		}
		return time;
	}

	@Override
	public void update(float delta) {
		avatar.update(delta);
	}
	
}
