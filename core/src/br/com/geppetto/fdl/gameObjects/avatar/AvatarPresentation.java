package br.com.geppetto.fdl.gameObjects.avatar;

import com.badlogic.gdx.math.Rectangle;

import br.com.geppetto.fdl.core.Map;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;

public class AvatarPresentation extends Avatar{
	
	private CollidableSprite decoy = Factory.INSTANCE.buildAvatarScope();
	
	public AvatarPresentation(int avatarCode, DeepnessHelper deepnessHelper,
			Map map, SymbolicControll symbolicControll) {
		super(avatarCode, deepnessHelper, map, symbolicControll);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void init() {
		super.init();
		updatePosition(-99f, -99f);
	}
	
	@Override
	public void update(float delta) {}
	
	@Override
	public boolean isContained(Rectangle rectangle) {
		return true;
	}

	@Override
	public boolean collided(CollidableSprite c) {
		return false;
	}
	
	@Override
	public void reverse() {}
	
	@Override
	public CollidableSprite getAvatar() {
		decoy.updatePositionX(-99f);
		decoy.updatePositionY(-99f);
		return decoy;
	}
}
