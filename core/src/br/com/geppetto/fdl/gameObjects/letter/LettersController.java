package br.com.geppetto.fdl.gameObjects.letter;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.Map;
import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.digit.Digits;
import br.com.geppetto.utilities.math.Utils;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.screen.renderable.GUIRenderable;
import br.com.geppetto.utilities.screen.renderable.HUDRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class LettersController implements Initializable, Updatable, DebugRenderable, HUDRenderable, GUIRenderable {
	private static final String TAG = LettersController.class.getSimpleName();
	
	private StageFactory stageFactory;
	
	private Array<Letter> letters;
	private Array<Letter> discarted;
	private Avatar avatar;
	private Map map;
	
	private Label label;
	private Label wordLabel;
	private Label current;
	private Array<String> word;
	private int currLetter;
	private static Array<String> validLetters;
	
	private DeepnessHelper deepnessHelper;
	
	private int completedWords;
	private int takenLetters;
	
	private TellWord tellWord;
	private Updatable currTellBehavior;
	
	private Digits digits;
	static {
		validLetters = new Array<>();
		for(char l = 'A'; l <= 'Z'; l++) {
			validLetters.add(Character.toString(l));
		}
		validLetters.add("Á");
		validLetters.add("Â");
		validLetters.add("Ã");
		validLetters.add("É");
		validLetters.add("Ê");
		validLetters.add("Í");
		validLetters.add("Ó");
		validLetters.add("Ô");
		validLetters.add("Õ");
		validLetters.add("Ú");
		validLetters.add("Ç");
	}
	
	public LettersController(StageFactory stageFactory, Avatar avatar, Map map, DeepnessHelper deepnessHelper) {
		this.stageFactory = stageFactory;
		this.avatar = avatar;
		this.map = map;
		this.deepnessHelper = deepnessHelper;
	}
	
	@Override
	public void init() {
		word = new Array<>();
		letters = new Array<>();
		discarted = new Array<>();
		tellWord = new TellWord();
		currTellBehavior = Updatable.NULL_UPDATABLE;
		wordLabel = new Label(Assets.INSTANCE.fonts.label, 
				"EXEMPLO", 0f, 0f, 12f);
		wordLabel.setColor(.125f, .25f, .75f, 1f);
		setNewWord(stageFactory.newWord());
		wordLabel.setPosition(.5f, 1.125f);
		Array<Vector2> spawns = map.getLettersSpawns();
		for(int i = 0; i < spawns.size; i++) {
			addNewLetter();
		}
		label = new Label(Assets.INSTANCE.fonts.buttonBig, 
				"Palavra", 0f, 0f, 10f);
		label.setPosition(.5f, Constants.HUD_VIEWPORT_HEIGHT - (label.getHeight() + .5f));
		label.setColor(.125f, .125f, .375f, 1f);
		current = new Label(Assets.INSTANCE.fonts.label, 
				"", .5f, 1.125f, 12f);
		current.setColor(.25f, .75f, .25f, 1f);
		completedWords = 0;
		takenLetters = 0;
		
		digits = Factory.INSTANCE.buildWordsDigits();
		updateDigits(0);
	}
	
	private void updateDigits(int value) {
		digits.updateByCenter(value, Constants.VIEWPORT_WIDTH / 2f, Constants.VIEWPORT_HEIGHT - 3.5f);
	}
	
	// Words with at most 13 letters
	private void setNewWord(String newWord) {
		wordLabel.setText(newWord);
		this.word.clear();
		char[] word2 = newWord.toCharArray();
		for(char c : word2) {
			this.word.add(Character.toString(c));
		}
		currLetter = 0;
	}
	
	public void addNewLetter() {
		Vector2 spawn = null;
		
		// With this code I will find the spawn farer from
		// any letter or avatar
		Array<Vector2> spawns = map.getLettersSpawns();
		float farMostSpawn = 0f;
		for(Vector2 s : spawns) {
			float nearestLetter = Utils.distance(s.x, s.y, avatar.getX(), avatar.getY());
			for(Letter l : letters) {
				float dist = Utils.distance(s.x, s.y, 
						l.getX(), l.getY());
				nearestLetter = Math.min(dist, nearestLetter);
			}
			if(farMostSpawn < nearestLetter) {
				spawn = s;
				farMostSpawn = nearestLetter;
			}
		}
		////////////////////////////////////////
		
		// "already" talks about if the chasing
		// letter from the "word" is already
		// among those spawned
		boolean already = false;
		String l = word.get(currLetter);
		for(Letter letter : letters) {
			if(letter.getLetter().equalsIgnoreCase(l)) {
				already = true;
				break;
			}
		}
		Letter letter = null;
		if(already)
			letter = new Letter(avatar, this, validLetters.random());
		else
			letter = new Letter(avatar, this, word.get(currLetter));
		letter.init();
		deepnessHelper.addAnalybable(letter);
		letter.updatePosition(spawn.x, spawn.y);
		letters.add(letter);
	}

	@Override
	public void update(float delta) {
		currTellBehavior.update(delta);
		for(int i = 0; i < letters.size; i++)
			letters.get(i).update(delta);
		for(int i = 0; i < discarted.size; i++)
			discarted.get(i).update(delta);
		for(int i = 0; i < letters.size; i++) {
			final Letter l = letters.get(i);
			if(l.colided(avatar.getAvatar())) {
				if(word.get(currLetter).equalsIgnoreCase(l.getLetter())) {
					// It is right!
					String letter = word.get(currLetter);
					avatar.addLetter(letter, l.getAvatarCode());
					takenLetters++;
					updateDigits(takenLetters);
					Assets.INSTANCE.mp3.getLetter(letter).play();
				} else {
					//Wrong
					avatar.looseLetters();
					SoundManager.INSTANCE.playWrongLetter();
				}
				final Letter lr = letters.get(i);
				discarted.add(lr);
				lr.onElimination(new Runnable() {
					@Override
					public void run() {
						deepnessHelper.removeAnalyzable(lr);
						discarted.removeValue(lr, true);
					}
				});
				letters.removeIndex(i);
				addNewLetter();
			}
		}
	}
	
	public void setCurrLetter(int currLetter) {
		this.currLetter = currLetter;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < currLetter; i++) {
			sb.append(word.get(i));
		}
		current.setText(sb.toString());
		if(currLetter == word.size) {
			wordDone();
			return;
		}
		boolean present = false;
		for(int i = 0; i < letters.size; i++) {
			if(letters.get(i).getLetter().equalsIgnoreCase(word.get(currLetter))) {
				present = true;
				break;
			}
		}
		if(!present) {
			letters.first().setLetter(word.get(currLetter));
		}
	}
	
	public void wordDone() {
		Gdx.app.log(TAG, "Done!");
		tellWord.setWord(wordLabel.getText());
		currTellBehavior = tellWord;
		setNewWord(stageFactory.newWord());
		avatar.looseLetters();
		completedWords++;
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		for(Letter l : letters)
			l.renderDebug(shapeRenderer);
	}
	
	public Array<Letter> getLetters() {
		return letters;
	}
	
	public String getCurrLetter() {
		return word.get(currLetter);
	}

	@Override
	public void renderHUD(SpriteBatch batch) {
		label.render(batch);
		wordLabel.render(batch);
		current.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		tellWord.render(batch);
		batch.setColor(1f, 1f, 1f, .75f);
		digits.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}
	
	public int getCompletedWords() {
		return completedWords;
	}
	
	public int getTakenLetters() {
		return takenLetters;
	}
	
	private class TellWord implements Updatable, Renderable {
		private String word;
		private float timeLetter = 1.35f;
		private float timeWord;
		private int currLetter = -1;
		private float alpha;
		private Label labelOnScreen;
		
		private boolean render;
		
		public TellWord() {
			labelOnScreen = new Label();
			labelOnScreen.setFont(Assets.INSTANCE.fonts.onScreenLetter);
			labelOnScreen.setColor(.125f, .125f, .375f, 1f);
			labelOnScreen.setTextWidth(Constants.VIEWPORT_WIDTH);
		}
		
		@Override
		public void update(float delta) {
			timeLetter -= delta;
			if(timeWord > 0f) {
				alpha = Math.max(alpha - delta / 2f, 0f);
				timeWord -= delta;
				if(timeWord <= 0f) {
					currTellBehavior = Updatable.NULL_UPDATABLE;
					currLetter = -1;
					timeLetter = 1.35f;
					labelOnScreen.setFont(Assets.INSTANCE.fonts.onScreenLetter);
					render = false;
				}
			} else if(timeLetter <= 0f) {
				currLetter++;
				if(currLetter == word.length()) {
					alpha = 1f;
					labelOnScreen.setFont(Assets.INSTANCE.fonts.onScreenWord);
					labelOnScreen.setText(word);
					updateLabelPosition();
					Assets.INSTANCE.mp3.getWord(word).play();
					timeWord = 2f;
				} else {
					alpha = 1f;
					String letter = word.substring(currLetter, currLetter + 1);
					labelOnScreen.setText(letter);
					updateLabelPosition();
					Assets.INSTANCE.mp3.getLetter(letter).play();
					timeLetter = .675f;
					render = true;
				}
			} else {
				alpha = Math.max(alpha - delta, 0f);
			}
			
			labelOnScreen.setAlpha(alpha);
		}
		
		private void updateLabelPosition() {
			labelOnScreen.setPosition(
			Constants.VIEWPORT_WIDTH / 2f - labelOnScreen.getWidth() / 2f,
			Constants.VIEWPORT_HEIGHT / 2f - labelOnScreen.getHeight() / 2f);
		}
		
		public void setWord(String word) {
			this.word = word;
		}

		@Override
		public void render(SpriteBatch batch) {
			if(render)
				labelOnScreen.render(batch);
		}
	}
}