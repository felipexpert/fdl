package br.com.geppetto.fdl.gameObjects.letter;

import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.fdl.gameObjects.BasicLetter;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.CoordnateAnalyzable;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.math.Utils;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.renderable.DebugRenderable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Letter implements Initializable, Updatable, Renderable, DebugRenderable, CoordnateAnalyzable {
	private static final float AVATAR_DISTANCE = 7f;
	private static final float LETTER_DISTANCE = 4f;
	private static final float NEW_RANDOM_DIRECTION = 3f;
	private static final float VELOCITY = 3.5f;
	private static final float THINK_WATING_TIME = .125f;
	
	private BasicLetter basicLetter;
	private float lastChange;
	private float timeToThink = 0f;
	private Avatar avatar;
	//private Array<Letter> letters;
	private LettersController lettersController;
	
	private String letter;
	
	public Letter(Avatar avatar, LettersController letters, String letter) {
		this.avatar = avatar;
		this.lettersController = letters;
		this.letter = letter;
	}
	
	@Override
	public void init() {
		basicLetter = new BasicLetter(letter, VELOCITY, MathUtils.random(101, 104));
		basicLetter.setVelocity(VELOCITY);
		basicLetter.init();
		lastChange = 0f;
	}
	
	public void updatePosition(float x, float y) {
		basicLetter.updatePosition(x, y);
	}
	
	private void think() {
		float avatDist = Utils.distance(basicLetter.getX(), 
				basicLetter.getY(), avatar.getX(), avatar.getY());
		if(avatDist <= AVATAR_DISTANCE) {
			if(lettersController.getCurrLetter().equalsIgnoreCase(basicLetter.getLetter()))
				escape(avatar.getX(), avatar.getY(), true);
			else
				chase(avatar.getX(), avatar.getY(), true);
			return;
		}
		
		Letter nearestLetter = null;
		float letterDist = 9999f;
		for(Letter l : lettersController.getLetters()) {
			if(l == this) continue;
			float dist = Utils.distance(basicLetter.getX(), 
					basicLetter.getY(), l.basicLetter.getX(), l.basicLetter.getY());
			if(dist < letterDist) {
				nearestLetter = l;
				letterDist = dist;
			}
		}
		
		if(letterDist <= LETTER_DISTANCE) {
			escape(nearestLetter.basicLetter.getX(), nearestLetter.basicLetter.getY(), true);
			return;
		}
		
		if(lastChange >= NEW_RANDOM_DIRECTION) {
			setDirection(basicLetter.getDirection().plus(MathUtils.random(1, Direction.values().length - 1)));
		}
	}
	
	private void chase(float enemyX, float enemyY, boolean canGoSameDirection) {
		float distX = basicLetter.getX() - enemyX;
		float distY = basicLetter.getY() - enemyY;
		if(!canGoSameDirection) {
			if(basicLetter.getDirection().horizontal)
				distX = 0f;
			else
				distY = 0f;
		}
		if(Math.abs(distX) > Math.abs(distY)) { // Must choose horizontal
			if(distX <= 0) {
				setDirection(Direction.EAST);
			} else {
				setDirection(Direction.WEST);
			}
		} else { // Must choose vertical 
			if(distY <= 0) {
				setDirection(Direction.NORTH);
			} else {
				setDirection(Direction.SOUTH);
			}
		}
	}
	
	private void escape(float enemyX, float enemyY, boolean canGoSameDirection) {
		float distX = basicLetter.getX() - enemyX;
		float distY = basicLetter.getY() - enemyY;
		if(!canGoSameDirection) {
			if(basicLetter.getDirection().horizontal)
				distX = 0f;
			else
				distY = 0f;
		}
		if(Math.abs(distX) > Math.abs(distY)) { // Must choose horizontal
			if(distX >= 0) {
				setDirection(Direction.EAST);
			} else {
				setDirection(Direction.WEST);
			}
		} else { // Must choose vertical 
			if(distY >= 0) {
				setDirection(Direction.NORTH);
			} else {
				setDirection(Direction.SOUTH);
			}
		}
	}
	
	private void setDirection(Direction direction) {
		basicLetter.setDirection(direction);
		lastChange = 0f;
	}
	
	public boolean isContained(Rectangle rectangle) {
		return basicLetter.isContained(rectangle);
	}
	
	public void turn() {
		float avatDist = Utils.distance(basicLetter.getX(), 
				basicLetter.getY(), avatar.getX(), avatar.getY());
			if(timeToThink <= 0f && avatDist <= AVATAR_DISTANCE) {
				if(lettersController.getCurrLetter() == basicLetter.getLetter())
					escape(avatar.getX(), avatar.getY(), false);
				else
					chase(avatar.getX(), avatar.getY(), false);
			} else {
				setDirection(basicLetter.getDirection().plus(MathUtils.randomBoolean() ? 1 : -1));
			}
			timeToThink = THINK_WATING_TIME * 5f;
	}
	
	public boolean colided(CollidableSprite c) {
		return basicLetter.collided(c);
	}
	
	public String getLetter() {
		return basicLetter.getLetter();
	}
	
	public void setLetter(String letter) {
		basicLetter.setLetter(letter);
	}

	@Override
	public void update(float delta) {
		lastChange += delta;
		timeToThink -= delta;
		if(timeToThink <= 0f) {
			think();
			timeToThink = THINK_WATING_TIME;
		}
		basicLetter.update(delta);
	}
	
	@Override
	public float getX() {
		return basicLetter.getX();
	}
	
	@Override
	public float getY() {
		return basicLetter.getY();
	}

	@Override
	public void render(SpriteBatch batch) {
		basicLetter.render(batch);
	}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		basicLetter.renderDebug(shapeRenderer);
	}
	
	public CollidableSprite getCollidableLetter() {
		return basicLetter.getCollidableLetter();
	}
	
	public int getAvatarCode() {
		return basicLetter.getAvatarCode();
	}
	
	public void onElimination(Runnable runnable) {
		basicLetter.onElimination(runnable);
	}
}
