package br.com.geppetto.fdl.screen;

import java.util.Map;
import java.util.TreeMap;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.GameController;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.noncore.AccountController;
import br.com.geppetto.fdl.noncore.AcknowledgementsController;
import br.com.geppetto.fdl.noncore.AvatarController;
import br.com.geppetto.fdl.noncore.ConfigurationController;
import br.com.geppetto.fdl.noncore.NewUserController;
import br.com.geppetto.fdl.noncore.PopUpController;
import br.com.geppetto.fdl.noncore.StageController;
import br.com.geppetto.fdl.noncore.StoryboardController;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.screen.AbstractGameScreen;
import br.com.geppetto.utilities.screen.DirectedGame;
import br.com.geppetto.utilities.screen.templates.GameScreen;
import br.com.geppetto.utilities.screen.templates.HUDGameScreen;
import br.com.geppetto.utilities.screen.templates.MenuController;
import br.com.geppetto.utilities.screen.templates.MenuScreen;
import br.com.geppetto.utilities.screen.templates.PopUpScreen;
import br.com.geppetto.utilities.screen.templates.StagedGameScreen;
import br.com.geppetto.utilities.screen.transition.ScreenTransition;
import br.com.geppetto.utilities.screen.transition.ScreenTransitionFade;
import br.com.geppetto.utilities.screen.transition.ScreenTransitionSlice;
import br.com.geppetto.utilities.screen.transition.ScreenTransitionSlide;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;

public class ScreenManager {
	public final static ScreenManager INSTANCE = new ScreenManager();
	
	private DirectedGame game;
	
	private ScreenManager(){}
	
	public void init(DirectedGame game) {
		this.game = game;
	}
	
	public void setGameScreen(int stageCode, int seconds, int avatarCode) {
		Color color = new Color(193f / 255f, 156f / 255f, 25f  / 255f, 1f);
		GameController controller = new GameController(stageCode, seconds, avatarCode);
		HUDGameScreen hud = new HUDGameScreen(color, controller, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT, Constants.VIEWPORT_WIDTH, Constants.HUD_VIEWPORT_HEIGHT, Constants.HUD_FACTOR, ConfigPersistence.INSTANCE.isDebug());
		game.setScreen(hud, ScreenTransitionSlice.init(1f, ScreenTransitionSlice.UP_DOWN, 8, Interpolation.bounce));
	}
	
	public void setPresentationScreen(boolean forward) {
		Color color = new Color(193f / 255f, 156f / 255f, 25f  / 255f, 1f);
		GameController controller = Factory.INSTANCE.buildPresentationController(false);
		GameScreen hud = new GameScreen(color, controller, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT, false);
		ScreenTransition st = forward ? ScreenTransitionFade.init(.5f) : 
			ScreenTransitionSlide.init(.5f, ScreenTransitionSlide.RIGHT, true, Interpolation.bounce); 
		game.setScreen(hud, st);
		//game.setScreen(hud, ScreenTransitionSlide.init(.5f, ScreenTransitionSlide.RIGHT, true, Interpolation.bounce));
		Gdx.input.setInputProcessor(null);
		
		if(!forward)	SoundManager.INSTANCE.playSlipper();
	}
	
	public void setCreditsScreen(boolean forward) {
		MenuController controller = new AcknowledgementsController();
		setGenericMenuSlide(Color.GREEN, controller, forward);
	}
	
	public void setAccountScreen(boolean forward) {
		Color color = new Color(0f, 1f, 0f, 1f);
		setGenericMenuSlide(color, new AccountController(), forward);
	}
	
	public void setNewUserScreen(boolean forward) {
		Color color = new Color(0f, 1f, 0f, 1f);
		setGenericMenuSlide(color, new NewUserController(), forward);
	}
	
	public void setStageScreen(int avatarCode, int stageCode) {
		setGenericMenuFade(Color.GREEN, new StageController(avatarCode, stageCode));
	}
	
	public void setStageScreen(int avatarCode, boolean forward) {
		setGenericMenuSlide(Color.GREEN, new StageController(avatarCode, 1), forward);
	}
	
	public void setStoryboard(int stageCode) {
		StoryboardController controller = new StoryboardController(stageCode);
		controller.setUnderScreen(game.getCurrScreen());
		MenuScreen screen = new MenuScreen(Color.BLACK, controller, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		setScreen(screen, true);
	}
	
	public void setAvatarScreen(int avatarCode) {
		setGenericMenuFade(Color.GREEN, new AvatarController(avatarCode));
	}
	
	public void setAvatarScreen(boolean forward) {
		setGenericMenuSlide(Color.GREEN, new AvatarController(1), forward);
	}
	
	public void setMessagePopUpScreen(String text, String confirmMessage) {
		setMessagePopUpScreen(text, confirmMessage, Function.NULL_FUNCTION);
	}
	
	public void setMessagePopUpScreen(String text, String confirmMessage, Function confirmFunction) {
		Map<Button, Clickable> options = new TreeMap<>();
		Button confirm = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, confirmMessage);
		Clickable confirmC = new Clickable(confirm, confirmFunction);
		options.put(confirm, confirmC);
		setPopUpScreen(text, options);
	}
	
	public void setConfirmPopUpScreen(String text, String cancelMessage, Function cancelFunction, String confirmMessage, Function confirmFunction) {
		Map<Button, Clickable> options = new TreeMap<>();
		Button cancel = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, cancelMessage);
		Clickable cancelC = new Clickable(cancel, cancelFunction);
		Button confirm = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, confirmMessage);
		Clickable confirmC = new Clickable(confirm, confirmFunction);
		options.put(cancel, cancelC);
		options.put(confirm, confirmC);
		setPopUpScreen(text, options);
	}
	
	public void setPopUpScreen(String text, Map<Button, Clickable> options) {
		PopUpController controller = new PopUpController(text, options);
		game.setScreen(new PopUpScreen(game.getCurrScreen(), controller, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT));
		SoundManager.INSTANCE.playAlert();
	}
	
	public void setScreen(AbstractGameScreen screen) {
		game.setScreen(screen);
	}
	
	public void setScreen(AbstractGameScreen screen, boolean forward) {
		int direction = forward ? ScreenTransitionSlide.LEFT : ScreenTransitionSlide.RIGHT;
		game.setScreen(screen, ScreenTransitionSlide.init(.5f, direction, !forward, Interpolation.bounce));
		
		if(forward) SoundManager.INSTANCE.playRoller();
		else		SoundManager.INSTANCE.playSlipper();
	}
	
	public void setConfigScreen() {
		ConfigurationController controller = new ConfigurationController();
		StagedGameScreen screen = new StagedGameScreen(Color.GREEN, 
				controller, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT, false);
		setScreen(screen, true);
	}
	
	private void setGenericMenuSlide(Color color, MenuController gmc, boolean forward) {
		MenuScreen menuScreen = new MenuScreen(color, gmc, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		int direction = forward ? ScreenTransitionSlide.LEFT : ScreenTransitionSlide.RIGHT;
		game.setScreen(menuScreen, ScreenTransitionSlide.init(.5f, direction, !forward, Interpolation.bounce));
		
		if(forward) SoundManager.INSTANCE.playRoller();
		else		SoundManager.INSTANCE.playSlipper();
	}
	
	private void setGenericMenuFade(Color color, MenuController gmc) {
		MenuScreen menuScreen = new MenuScreen(color, gmc, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		game.setScreen(menuScreen, ScreenTransitionFade.init(.5f));
	}
}
