package br.com.geppetto.fdl.noncore;

import java.util.List;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.storyboard.StoryboardAssets;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.core.stage.storyboard.StoryboardPage;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.screen.AbstractGameScreen;
import br.com.geppetto.utilities.screen.templates.GenericPopUpController;
import br.com.geppetto.utilities.screen.templates.MenuController;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StoryboardController implements MenuController, ClickAt {
	private ClickAtHandler handler;
	private List<StoryboardPage> pages;
	private int currIndex;
	private float alpha;
	private Clickable next;
	private Button btnNext;
	private AbstractGameScreen underScreen;
	
	private int stageCode;

	public StoryboardController(int stageCode) {
		this.stageCode = stageCode;
	}
	
	private void updateBtnNextPosition() {
		btnNext.setPosition(Constants.VIEWPORT_WIDTH - btnNext.getWidth(),
				Constants.VIEWPORT_HEIGHT - btnNext.getHeight());
	}
	
	@Override
	public void init() {
		StoryboardAssets.INSTANCE.init(new AssetManager());
		pages = StageUtilities.INSTANCE.getStoryPages(stageCode);
		handler = new ClickAtHandler(this);
		alpha = 1f;
		btnNext = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonBig,
				"Próximo");
		updateBtnNextPosition();
		next = new Clickable(btnNext, new Function() {
			@Override
			public void run(Clickable source) {
				alpha = 1f;
				if(currIndex == pages.size() - 2) {
					btnNext.setText("Fim");
					updateBtnNextPosition();
				} else if(currIndex == pages.size() - 1) {
					ScreenManager.INSTANCE.setScreen(underScreen, false);
					return;
				}
				currIndex++;
			}
		});
		for(StoryboardPage p : pages)
			p.init();
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		pages.get(currIndex).render(batch);
		if(currIndex > 0) {
			batch.setColor(1f, 1f, 1f, alpha);
			pages.get(currIndex - 1).renderPic(batch);
			batch.setColor(1f, 1f, 1f, 1f);
		}
		btnNext.render(batch);
	}

	@Override
	public void update(float delta) {
		alpha = Math.max(alpha - delta, 0f);
		handler.update();
	}

	@Override
	public void clickAt(float x, float y) {
		next.testClick(x, y);
	}

	public void setUnderScreen(AbstractGameScreen screen) {
		this.underScreen = screen;
	}

	@Override
	public void hide() {
		StoryboardAssets.INSTANCE.dispose();
	}

}
