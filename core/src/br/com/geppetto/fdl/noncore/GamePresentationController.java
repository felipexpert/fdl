package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.List;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.GameController;
import br.com.geppetto.fdl.core.Map;
import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.util.CameraHelper;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.core.util.HandlerHelper;
import br.com.geppetto.fdl.core.util.HandlerHelperPresentation;
import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.SymbolicControllPresentation;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.fdl.gameObjects.avatar.AvatarPresentation;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GamePresentationController extends GameController{
	private static final String TAG = GamePresentationController.class.getSimpleName();
	private ClickAtHandler handler;
	private List<Renderable> renderables;
	private List<Clickable> clickables;
	
	public GamePresentationController(int stageCode) {
		super(stageCode, 99999, 1);
	}
	
	@Override
	public void init() {
		super.init();
		handler = new ClickAtHandler(new ClickHelper());
		renderables = new ArrayList<>();
		clickables = new ArrayList<>();
		Label title = Factory.INSTANCE.buildScreenTitles("Fila das\nLetras");
		renderables.add(title);
		Button button = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.game, "PROSSEGUIR!");
		button.setPosition(Constants.VIEWPORT_WIDTH / 2f - (button.getWidth() / 2f), 
				Constants.VIEWPORT_HEIGHT / 2f - 4f);
		button.setPadding(.5f);
		Clickable clickable = new Clickable(button, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setAccountScreen(true);
			}
		});
		Sprite exit = Factory.INSTANCE.buildExit();
		Clickable exitClickable = new Clickable(exit, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setConfirmPopUpScreen("Tem certeza que quer sair?", 
						"NÃO!", 
						Function.NULL_FUNCTION, 
						"SAIR", 
						new Function() {
							@Override
							public void run(Clickable source) {
								Gdx.app.exit();
							}
						});
			}
		});
		Button btnThanks = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, "Créditos");
		btnThanks.setPosition(Constants.VIEWPORT_WIDTH / 2f - (btnThanks.getWidth() / 2f), 
				.5f);
		Clickable thanksC = new Clickable(btnThanks, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setCreditsScreen(true);
			}
		});
		Sprite gear = Factory.INSTANCE.buildGear();
		gear.setX(Constants.VIEWPORT_WIDTH - (gear.getWidth() + .25f));
		gear.setY(.25f);
		Clickable gearC = new Clickable(gear, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setConfigScreen();
			}
		});
		clickables.add(gearC);
		Button howToPlay = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, "Como jogar");
		howToPlay.setPosition(btnThanks.getBounds().x, btnThanks.getBounds().y + 2f);
		renderables.add(gear);
		renderables.add(exit);
		clickables.add(exitClickable);
		renderables.add(button);
		clickables.add(clickable);
		renderables.add(btnThanks);
		clickables.add(thanksC);
		renderables.add(howToPlay);
	}
	
	private class ClickHelper implements ClickAt {
		@Override
		public void clickAt(float x, float y) {
			for(Clickable c : clickables)
				c.testClick(x, y);
		}
	}

	@Override
	protected Avatar buildAvatar(int avatarCode, DeepnessHelper deepnessHelper,
			Map map, SymbolicControll symbolicControll) {
		// TODO Auto-generated method stub
		return new AvatarPresentation(avatarCode, deepnessHelper, map, symbolicControll);
	}
	
	@Override
	protected void setCameraHelperTarget() {
		randomTarget();
	}
	
	@Override
	protected SymbolicControll buildSymbolicControll() {
		return new SymbolicControllPresentation();
	}
	
	@Override
	protected HandlerHelper buildHandlerHelper() {
		return new HandlerHelperPresentation();
	}
	
	@Override
	public void renderGUI(SpriteBatch batch) {
		for(Renderable r : renderables)
			r.render(batch);
	}
	@Override
	public void update(float delta) {
		handler.update();
		super.update(delta);
	}
}
