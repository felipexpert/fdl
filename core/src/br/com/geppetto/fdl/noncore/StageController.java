package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.storyboard.StoryboardAssets;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.fdl.stage.Time;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.asset.FontWrapper;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.templates.MenuController;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StageController implements MenuController, ClickAt{
	private static final int MAX_ITEMS = 2;
	private List<Renderable> renderables;
	private List<Renderable> green;
	private List<Renderable> red;
	private List<Clickable> clickables;
	private GamePresentationController presentation;
	private ClickAtHandler handler;
	private final int stageCode;
	private int avatarCode;
	
	public StageController(int avatarCode, int stageCode) {
		this.avatarCode = avatarCode;
		this.stageCode = stageCode;
	}
	
	@Override
	public void init() {
		renderables = new ArrayList<>();
		green = new ArrayList<>();
		red = new ArrayList<>();
		clickables = new ArrayList<>();
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		handler = new ClickAtHandler(this);
		Label title = Factory.INSTANCE.buildScreenTitles("Estágios");
		renderables.add(title);
		
		
		Button back = ButtonFactory.INSTANCE.genBackButton();
		Clickable buttonC = new Clickable(back, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setAvatarScreen(false);
			}
		});
		renderables.add(back);
		clickables.add(buttonC);
		
		// Programation to load stage clickables;
		Set<Integer> playerStages = StageUtilities.INSTANCE.getOpenedStages();
		Set<Integer> stages = StageUtilities.INSTANCE.getStageCodes();
		Set<Integer> storyboards = StageUtilities.INSTANCE.getStoryboardsCodes();
		int pics = Math.min(MAX_ITEMS, stages.size() - (stageCode - 1));
		float y = title.getY() + 1.5f;
		final int initialStageCode = stageCode;
		int initStageCode = stageCode;
		for(int i = 0; i < pics; i++, initStageCode++) {
			final int localStageCode = initStageCode;
			float x = Constants.VIEWPORT_WIDTH / 2f;
			Sprite bg = Factory.INSTANCE.buildThumbnailBG();
			Sprite thumbnail = StageUtilities.INSTANCE.getThumbNail(localStageCode);
			Label name = Factory.INSTANCE.buildNameLabel(
					StageUtilities.INSTANCE.getStageName(localStageCode), 
					12f);
			y -= bg.getHeight() + name.getHeight() + 2.25f;
			bg.setCenterX(x);
			bg.setY(y);
			thumbnail.setCenterX(bg.getCenterX());
			thumbnail.setCenterY(bg.getCenterY());
			name.setPosition(bg.getX() + bg.getWidth() / 2f
					- name.getWidth() / 2f, y + bg.getHeight() + .125f);
			renderables.add(name);
			renderables.add(thumbnail);
			if(playerStages.contains(localStageCode)) {
				// Unlocked
				green.add(bg);
				Clickable c = new Clickable(bg, new Function() {
					@Override
					public void run(Clickable source) {
						Map<Button, Clickable> map= new TreeMap<>();
						addTimeToMap(map, Time.SHORT, localStageCode);
						addTimeToMap(map, Time.MEDIUM, localStageCode);
						addTimeToMap(map, Time.LONG, localStageCode);
						Button btnBack = ButtonFactory.INSTANCE.
								genTypeLetters(Assets.INSTANCE.fonts.buttonNormal, "Voltar");
						Clickable cBack = new Clickable(btnBack, Function.NULL_FUNCTION);
						map.put(btnBack, cBack);
						ScreenManager.INSTANCE.setPopUpScreen("Qual modalidade de tempo?",
								map);
					}
				});
				clickables.add(c);
				if(storyboards.contains(localStageCode)) {
					Button watch = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonBig
							, "Assistir");
					watch.setPosition(bg.getCenterX() - watch.getWidth() / 2f, bg.getY() - watch.getHeight());
					Clickable watchC = new Clickable(watch, 
							new Function() {
								@Override
								public void run(Clickable source) {
									ScreenManager.INSTANCE.setStoryboard(localStageCode);
								}
							});
					clickables.add(watchC);
					renderables.add(watch);
				}
			} else {
				// locked
				red.add(bg);
				final int previousStage = initStageCode - 1;
				Clickable c = new Clickable(bg, new Function() {
					@Override
					public void run(Clickable source) {
						ScreenManager.INSTANCE.setMessagePopUpScreen
						("Você precisa de mais " + 
						 StageUtilities.INSTANCE.getPointsToNext(previousStage) + 
						 " palavra(s) no estágio anterior para abrir este estágio", 
						"OK");
					}
				});
				clickables.add(c);
			}
		}
		
		// To Browse through the stages;
		int screenNum = initialStageCode / MAX_ITEMS + 1;
		
		if(screenNum > 1) {
			Button previous = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal,
					"Página " + (screenNum - 1));
			previous.setPosition(Constants.VIEWPORT_WIDTH -
					(previous.getWidth() * 2f + .25f), 0f);
			Clickable c = new Clickable(previous, new Function() {
				@Override
				public void run(Clickable source) {
					ScreenManager.INSTANCE.setStageScreen(avatarCode, initialStageCode - MAX_ITEMS);
				}
			});
			clickables.add(c);
			renderables.add(previous);
			// btnBack
		} 
		if(initStageCode <= stages.size()) {
			Button next = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal,
					"Página" + (screenNum + 1));
			next.setPosition(Constants.VIEWPORT_WIDTH -
					(next.getWidth() + .25f), 0f);
			final int ls = initStageCode;
			Clickable c = new Clickable(next, new Function() {
				@Override
				public void run(Clickable source) {
					ScreenManager.INSTANCE.setStageScreen(avatarCode, ls);
				}
			});
			clickables.add(c);
			renderables.add(next);
		}
	}
	
	private void addTimeToMap(Map<Button, Clickable> map, final Time time, final int stageCode) {
		FontWrapper font = Assets.INSTANCE.fonts.buttonNormal;
		String text = time.minutes + ":00";
		if(time.minutes < 10)
			text = "0" + text; 
		Button bS = ButtonFactory.INSTANCE.genTypeLetters
				(font, text);
		Clickable cS = new Clickable(bS, 
				new Function() {

					@Override
					public void run(Clickable source) {
						UserPersistence.INSTANCE.getLastUserAccount().setLastStagePlayed(stageCode);
						UserPersistence.INSTANCE.save();
						ScreenManager.INSTANCE.setGameScreen(stageCode, time.minutes * 60, avatarCode);
						SoundManager.INSTANCE.playLastStagePlayedTheme();
					}
				});
		map.put(bS, cS);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		batch.setColor(0f, .75f, 0f, 1f);
		for(Renderable r : green)
			r.render(batch);
		batch.setColor(.75f, 0f, 0f, 1f);
		for(Renderable r : red)
			r.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
		for(Renderable r : renderables)
			r.render(batch);
	}

	@Override
	public void update(float delta) {
		handler.update();
		presentation.update(delta);
	}

	@Override
	public void clickAt(float x, float y) {
		for(Clickable c : clickables)
			c.testClick(x, y);
	}

	@Override
	public void hide() {}

}
