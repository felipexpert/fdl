package br.com.geppetto.fdl.noncore;

import java.util.Map;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.PopUp;
import br.com.geppetto.utilities.screen.AbstractGameScreen;
import br.com.geppetto.utilities.screen.templates.GenericPopUpController;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PopUpController implements GenericPopUpController{

	private AbstractGameScreen underScreen;
	private String text;
	private Map<Button, Clickable> options;
	private PopUp popUp;
	
	public PopUpController(String text, Map<Button, Clickable> options) {
		this.text = text;
		this.options = options;
	}
	
	@Override
	public void init() {
		popUp = new PopUp(Factory.INSTANCE.buildPopUp(), 
				Assets.INSTANCE.fonts.labelSmall, 
				text, 
				Constants.VIEWPORT_WIDTH - 2f, 
				options, 
				1f, 
				.25f);
		popUp.arrangeAt(1f, 4f);
		for(Button b : options.keySet()) {
			b.setColor(.125f, .125f, .375f, 1f);
			Clickable c = options.get(b);
			final Function incomplete = c.getFunction();
			Function complete = new Function() {
				@Override
				public void run(Clickable clickable) {
					underScreen.pause();
					ScreenManager.INSTANCE.setScreen(underScreen);
					incomplete.run(clickable);
				}
			};
			c.setFunction(complete);
		}
	}
	
	@Override
	public void renderGUI(SpriteBatch batch) {
		popUp.render(batch);
	}

	@Override
	public void update(float delta) {
		popUp.update(delta);
	}

	@Override
	public void setUnderScreen(AbstractGameScreen screen) {
		underScreen = screen;
	}

	@Override
	public void hide() {}

}
