package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.persistence.UserAccount;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.asset.FontWrapper;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.templates.MenuController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class NewUserController implements MenuController, Function {
	private static final String TAG = NewUserController.class.getSimpleName();
	private static final float PADDING = .25f;
	private static final float NEW_LINE = 2.25f;
	private static final float MAX_CHARACTERS = 25f;
	
	Label label;
	private Map<Character, Clickable> buttons;
	private List<Renderable> renderables;
	
	private Clickable delete;
	private Clickable save;
	private Clickable cancel;
	private ClickAtHandler clickHandler;
	
	private float x;
	private float y;
	
	private GamePresentationController presentation;
	
	@Override
	public void init() {
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		label = new Label();
		label.setFont(Assets.INSTANCE.fonts.label);
		label.setTextWidth(Constants.VIEWPORT_WIDTH - PADDING * 2f);
		label.setColor(0f, 0f, 0f, 1f);
		label.setText("");
		renderables = new ArrayList<>();
		buttons = new HashMap<>();
		x = .25f;
		y = Constants.VIEWPORT_HEIGHT - 5f;
		for(char i = 'A'; i <= 'Z'; i++) {
			putItem(i);
		}
		putItem('Ã');
		putItem('Á');
		putItem('À');
		putItem('Â');
		putItem('É');
		putItem('Ê');
		putItem('Í');
		putItem('Ó');
		putItem('Õ');
		putItem('Ô');
		putItem('Ú');
		for(char i = '0'; i <= '9'; i++) {
			putItem(i);
		}
		putItem('_');
		x = PADDING;
		y -= NEW_LINE;
		
		FontWrapper wrapper = Assets.INSTANCE.fonts.buttonNormal;
		Button delete = ButtonFactory.INSTANCE.genTypeLetters(wrapper, "Corrigir");
		delete.setPosition(x, y);
		renderables.add(delete);
		Clickable deleteClickable = new Clickable(delete, this);
		this.delete = deleteClickable;
		x += delete.getWidth() + PADDING;
		
		Button save = ButtonFactory.INSTANCE.genTypeLetters(wrapper, "Salvar");
		save.setPosition(x, y);
		renderables.add(save);
		Clickable saveClickable = new Clickable(save, this);
		this.save = saveClickable;
		x += save.getWidth() + PADDING;
		Button cancel = ButtonFactory.INSTANCE.genTypeLetters(wrapper, "Cancelar");
		cancel.setPosition(x, y);
		renderables.add(cancel);
		Clickable cancelClickable = new Clickable(cancel, this);
		this.cancel = cancelClickable;	
		clickHandler = new ClickAtHandler(new ClickHelper());
	}
	
	private class ClickHelper implements ClickAt {
		@Override
		public void clickAt(float x, float y) {
			if(delete.testClick(x, y) ||
				save.testClick(x, y) ||
				cancel.testClick(x, y)) {
				return;
			}
			Set<Character> keys = buttons.keySet();
			for(Character c : keys) {
				if(buttons.get(c).testClick(x, y))
					return;
			}
		}
	}
	
	private void putItem(char e) {
		Button b = addButton(e);
		float next = x + b.getWidth() + PADDING;
		if(next + b.getWidth()> Constants.VIEWPORT_WIDTH) {
			x = .25f;
			y -= NEW_LINE;
		} else {
			x = next;
		}
	}
	
	private Button addButton(char e) {
		Button button = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.label, Character.toString(e));
		button.setColor(0f, 0f, 0f, 1f);
		button.setPosition(x, y);
		renderables.add(button);
		Clickable clickable = new Clickable(button, this);
		buttons.put(e, clickable);
		return button;
	}
	
	@Override
	public void run(Clickable clickable) {
		String text = label.getText();
		if(clickable == delete) {
			if(text.length() > 0)
				//text.deleteCharAt(text.length() - 1);
				label.setText(text.substring(0, text.length() - 1));
			return;
		} else if(clickable == save) {
			Gdx.app.log(TAG, "Saving...");
			if(text.trim().length() > 0) {
				String name = text;
				Set<String> names = UserPersistence.INSTANCE.getUserNames();
				if(names.contains(name)) {
					ScreenManager.INSTANCE.setMessagePopUpScreen
					("Este nome de usuário já está cadastrado...", "OK");
					return;
				}
				UserAccount account = new UserAccount();
				account.setName(name);
				UserPersistence.INSTANCE.save(account);
				Gdx.app.log(TAG, "User " + name + " has been saved");
				ScreenManager.INSTANCE.setAccountScreen(false);
			} else {
				ScreenManager.INSTANCE.setMessagePopUpScreen("Nome de usuário inválido", "OK");
			}
			return;
		} else if(clickable == cancel) {
			if(text.length() > 0) {
			ScreenManager.INSTANCE.setConfirmPopUpScreen
			("Você realmente quer cancelar?", 
					"Não!", 
					Function.NULL_FUNCTION, 
					"Sim", 
					new Function() {
						@Override
						public void run(Clickable source) {
							ScreenManager.INSTANCE.setAccountScreen(false);
						}
					});
			} else {
				ScreenManager.INSTANCE.setAccountScreen(false);
			}
			return;
		} else if(text.length() < MAX_CHARACTERS) {
			Set<Character> keys = buttons.keySet();
			for(Character c : keys) {
				if(buttons.get(c) == clickable) {
					if(c == '_')
						c = ' ';
					
					if(text.length() == 0 || 
					   text.charAt(text.length() - 1) == ' ') {
						if(c != ' ') {
							label.setText(text + c);
							updateLabelPosition();
						}
					} else {
						label.setText(text + Character.toLowerCase(c));
						updateLabelPosition();
					}
					return;
				}
			}
		}
	}
	
	private void updateLabelPosition() {
		label.setPosition(PADDING, Constants.VIEWPORT_HEIGHT - label.getHeight());
	}
	
	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		for(Renderable r : renderables) {
			r.render(batch);
		}
		label.render(batch);
	}

	@Override
	public void update(float delta) {
		clickHandler.update();
		presentation.update(delta);
	}

	@Override
	public void hide() {}
}
