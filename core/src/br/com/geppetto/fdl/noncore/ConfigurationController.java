package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.List;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.screen.templates.GenericGameController;
import br.com.geppetto.utilities.screen.templates.StagedGameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;

public class ConfigurationController implements StagedGameController, ClickAt {
	
	private ClickAtHandler handler;
	private Stage stage;
	private Skin skinLibgdx;
	private SelectBox<String> selGraphics;
	private CheckBox chkSound;
	private Slider sldSound;
	private CheckBox chkMusic;
	private Slider sldMusic;
	private SelectBox<String> selControll;
	private CheckBox chkDebug;
	private List<Renderable> renderables;
	private GamePresentationController presentation;
	
	private List<Clickable> clickables;
	
	@Override
	public void init() {
		handler = new ClickAtHandler(this);
		clickables = new ArrayList<>();
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		renderables = new ArrayList<>();
		br.com.geppetto.utilities.menuObjects.Label title = 
				Factory.INSTANCE.buildScreenTitles("Opções");
		renderables.add(title);
		skinLibgdx = new Skin(Gdx.files.internal(Constants.SKIN_LIBGDX_UI), 
				new TextureAtlas(Constants.TEXTURE_ATLAS_LIBGDX_UI));
		stage = new Stage();
		
		stage.clear();
		Stack stack = new Stack();
		stage.addActor(stack);
		//stack.setSize(320,
		//800);
		stack.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stack.setPosition(0f, -10f);
		Table main = new Table();
		stack.add(main);
		main.add(buildGraphicsSettings());
		main.row().pad(30);
		main.add(buildAudioSettings());
		main.row();
		main.add(buildControlSettings());
		main.row().pad(30);
		main.add(buildDebugSettings());
		Button cancel = ButtonFactory.INSTANCE.genBackButton();
		cancel.setText("Cancelar");
		renderables.add(cancel);
		clickables.add(new Clickable(cancel, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setPresentationScreen(false);
			}
		}));
		Button save = ButtonFactory.INSTANCE.genBackButton();
		save.setText("Salvar");
		save.setPosition(Constants.VIEWPORT_WIDTH - save.getWidth(), 0f);
		renderables.add(save);
		clickables.add(new Clickable(save, new Function() {
			@Override
			public void run(Clickable source) {
				onSave();
				ScreenManager.INSTANCE.setMessagePopUpScreen(
				"Registrado com êxito!!!", "=)",
				new Function() {
					@Override
					public void run(Clickable source) {
						ScreenManager.INSTANCE.setPresentationScreen(false);
					}
				});
			}
		}));
		
		initFromConfigPersistence();
	}
	
	private Table buildDebugSettings() {
		Table tbl = new Table();
		tbl.add(new Label("Debug", skinLibgdx, "default-font",
		Color.RED));
		tbl.row();
		chkDebug = new CheckBox("  Ativado", skinLibgdx);
		tbl.add(chkDebug);
		return tbl;
	}
	
	private Table buildControlSettings() {
		Table tbl = new Table();
		tbl.add(new Label("Controle", skinLibgdx, "default-font", 
		Color.ORANGE));
		tbl.row();
		selControll = new SelectBox<>(skinLibgdx);
		Array<String> items = new Array<>();
		items.add("4 botoes");
		items.add("2 botoes");
		selControll.setItems(items);
		tbl.add(selControll);
		return tbl;
	}
	
	private Table buildAudioSettings() {
		Table tbl = new Table();
		// + Title: "Audio"
		tbl.pad(10, 10, 0, 10);
		tbl.add(new Label("Audio", skinLibgdx, "default-font",
		Color.ORANGE)).colspan(3);
		tbl.row();
		tbl.columnDefaults(0).padRight(10);
		tbl.columnDefaults(1).padRight(10);
		// + Checkbox, "Sound" label, sound volume slider
		chkSound = new CheckBox("", skinLibgdx);
		tbl.add(chkSound);
		tbl.add(new Label("SFX(Som)", skinLibgdx));
		sldSound = new Slider(0.0f, 1.0f, 0.1f, false, skinLibgdx);
		tbl.add(sldSound);
		tbl.row();
		// + Checkbox, "Music" label, music volume slider
		chkMusic = new CheckBox("", skinLibgdx);
		tbl.add(chkMusic);
		tbl.add(new Label("BGM(Musica)", skinLibgdx));
		sldMusic = new Slider(0.0f, 0.5f, 0.0625f, false, skinLibgdx);
		tbl.add(sldMusic);
		tbl.row();
		return tbl;
	}
	
	private Table buildGraphicsSettings() {
		Table tbl = new Table();
		// + Title: "Character Skin"
		tbl.pad(10, 10, 0, 10);
		tbl.add(new Label("Grafico", skinLibgdx,
		"default-font", Color.ORANGE)).colspan(2);
		tbl.row();
		// + Drop down box filled with skin items
		selGraphics = new SelectBox<>(skinLibgdx);
		Array<String> items = new Array<>();
		items.add("Otimo");
		items.add("Bom");
		items.add("Basico");
		selGraphics.setItems(items);
		tbl.add(selGraphics);
		return tbl;
	}
	
	private void initFromConfigPersistence() {
		selGraphics.setSelectedIndex(ConfigPersistence.INSTANCE.getGraphic());
		chkSound.setChecked(ConfigPersistence.INSTANCE.isSfx());
		sldSound.setValue(ConfigPersistence.INSTANCE.getSfxVol());
		chkMusic.setChecked(ConfigPersistence.INSTANCE.isBgm());
		sldMusic.setValue(ConfigPersistence.INSTANCE.getBgmVol());
		selControll.setSelectedIndex(
		ConfigPersistence.INSTANCE.isFourButtons() ? 0 : 1);
		chkDebug.setChecked(ConfigPersistence.INSTANCE.isDebug());
	}
	
	private void onSave() {
		ConfigPersistence.INSTANCE.setGraphic(selGraphics.getSelectedIndex());
		ConfigPersistence.INSTANCE.setSfx(chkSound.isChecked());
		ConfigPersistence.INSTANCE.setSfxVol(sldSound.getValue());
		ConfigPersistence.INSTANCE.setBgm(chkMusic.isChecked());
		ConfigPersistence.INSTANCE.setBgmVol(sldMusic.getValue());
		ConfigPersistence.INSTANCE.setFourButtons(
		selControll.getSelectedIndex() == 0 ? true : false);
		ConfigPersistence.INSTANCE.setDebug(chkDebug.isChecked());
		SoundManager.INSTANCE.init();
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		for(Renderable r : renderables)
			r.render(batch);
	}

	@Override
	public void update(float delta) {
		handler.update();
		stage.act(delta);
		presentation.update(delta);
	}

	@Override
	public void hide() {
		stage.dispose();
		skinLibgdx.dispose();
	}

	@Override
	public void render(SpriteBatch batch) {}

	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {}

	@Override
	public void setCamera(OrthographicCamera camera) {}

	@Override
	public void applyToCamera() {}

	@Override
	public void resize(float width, float height) {}

	@Override
	public InputProcessor getInputProcessor() {
		return stage;
	}

	@Override
	public void drawStage() {
		stage.draw();
	}

	@Override
	public void clickAt(float x, float y) {
		for(Clickable c : clickables)
			c.testClick(x, y);
	}

}
