package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.List;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.templates.MenuController;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AcknowledgementsController implements MenuController, ClickAt {
	private ClickAtHandler handler;
	private List<Renderable> renderables;
	private GamePresentationController presentation;
	private Clickable back;
	@Override
	public void init() {
		renderables = new ArrayList<>();
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		handler = new ClickAtHandler(this);
		Label title = Factory.INSTANCE.buildScreenTitles("Créditos");
		renderables.add(title);
		Label label = Factory.INSTANCE.buildCreditsLabel(
				  "DESENVOLVEDOR/IDEALIZADOR"
				+ "\n-Felipe Miquilini"
				+ "\n\nAVATARES ART"
				+ "\n-Svetlana Kushnariova (lana-chan@yandex.ru)"
				+ "\n\nAGRADECIMENTOS ESPECIAIS"
				+ "\n-OpenGameArt.org"
				+ "\n\n\n\n\n\nRepública Federativa do Brasil"
				+ "\nMINISTÉRIO DAS COMUNICAÇÕES", 
				Constants.VIEWPORT_WIDTH  - .1f);
		label.setPosition(1f, 1.875f);
		renderables.add(label);
		Button back = ButtonFactory.INSTANCE.genBackButton();
		this.back = new Clickable(back, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setPresentationScreen(false);
			}
		});
		renderables.add(back);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		for(Renderable r : renderables)
			r.render(batch);
	}

	@Override
	public void update(float delta) {
		handler.update();
		presentation.update(delta);
	}

	@Override
	public void clickAt(float x, float y) {
		back.testClick(x, y);
	}

	@Override
	public void hide() {}

}
