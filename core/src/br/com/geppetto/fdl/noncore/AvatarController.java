package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.gameObjects.avatar.AvatarUtility;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.persistence.UserAccount;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.screen.templates.MenuController;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AvatarController implements MenuController, ClickAt {
	private static final int AVATAR_VALUE = 300;
	private ClickAtHandler handler;
	private List<Renderable> renderables;
	private List<Renderable> blocked;
	private List<Clickable> clickables;
	private GamePresentationController presentation;
	
	final static Map<Integer, String> NAMES;
	
	static {
		NAMES = new HashMap<>();
		NAMES.put(1, "Felipe");
		NAMES.put(2, "Camila");
		NAMES.put(3, "Sandra");
		NAMES.put(4, "Thiago");
		NAMES.put(5, "Fátima");
		NAMES.put(6, "Rute");
		NAMES.put(7, "João");
		NAMES.put(8, "Matheus");
		NAMES.put(9, "Rei");
	}
	
	private final int avatarCode;
	
	public AvatarController(int avatarCode) {
		this.avatarCode = avatarCode;
	}
	
	@Override
	public void init() {
		renderables = new ArrayList<>();
		blocked = new ArrayList<>();
		clickables = new ArrayList<>();
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		handler = new ClickAtHandler(this);
		Label title = Factory.INSTANCE.buildScreenTitles("Avatares");
		
		final UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
		Label info = Factory.INSTANCE.buildCommonLabel("Letra$: " + user.getLetters(), 10);
		info.setPosition(title.getX(), title.getY() - 1.675f);
		Button back = ButtonFactory.INSTANCE.genBackButton();
		Clickable buttonC = new Clickable(back, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setAccountScreen(false);
			}
		});
		
		// Programation to load avatar clickables;
		Set<Integer> playerAvatares = user.getAvatares();
		Set<Integer> avatares = AvatarUtility.avatares();
		int pics = Math.min(4, avatares.size() - (avatarCode - 1));
		float y = info.getY() + .5f;
		int localAvatarCode = avatarCode;
		for(int i = 0; i < pics; i++, localAvatarCode++) {
			float x = localAvatarCode % 2 != 0 ? 2f : Constants.VIEWPORT_WIDTH / 2f + 2f;
			Sprite face = Factory.INSTANCE.
					buildAvatarFace(AvatarUtility.getFace(localAvatarCode));
			if(i % 2 == 0)
				y -= face.getHeight() + 2.6f;
			face.setX(x); 
			face.setY(y);
			
			final int avatCode = localAvatarCode;
			if(playerAvatares.contains(localAvatarCode)) {
				// The player already owns this avatar
				Clickable c = new Clickable(face, new Function() {
					@Override
					public void run(Clickable source) {
						//ScreenManager.INSTANCE.setGameScreen(avatCode);
						ScreenManager.INSTANCE.setStageScreen(avatCode, true);
					}
				});
				clickables.add(c);
				renderables.add(face);
			} else {
				// Blocked avatar
				Label label = 
				Factory.INSTANCE.buildCommonLabel(AVATAR_VALUE + " L$", 10f);
				label.setPosition(face.getCenterX() - label.getWidth() / 2f,
						y - 1f);
				Clickable c = null;
				if(user.getLetters() >= AVATAR_VALUE) {
					// The player has money $
					c = new Clickable(face, new Function() {
						@Override
						public void run(Clickable source) {
							ScreenManager.INSTANCE.setConfirmPopUpScreen(
							"Você quer desbloquear " + NAMES.get(avatCode) + "?",
							"Não", 
							Function.NULL_FUNCTION, 
							"Sim!", 
							new Function() {
								@Override
								public void run(Clickable source) {
									user.addAvatar(avatCode);
									user.setLetters(user.getLetters() - AVATAR_VALUE);
									UserPersistence.INSTANCE.save();
									init();
									SoundManager.INSTANCE.playNewAvatar();
								}
							});
						}
					});
				} else {
					// The player is poor :(
					c = new Clickable(face, new Function() {
						@Override
						public void run(Clickable source) {
							ScreenManager.INSTANCE.setMessagePopUpScreen
							("Você precisa de " + AVATAR_VALUE +
							" LETRAS para desbloquear este avatar." +
							"\n\nFaltam apenas " + 
							(AVATAR_VALUE - user.getLetters()) + "!" + 
							(user.getLetters() < 20 ? "\n=D" : ""), 
							"OK");
						}
					});
				}
				clickables.add(c);
				blocked.add(face);
				renderables.add(label);
			}
			Label name = Factory.INSTANCE.buildNameLabel(NAMES.get(localAvatarCode), 6f);
			name.setPosition(face.getCenterX() - name.getWidth() / 2f, 
					face.getY() + face.getHeight());
			renderables.add(name);
		}
		
		int screenNum = avatarCode / 4 + 1;
		
		if(screenNum > 1) {
			Button previous = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal,
					"Página " + (screenNum - 1));
			previous.setPosition(Constants.VIEWPORT_WIDTH -
					(previous.getWidth() * 2f + .25f), 0f);
			Clickable c = new Clickable(previous, new Function() {
				@Override
				public void run(Clickable source) {
					ScreenManager.INSTANCE.setAvatarScreen(avatarCode - 4);
				}
			});
			clickables.add(c);
			renderables.add(previous);
			// btnBack
		} 
		if(localAvatarCode <= avatares.size()) {
			Button next = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.buttonNormal,
					"Página" + (screenNum + 1));
			next.setPosition(Constants.VIEWPORT_WIDTH -
					(next.getWidth() + .25f), 0f);
			Clickable c = new Clickable(next, new Function() {
				@Override
				public void run(Clickable source) {
					ScreenManager.INSTANCE.setAvatarScreen(avatarCode + 4);
				}
			});
			clickables.add(c);
			renderables.add(next);
		}
		
		clickables.add(buttonC);
		renderables.add(title);
		renderables.add(info);
		renderables.add(back);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		for(Renderable r : renderables)
			r.render(batch);
		batch.setColor(1f, 1f, 1f, .675f);
		for(Renderable r : blocked)
			r.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}

	@Override
	public void update(float delta) {
		handler.update();
		presentation.update(delta);
	}

	@Override
	public void clickAt(float x, float y) {
		for(Clickable c : clickables)
			c.testClick(x, y);
	}

	@Override
	public void hide() {}

}
