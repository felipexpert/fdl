package br.com.geppetto.fdl.noncore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.gameObjects.factory.ButtonFactory;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.fdl.persistence.UserAccount;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.fdl.stage.Stage;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.menuObjects.Button;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.templates.MenuController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AccountController implements MenuController, Function {
	private static final String TAG = AccountController.class.getSimpleName();
	private List<Renderable> renderables;
	
	private Map<String, Item> users;
	
	private List<Clickable> empties;
	
	private ClickAtHandler clickHandler;
	
	private GamePresentationController presentation;
	
	private Clickable back;
	
	private Clickable easterEgg;
	
	private int easterEggClicks;
	
	@Override
	public void init() {
		presentation = Factory.INSTANCE.buildPresentationController(true);
		presentation.init();
		renderables = new ArrayList<>();
		users = new HashMap<>();
		empties = new ArrayList<>();
		{ // prepare title
			Label title = Factory.INSTANCE.buildScreenTitles("Contas de\nUsuário");
			renderables.add(title);
			easterEgg = new Clickable(title, 
					new Function() {
						@Override
						public void run(Clickable source) {
							easterEggClicks++;
						}
					});
		}
		float y = Constants.VIEWPORT_HEIGHT - 10f;
		final float padding = .5f;
		final float lineInterval = 4f;
		Set<String> users = UserPersistence.INSTANCE.getUserNames();
		for(String s : users) {
			//Button button = ButtonFactory.INSTANCE.genTypeLetters(Assets.INSTANCE.fonts.label,
			//		s, Constants.VIEWPORT_WIDTH - 4f);
			//button.setPosition(padding, y);
			Label label = new Label(Assets.INSTANCE.fonts.label,
					s, padding, y + .25f, Constants.VIEWPORT_WIDTH - 4f);
			label.setColor(.125f, .125f, .375f, 1f);
			Sprite layer = Factory.INSTANCE.buildAccLayer();
			layer.setSize(Constants.VIEWPORT_WIDTH - 4f + 2*padding, 2.4f + 2*padding);
			layer.setX(padding / 2f);
			layer.setY(y - padding);
			renderables.add(layer);
			renderables.add(label);
			Clickable clickableU = new Clickable(layer, this);
			Sprite garbage = Factory.INSTANCE.buildGarbage();
			garbage.setY((y + 2.4f / 2f) - garbage.getHeight() / 2f);
			garbage.setCenterX(Constants.VIEWPORT_WIDTH - 
					(garbage.getWidth()));
			renderables.add(garbage);
			Clickable clickableG = new Clickable(garbage, this);
			this.users.put(s, new Item(clickableU, clickableG));
			y -= lineInterval;
		}
		
		int empty = Constants.MAX_ACCOUNTS - users.size();
		
		for(int i = 0; i < empty; i++) {
			Label label = new Label(Assets.INSTANCE.fonts.label,
					"REGISTRAR", padding, y, Constants.VIEWPORT_WIDTH - padding * 2f);
			label.setColor(0f, 0f, 0f, 1f);
			Sprite layer = Factory.INSTANCE.buildAccLayer();
			layer.setSize(Constants.VIEWPORT_WIDTH - 4f + 2*padding, 2.4f + 2*padding);
			layer.setX(padding / 2f);
			layer.setY(y - padding);
			renderables.add(layer);
			renderables.add(label);
			Clickable clickable = new Clickable(layer, this);
			empties.add(clickable);
			y -= lineInterval;
		}
		Button back = ButtonFactory.INSTANCE.genBackButton();
		Clickable backClickable = new Clickable(back, new Function() {
			@Override
			public void run(Clickable source) {
				ScreenManager.INSTANCE.setPresentationScreen(false);
			}
		});
		this.back = backClickable;
		renderables.add(back);
		clickHandler = new ClickAtHandler(new ClickHelper());
	}
	
	private class ClickHelper implements ClickAt {
		@Override
		public void clickAt(float x, float y) {
			for(String user : users.keySet()) {
				Item item = users.get(user);
				if(item.user.testClick(x, y)) {
					Gdx.app.log(TAG, "User " + user + " is going to next menu");
					UserAccount ua = UserPersistence.INSTANCE.load(user);
					if(easterEggClicks == 8) {
						ua.setLetters(99999);
						Set<Integer> codes = StageUtilities.INSTANCE.getStageCodes();
						for(Integer code : codes) {
							Stage stage = ua.getStage(code);
							stage.setWords(99999);							
						}
						UserPersistence.INSTANCE.save();
					}
					ConfigPersistence.INSTANCE.setLastUser(user);
					ConfigPersistence.INSTANCE.save();
					ScreenManager.INSTANCE.setAvatarScreen(true);
					SoundManager.INSTANCE.playLastStagePlayedTheme();
					return;
				} else if(item.garbage.testClick(x, y)) {
					Gdx.app.log(TAG, "User " + user + " is going to be deleted");
					final String name = user;
					
					ScreenManager.INSTANCE.setConfirmPopUpScreen(
							"Deseja realmente excluir " + name +
							", eliminando todos seus registros?", 
							"Não!", new Function() {
								@Override
								public void run(Clickable clickable) {
									Gdx.app.log(TAG, "canceled"); 
								}
							}, 
							"Sim", new Function() {
								@Override
								public void run(Clickable clickable) {
									if(ConfigPersistence.INSTANCE.getLastUser().equals(name)) {
										ConfigPersistence.INSTANCE.setLastUser("");
										ConfigPersistence.INSTANCE.save();
									}
									UserPersistence.INSTANCE.deleteAccount(name);
									init();
								}
							});
					return;
				}
			}
			
			for(Clickable c : empties) {
				if(c.testClick(x, y)) {
					ScreenManager.INSTANCE.setNewUserScreen(true);
					return;
				}
			}
			
			back.testClick(x, y);
			
			easterEgg.testClick(x, y);
		}
	}
	
	private class Item {
		private Clickable user;
		private Clickable garbage;
		
		private Item(Clickable user, Clickable garbage) {
			this.user = user;
			this.garbage = garbage;
		}
	}
	
	@Override
	public void renderGUI(SpriteBatch batch) {
		presentation.render(batch);
		for(Renderable r : renderables)
			r.render(batch);
	}

	@Override
	public void update(float delta) {
		clickHandler.update();
		presentation.update(delta);
	}

	@Override
	public void run(Clickable clickable) {}

	@Override
	public void hide() {}
}
