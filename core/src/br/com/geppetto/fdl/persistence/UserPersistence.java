package br.com.geppetto.fdl.persistence;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import br.com.geppetto.fdl.stage.Stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class UserPersistence {
	private static final String TAG = UserPersistence.class.getSimpleName();
	public static final UserPersistence INSTANCE = new UserPersistence(); 
	
	private static final String PREFS_SOURCE = "fdlUser.prefs";
	
	private final Preferences prefs;
	
	private UserAccount lastUser;
	
	private UserPersistence() {
		prefs = Gdx.app.getPreferences(PREFS_SOURCE);
	}
	
	public Set<String> getUserNames() {
		return prefs.get().keySet();
	}
	
	public UserAccount load(String name) {
		if(!prefs.contains(name))
			throw new IllegalArgumentException("This user doesn't exist yet");
		String stagesInfo = prefs.getString(name);
		String[] data = stagesInfo.split("\\|");
		Set<Stage> setStages = new HashSet<>();
		{
			if(data[0].length() > 0) {
				String[] stages = data[0].split(";");
				for(String s : stages) {
					String[] i = s.split(":");
					Stage stage = new Stage();
					stage.setCode(Integer.parseInt(i[0]));;
					stage.setShortBestScore(Integer.parseInt(i[1]));
					stage.setShortBestScore(Integer.parseInt(i[2]));
					stage.setShortBestScore(Integer.parseInt(i[3]));
					stage.setWords(Integer.parseInt(i[4]));
					setStages.add(stage);
				}
			}
		}
		Set<Integer> setAvatares = new TreeSet<>();
		setAvatares.add(1); setAvatares.add(2);
		{
			if(data[1].length() > 0) {
				String[] avatares = data[1].split(";");
				for(String s : avatares) {
					setAvatares.add(Integer.valueOf(s));
				}
			}
		}
		int letters = Integer.parseInt((data[2].length() > 0 ? data[2] : "0"));
		int lastStagePlayed = Integer.parseInt((data[3].length() > 0 ? data[3] : "1"));
		UserAccount ua = new UserAccount(name, setStages, setAvatares, letters, lastStagePlayed);
		Gdx.app.log(TAG, ua.toString());
		lastUser = ua;
		return ua;
	}
	
	public void save() {
		save(lastUser);
	}
	
	public void save(UserAccount userAccount) {
		
		StringBuilder sb = new StringBuilder();
		Set<Stage> stages = userAccount.getStages();
		if(stages != null)
			for(Stage s : stages) {
				sb.append(s.getCode())
				.append(':')
				.append(s.getShortBestScore())
				.append(':')
				.append(s.getMediumBestScore())
				.append(':')
				.append(s.getLongBestScore())
				.append(':')
				.append(s.getWords())
				.append(';');
			}
		sb.append("|");
		Set<Integer> avatares = userAccount.getAvatares();
		if(avatares != null)
			for(Integer a : avatares) {
				sb.append(a).append(";");
			}
		sb.append("|").append(userAccount.getLetters())
		.append("|").append(Math.max(userAccount.getLastStagePlayed(), 1));
		prefs.putString(userAccount.getName(), sb.toString());
		prefs.flush();
	}
	
	public UserAccount getLastUserAccount() {
		return lastUser;
	}
	
	public void deleteAccount(String name) {
		prefs.remove(name);
		prefs.flush();
	}
	
	public void clear() {
		prefs.clear();
		prefs.flush();
	}
}
