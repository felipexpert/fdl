package br.com.geppetto.fdl.persistence;

import java.util.Set;

import br.com.geppetto.fdl.stage.Stage;

public class UserAccount {
	private String name;
	private Set<Stage> stages;
	private Set<Integer> avatares;
	private int letters;
	
	private int lastStagePlayed;
	
	public UserAccount() {}
	
	public UserAccount(String name, Set<Stage> stages, Set<Integer> avatares, int letters, int lastStagePlayed) {
		this.name = name;
		this.stages = stages;
		this.avatares = avatares;
		this.letters = letters;
		this.lastStagePlayed = lastStagePlayed;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Stage> getStages() {
		return stages;
	}

	public void setStages(Set<Stage> stages) {
		this.stages = stages;
	}
	
	public void addStage(int code) {
		Stage s = new Stage(code, 0, 0, 0, 0);
		stages.add(s);
	}
	
	public void setAvatares(Set<Integer> avatares) {
		this.avatares = avatares;
	}
	
	public void addAvatar(Integer avatar) {
		avatares.add(avatar);
	}
	
	public Set<Integer> getAvatares() {
		return avatares;
	}
	
	public int getLetters() {
		return letters;
	}

	public void setLetters(int letters) {
		this.letters = letters;
	}
	
	public void addLetters(int letters) {
		this.letters += letters;
	}

	public int getLastStagePlayed() {
		return lastStagePlayed;
	}

	public void setLastStagePlayed(int lastStagePlayed) {
		this.lastStagePlayed = lastStagePlayed;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		if(!(obj instanceof UserAccount))
			return false;
		
		return ((UserAccount)obj).name.equals(name);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("---").append(name).append("---").append("\n");
		sb.append("Stages:\n");
		for(Stage s : stages) {
			sb.append("  ").append(s.toString()).append("\n");
		}
		sb.append("Avatares:\n");
		for(Integer i : avatares) {
			sb.append("  ").append(i).append("\n");
		}
		sb.append("Letras\n  ").append(letters);
		return sb.toString();
	}
	
	public Stage getStage(int stageCode) {
		Stage stage = new Stage();
		stage.setCode(stageCode);
		boolean found = false;
		for(Stage s : stages) {
			if(s.equals(stage)) {
				stage = s;
				found = true;
				break;
			}
		}
		if(!found) {
			stages.add(stage);
		}
		return stage;
	}
}
