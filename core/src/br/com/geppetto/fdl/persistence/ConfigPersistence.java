package br.com.geppetto.fdl.persistence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class ConfigPersistence {
	
	public static final ConfigPersistence INSTANCE = new ConfigPersistence();
	private static final String PREFS_SOURCE = "fdlConfig.prefs";
	
	private final Preferences prefs;
	
	private String lastUser;
	
	private int graphic;
	private boolean sfx;
	private float sfxVol;
	private boolean bgm;
	private float bgmVol;
	private boolean fourButtons;
	private boolean debug;
	
	private ConfigPersistence() {
		prefs = Gdx.app.getPreferences(PREFS_SOURCE);
	}
	
	
	public void load() {
		lastUser = prefs.getString("lastUser", "");
		graphic = prefs.getInteger("graphic", 0);
		sfx = prefs.getBoolean("sfx", true);
		sfxVol = prefs.getFloat("sfxVol", .5f);
		bgm = prefs.getBoolean("bgm", true);
		bgmVol = prefs.getFloat("bgmVol", .25f);
		fourButtons = prefs.getBoolean("fourButtons", true);
		debug = prefs.getBoolean("debug", false);
	}
	
	public void save() {
		prefs.putString("lastUser", lastUser);
		prefs.putInteger("graphic", graphic);
		prefs.putBoolean("sfx", sfx);
		prefs.putFloat("sfxVol", sfxVol);
		prefs.putBoolean("bgm", bgm);
		prefs.putFloat("bgmVol", bgmVol);
		prefs.putBoolean("fourButtons", fourButtons);
		prefs.putBoolean("debug", debug);
		prefs.flush();
	}
	
	public void clear() {
		prefs.clear();
		prefs.flush();
	}

	
	// GETTERS AND SETTERS
	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}


	public int getGraphic() {
		return graphic;
	}


	public void setGraphic(int graphic) {
		this.graphic = graphic;
	}


	public boolean isBgm() {
		return bgm;
	}


	public void setBgm(boolean bgm) {
		this.bgm = bgm;
	}


	public float getBgmVol() {
		return bgmVol;
	}


	public void setBgmVol(float bgmVol) {
		this.bgmVol = bgmVol;
	}


	public boolean isSfx() {
		return sfx;
	}


	public void setSfx(boolean sfx) {
		this.sfx = sfx;
	}


	public float getSfxVol() {
		return sfxVol;
	}


	public void setSfxVol(float sfxVol) {
		this.sfxVol = sfxVol;
	}


	public boolean isFourButtons() {
		return fourButtons;
	}


	public void setFourButtons(boolean fourButtons) {
		this.fourButtons = fourButtons;
	}


	public boolean isDebug() {
		return debug;
	}


	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	public int getLastStagePlayed() {
		int lastStagePlayed = 1;
		String lastUser = getLastUser();
		if(!lastUser.equals("")) {
			if(UserPersistence.INSTANCE.getLastUserAccount() == null)
				UserPersistence.INSTANCE.load(lastUser);
			UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
			lastStagePlayed = user.getLastStagePlayed();
		}
		return lastStagePlayed;
	}
}
