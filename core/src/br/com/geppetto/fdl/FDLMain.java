package br.com.geppetto.fdl;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.storyboard.StoryboardAssets;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.fdl.sound.SoundManager;
import br.com.geppetto.utilities.clickHandler.ClickHandler;
import br.com.geppetto.utilities.screen.DirectedGame;
import br.com.geppetto.utilities.screenshot.ScreenshotFactory;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FDLMain extends DirectedGame {	
	
	@Override
	public void create () {
		// Set Libgdx log level
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		Gdx.input.setCatchBackKey(true);
		ConfigPersistence.INSTANCE.load();
		ClickHandler.INSTANCE.setViewportWidth(Constants.VIEWPORT_WIDTH);
		ClickHandler.INSTANCE.setViewportHeight(Constants.VIEWPORT_HEIGHT);
		// Load assets
		Assets.INSTANCE.init(new AssetManager());
		
		// setup SoundManager
		SoundManager.INSTANCE.init();
		
		setBatch(new SpriteBatch());
		setOnResume(new Runnable() {
			@Override
			public void run() {
				Assets.INSTANCE.init(new AssetManager());
				SoundManager.INSTANCE.init();
			}
		});
		setOnDispose(new Runnable() {
			@Override
			public void run() {
				Assets.INSTANCE.dispose();
			}
		});
		// Load preferences for audio settings and start playing music
		//GamePreferences.instance.load();
		//AudioManager.instance.play(Assets.instance.music.song01);

		// Start game at Presentation Screen
		ScreenManager.INSTANCE.init(this);
		ScreenManager.INSTANCE.setPresentationScreen(true);
	}
	
	// This code is used when I want to generate screenshots easily
	/*@Override
	public void render() {
		super.render();
		ScreenshotFactory.saveScreenshot("prints/", true);
	}*/
}
