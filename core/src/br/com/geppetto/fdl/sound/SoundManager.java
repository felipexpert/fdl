package br.com.geppetto.fdl.sound;

import java.lang.reflect.Field;
import java.util.Map;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.Mp3;
import br.com.geppetto.fdl.asset.SFX;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.utilities.asset.AbstractSound;
import br.com.geppetto.utilities.asset.MusicWrapper;
import br.com.geppetto.utilities.screen.Initializable;

public class SoundManager implements Initializable {
	public static final SoundManager INSTANCE = new SoundManager();
	
	private MusicWrapper lastBGM;
	
	private SoundManager() {}
	
	@Override
	public void init() {
		try {
			float bgmVolume = ConfigPersistence.INSTANCE.getBgmVol();
			boolean bgm = ConfigPersistence.INSTANCE.isBgm();
			float sfxVolume = ConfigPersistence.INSTANCE.getSfxVol();
			Mp3 mp3 = Assets.INSTANCE.mp3;
			allBgm(!bgm, bgmVolume);
			if(bgm) forcePlayLastTheme();
			for(String letter : mp3.getLetters().keySet()) {
				MusicWrapper m = mp3.getLetters().get(letter);
				m.setVolume(Math.min(sfxVolume + .5f, 1f));
			}
			for(String word : mp3.getWords().keySet()) {
				MusicWrapper m = mp3.getWords().get(word); 
				m.setVolume(Math.min(sfxVolume + .5f, 1f));
			}
			Field[] fields = SFX.class.getFields();
			for(Field f : fields) {
				((AbstractSound)f.get(Assets.INSTANCE.sfx)).setVolume(sfxVolume); 
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void allBgm(boolean stop, float volume) {
		Map<String, MusicWrapper> stages = Assets.INSTANCE.mp3.getStages();
		for(String m : stages.keySet()) {
			MusicWrapper music = stages.get(m);
			music.setVolume(volume);
			if(stop)
				music.stop();
		}
	}
	
	private void forcePlayLastTheme() {
		MusicWrapper m = StageUtilities.INSTANCE.getBGM(ConfigPersistence.INSTANCE.getLastStagePlayed());
		m.play();
		lastBGM = m;
	}
	
	public void playLastStagePlayedTheme() {
		MusicWrapper m = StageUtilities.INSTANCE.getBGM(ConfigPersistence.INSTANCE.getLastStagePlayed());
		if(ConfigPersistence.INSTANCE.isBgm() && lastBGM != m) {
			if(lastBGM != null) lastBGM.stop();
			m.play();
			lastBGM = m;
		}
	}
	
	private void playSFX(AbstractSound as) {
		if(ConfigPersistence.INSTANCE.isSfx()) {
			as.play();
		}
	}
	
	public void playLetter(String letter) {
		playSFX(Assets.INSTANCE.mp3.getLetter(letter));
	}
	
	public void playWord(String word) {
		playSFX(Assets.INSTANCE.mp3.getWord(word));
	}
	
	public void playWalk() {
		playSFX(Assets.INSTANCE.sfx.walk);
	}
	
	public void playRoller() {
		playSFX(Assets.INSTANCE.sfx.roller);
	}
	
	public void playSlipper() {
		playSFX(Assets.INSTANCE.sfx.slipper);
	}
	
	public void playReverse() {
		playSFX(Assets.INSTANCE.sfx.reverse);
	}
	
	public void playTouchedQueue() {
		playSFX(Assets.INSTANCE.sfx.touchedQueue);
	}
	
	public void playWrongLetter() {
		playSFX(Assets.INSTANCE.sfx.wrongLetter);
	}
	
	public void playAlert() {
		playSFX(Assets.INSTANCE.sfx.alert);
	}
	
	public void playNewAvatar() {
		playSFX(Assets.INSTANCE.sfx.newAvatar);
	}
}
