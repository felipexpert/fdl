package br.com.geppetto.fdl.asset;

import java.lang.reflect.Field;

import br.com.geppetto.utilities.asset.FontWrapper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.Disposable;


public class Fonts implements Disposable{
	
	private static final String TAG = Fonts.class.getSimpleName();
	
	public final FontWrapper game = new FontWrapper();
	public final FontWrapper title = new FontWrapper();
	public final FontWrapper buttonNormal = new FontWrapper();
	public final FontWrapper buttonBig = new FontWrapper();
	public final FontWrapper onScreenLetter = new FontWrapper();
	public final FontWrapper onScreenWord = new FontWrapper();
	public final FontWrapper label = new FontWrapper();
	public final FontWrapper credits = new FontWrapper();
	public final FontWrapper labelSmall = new FontWrapper();
	
	private boolean initialized;
	
	public void init() {
		FreeTypeFontGenerator gameG = new FreeTypeFontGenerator(Gdx.files.internal("fonts/game.ttf"));
		FreeTypeFontGenerator titleG = new FreeTypeFontGenerator(Gdx.files.internal("fonts/title.ttf"));
		FreeTypeFontGenerator buttonG = new FreeTypeFontGenerator(Gdx.files.internal("fonts/button.ttf"));
		FreeTypeFontGenerator labelG = new FreeTypeFontGenerator(Gdx.files.internal("fonts/label.ttf"));
		
		game.setFont(genFont(gameG, 18));
		title.setFont(genFont(titleG, 34));
		buttonNormal.setFont(genFont(buttonG, 12));
		buttonBig.setFont(genFont(buttonG, 18));
		onScreenLetter.setFont(genFont(buttonG, 200));
		onScreenWord.setFont(genFont(buttonG, 30));
		label.setFont(genFont(labelG, 18));
		credits.setFont(genFont(labelG, 12));
		labelSmall.setFont(genFont(gameG, 10));
		gameG.dispose(); titleG.dispose(); buttonG.dispose(); labelG.dispose();
		
		initialized = true;
	}
	
	private static BitmapFont genFont(FreeTypeFontGenerator gen, int size) {
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.magFilter = TextureFilter.Linear;
		parameter.minFilter = TextureFilter.Linear;
		BitmapFont font = gen.generateFont(parameter);
		font.setScale(.075f);
		return font;
	}

	@Override
	public void dispose() {
		if(initialized) {
			try {
				for(Field o : Fonts.class.getFields()) {
					Object field = o.get(this);
					if(field instanceof FontWrapper) {
						FontWrapper font = (FontWrapper)field;
						font.getFont().dispose();
						Gdx.app.debug(TAG, o.getName() + " was disposed");
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

}
