package br.com.geppetto.fdl.asset;

import java.lang.reflect.Field;

import br.com.geppetto.utilities.asset.AnimationWrapper;
import br.com.geppetto.utilities.asset.RegionWrapper;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class Stages {
	
	public final RegionWrapper bemVindoWall = new RegionWrapper();
	public final RegionWrapper bemVindoGround = new RegionWrapper();
	public final RegionWrapper bemVindoThumbnail = new RegionWrapper();
	
	public final AnimationWrapper transitoWall = new AnimationWrapper();
	public final RegionWrapper transitoGround = new RegionWrapper();
	public final RegionWrapper transitoThumbnail = new RegionWrapper();
	
	public final AnimationWrapper amigosWall = new AnimationWrapper();
	public final AnimationWrapper amigosGround = new AnimationWrapper();
	public final RegionWrapper amigosThumbnail = new RegionWrapper();
	
	public final RegionWrapper cidadeWall = new RegionWrapper();
	public final RegionWrapper cidadeGround = new RegionWrapper();
	public final RegionWrapper cidadeThumbnail = new RegionWrapper();
	
	public final RegionWrapper tesouroWall = new RegionWrapper();
	public final RegionWrapper tesouroGround = new RegionWrapper();
	public final RegionWrapper tesouroThumbnail = new RegionWrapper();
	
	public final RegionWrapper serralheiriaWall = new RegionWrapper();
	public final RegionWrapper serralheiriaGround = new RegionWrapper();
	public final RegionWrapper serralheiriaThumbnail = new RegionWrapper();
	
	public final RegionWrapper ponteWall = new RegionWrapper();
	public final RegionWrapper ponteGround = new RegionWrapper();
	public final RegionWrapper ponteThumbnail = new RegionWrapper();
	
	
	public void init(TextureAtlas nearest) {
		prepareRegions(nearest);
		{
			Array<AtlasRegion> regions =  nearest.findRegions("transitoWall");
			transitoWall.setAnimation(new Animation(1f / 25f, regions));
			transitoWall.getAnimation().setPlayMode(PlayMode.LOOP);
		}
		
		{
			Array<AtlasRegion> gRegions = nearest.findRegions("amigosGround");
			amigosGround.setAnimation(new Animation(1f / 12f, gRegions));
			amigosGround.getAnimation().setPlayMode(PlayMode.LOOP);
			Array<AtlasRegion> wRegions = nearest.findRegions("amigosWall");
			amigosWall.setAnimation(new Animation(1.25f, wRegions));
			amigosWall.getAnimation().setPlayMode(PlayMode.LOOP);
		}
	}
	
	/**
	 * This method automatically setup the "static" guys
	 * @param atlas
	 */
	private void prepareRegions(TextureAtlas atlas) {
		try {
			Field[] fds = Stages.class.getFields();
			for(Field f : fds) {
				Object obj = f.get(Assets.INSTANCE.stages);
				if(obj instanceof RegionWrapper) {
					RegionWrapper regionWrapper = (RegionWrapper) obj;
					regionWrapper.setRegion(atlas.findRegion(f.getName()));
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
