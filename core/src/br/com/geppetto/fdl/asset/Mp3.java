package br.com.geppetto.fdl.asset;

import java.util.HashMap;
import java.util.Map;

import br.com.geppetto.utilities.asset.MusicWrapper;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;

public class Mp3 {
	private final Map<String, MusicWrapper> stages = new HashMap<>();
	private final Map<String, MusicWrapper> letters = new HashMap<>();	
	private final Map<String, MusicWrapper> words = new HashMap<>();
	
	public void init(AssetManager am) {
		
		for(String stage : Words.INSTANCE.getStages()) {
			putStage(stage, am);
		}
		
		for(char c = 'a'; c <= 'z'; c++) {
			putLetter(c, am);
		}
		
		putLetter("Á", "mp3/letters/a.mp3", am);
		putLetter("Â", "mp3/letters/a.mp3", am);
		putLetter("Ã", "mp3/letters/a.mp3", am);
		putLetter("É", "mp3/letters/e.mp3", am);
		putLetter("Ê", "mp3/letters/e.mp3", am);
		putLetter("Í", "mp3/letters/i.mp3", am);
		putLetter("Ó", "mp3/letters/o.mp3", am);
		putLetter("Ô", "mp3/letters/o.mp3", am);
		putLetter("Õ", "mp3/letters/o.mp3", am);
		putLetter("Ú", "mp3/letters/u.mp3", am);
		putLetter("Ç", "mp3/letters/c.mp3", am);
		
		for(String word : Words.INSTANCE.getWords())
			putWord(word, am);
	}
	
	public MusicWrapper getStage(String stage) {
		return stages.get(stage);
	}
	
	public MusicWrapper getWord(String word) {
		return words.get(word);
	}
	
	public MusicWrapper getLetter(String letter) {
		return letters.get(letter.toUpperCase());
	}
	
	public Map<String, MusicWrapper> getStages() {
		return stages;
	}
	
	public Map<String, MusicWrapper> getWords() {
		return words;
	}
	
	public Map<String, MusicWrapper> getLetters() {
		return letters;
	}
	
	private void putStage(String stage, AssetManager am) {
		Music stageMusic = am.get("mp3/stages/" + stage, Music.class);
		stageMusic.setLooping(true);
		String name = stage.substring(0, stage.length() - 4);
		if(stages.containsKey(name)) {
			stages.get(name).setMusic(stageMusic);
		} else {
			MusicWrapper mw = new MusicWrapper();
			mw.setMusic(stageMusic);
			stages.put(name, mw);
		}
	}
	
	private void putWord(String word, AssetManager am) {
		// I am doing a split to separate filename and name
		String[] tokens = word.split(";"); 
		String file = tokens[0];
		String name = tokens[1];
		Music m = am.get("mp3/words/" + file, Music.class);
		if(words.containsKey(word)) {
			words.get(name).setMusic(m);
		} else {
			MusicWrapper mw = new MusicWrapper();
			mw.setMusic(m);
			words.put(name, mw);
		}
	}
	
	private void putLetter(String letter, String file, AssetManager am) {
		Music m = am.get(file, Music.class);
		if(letters.containsKey(letter)) {
			letters.get(letter).setMusic(m);
		} else {
			MusicWrapper mw = new MusicWrapper();
			mw.setMusic(m);
			letters.put(letter, mw);
		}
	}
	
	private void putLetter(char c, AssetManager am) {
		putLetter(Character.toString(c).toUpperCase(), "mp3/letters/" + c + ".mp3", am);
	}
}
