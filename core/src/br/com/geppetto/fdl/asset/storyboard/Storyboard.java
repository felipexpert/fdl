package br.com.geppetto.fdl.asset.storyboard;

import java.lang.reflect.Field;
import java.util.Arrays;

import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class Storyboard implements Initializable{
	public RegionWrapper[] story1;
	
	private boolean firstTime = true;
	
	@Override
	public void init(TextureAtlas atlas) {
		try {
			Field[] fields = Storyboard.class.getFields();
			for(Field f : fields) {
				Array<AtlasRegion> regions = atlas.findRegions(f.getName());
				RegionWrapper[] reg = null; 
				if(firstTime) {		
					reg = new RegionWrapper[regions.size];
					for(int i = 0; i < regions.size; i++) {
						reg[i] = new RegionWrapper();
						reg[i].setRegion(regions.get(i));
					}
					f.set(StoryboardAssets.INSTANCE.storyboard, reg);
				} else {
					reg = (RegionWrapper[])f.get(StoryboardAssets.INSTANCE.storyboard);
				}
				for(int i = 0; i < regions.size; i++) {
					reg[i].setRegion(regions.get(i));
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		firstTime = false;
	}

}
