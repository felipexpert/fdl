package br.com.geppetto.fdl.asset.storyboard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.utilities.asset.AssetInterface;

public class StoryboardAssets implements AssetInterface {
	public static final String TAG = StoryboardAssets.class.getSimpleName();
	public static final StoryboardAssets INSTANCE = new StoryboardAssets();
	
	private AssetManager assetManager;
	
	public final Storyboard storyboard;
	
	private StoryboardAssets() {
		storyboard = new Storyboard();
	}
	
	@Override
	public void dispose() {
		Gdx.app.debug(TAG, "Disposing...");
		if(assetManager != null)
			assetManager.dispose();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception)throwable);
	}

	@Override
	public void init(AssetManager manager) {
		this.assetManager = manager;
		assetManager.setErrorListener(this);
		assetManager.load(Constants.TEXTURE_ATLAS_STORYBOARD, TextureAtlas.class);
		assetManager.finishLoading();
		
		Gdx.app.debug(TAG, "assets loaded: " + assetManager.getAssetNames().size);
		for(String name : assetManager.getAssetNames()) {
			Gdx.app.debug(TAG, "asset: " + name);
		}
		
		TextureAtlas atlas = new TextureAtlas(Constants.TEXTURE_ATLAS_STORYBOARD);
		storyboard.init(atlas);
	}

}
