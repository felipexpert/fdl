package br.com.geppetto.fdl.asset;

import java.lang.reflect.Field;

import br.com.geppetto.utilities.asset.AnimationWrapper;
import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Avatares {

	public final RegionWrapper avatar1 = new RegionWrapper();
	public final AnimationWrapper avatar1L = new AnimationWrapper();
	public final AnimationWrapper avatar1F = new AnimationWrapper();
	public final AnimationWrapper avatar1B = new AnimationWrapper();
	
	public final RegionWrapper avatar2 = new RegionWrapper();
	public final AnimationWrapper avatar2L = new AnimationWrapper();
	public final AnimationWrapper avatar2F = new AnimationWrapper();
	public final AnimationWrapper avatar2B = new AnimationWrapper();
	
	public final RegionWrapper avatar3 = new RegionWrapper();
	public final AnimationWrapper avatar3L = new AnimationWrapper();
	public final AnimationWrapper avatar3F = new AnimationWrapper();
	public final AnimationWrapper avatar3B = new AnimationWrapper();
	
	public final RegionWrapper avatar4 = new RegionWrapper();
	public final AnimationWrapper avatar4L = new AnimationWrapper();
	public final AnimationWrapper avatar4F = new AnimationWrapper();
	public final AnimationWrapper avatar4B = new AnimationWrapper();
	
	public final RegionWrapper avatar5 = new RegionWrapper();
	public final AnimationWrapper avatar5L = new AnimationWrapper();
	public final AnimationWrapper avatar5F = new AnimationWrapper();
	public final AnimationWrapper avatar5B = new AnimationWrapper();
	
	public final RegionWrapper avatar6 = new RegionWrapper();
	public final AnimationWrapper avatar6L = new AnimationWrapper();
	public final AnimationWrapper avatar6F = new AnimationWrapper();
	public final AnimationWrapper avatar6B = new AnimationWrapper();
	
	public final RegionWrapper avatar7 = new RegionWrapper();
	public final AnimationWrapper avatar7L = new AnimationWrapper();
	public final AnimationWrapper avatar7F = new AnimationWrapper();
	public final AnimationWrapper avatar7B = new AnimationWrapper();
	
	public final RegionWrapper avatar8 = new RegionWrapper();
	public final AnimationWrapper avatar8L = new AnimationWrapper();
	public final AnimationWrapper avatar8F = new AnimationWrapper();
	public final AnimationWrapper avatar8B = new AnimationWrapper();
	
	public final RegionWrapper avatar9 = new RegionWrapper();
	public final AnimationWrapper avatar9L = new AnimationWrapper();
	public final AnimationWrapper avatar9F = new AnimationWrapper();
	public final AnimationWrapper avatar9B = new AnimationWrapper();
	
	public final AnimationWrapper avatar101L = new AnimationWrapper();
	public final AnimationWrapper avatar101F = new AnimationWrapper();
	public final AnimationWrapper avatar101B = new AnimationWrapper();
	
	public final AnimationWrapper avatar102L = new AnimationWrapper();
	public final AnimationWrapper avatar102F = new AnimationWrapper();
	public final AnimationWrapper avatar102B = new AnimationWrapper();
	
	public final AnimationWrapper avatar103L = new AnimationWrapper();
	public final AnimationWrapper avatar103F = new AnimationWrapper();
	public final AnimationWrapper avatar103B = new AnimationWrapper();
	
	public final AnimationWrapper avatar104L = new AnimationWrapper();
	public final AnimationWrapper avatar104F = new AnimationWrapper();
	public final AnimationWrapper avatar104B = new AnimationWrapper();
	
	private static final float DURATION = 1f / 7f;
	
	public void init(TextureAtlas nearest, TextureAtlas linear) {
		Field[] fds = Avatares.class.getFields();
		for(Field f : fds) {
			String name = f.getName();
			if(name.matches("^\\w*L$")) {
				name = name.substring(0, name.length() - 1);
				int code = getCode(name);
				boolean loadPic = code > 100 ? false : true;
				
				loadAvatar(nearest, linear, name, loadPic);
			}
		}
	}
	
	private static int getCode(String name) {
		String regex = "^avatar(\\d+)\\w*$";
		return Integer.parseInt(name.replaceFirst(regex, "$1"));
	}
	
	private void loadAvatar(TextureAtlas nearest, TextureAtlas linear, String name, boolean loadPic) {
		try {
			if(loadPic)
				((RegionWrapper)Avatares.class.getField(name).get(this)).setRegion(linear.findRegion(name));
			
			loadAnimation(nearest, name + 'L');
			loadAnimation(nearest, name + 'F');
			loadAnimation(nearest, name + 'B');
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadAnimation(TextureAtlas atlas, String name) throws Exception {
		AnimationWrapper l = (AnimationWrapper)Avatares.class.getField(name).get(this);
		l.setAnimation(new Animation(DURATION, atlas.findRegions(name)));
		l.getAnimation().setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
	}
}
