package br.com.geppetto.fdl.asset;

import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Menu {
	
	public final RegionWrapper gear = new RegionWrapper();
	public final RegionWrapper garbage = new RegionWrapper();
	public final RegionWrapper popUp = new RegionWrapper();
	public final RegionWrapper arrowHor = new RegionWrapper();
	public final RegionWrapper arrowVer = new RegionWrapper();
	public final RegionWrapper zoomIn = new RegionWrapper();
	public final RegionWrapper zoomOut = new RegionWrapper();
	public final RegionWrapper zoomDefault = new RegionWrapper();
	public final RegionWrapper pause = new RegionWrapper();
	public final RegionWrapper close = new RegionWrapper();
	public final RegionWrapper hudBackground = new RegionWrapper();
	public final RegionWrapper accLayer = new RegionWrapper();
	public final RegionWrapper exit = new RegionWrapper();
	public final RegionWrapper whiteBG = new RegionWrapper();
	
	public void init(TextureAtlas nearest, TextureAtlas linear) {
		gear.setRegion(linear.findRegion("gear"));
		garbage.setRegion(nearest.findRegion("garbage"));
		popUp.setRegion(nearest.findRegion("popUp"));
		arrowHor.setRegion(linear.findRegion("arrowHor"));
		arrowVer.setRegion(linear.findRegion("arrowVer"));
		zoomIn.setRegion(linear.findRegion("zoomIn"));
		zoomOut.setRegion(linear.findRegion("zoomOut"));
		zoomDefault.setRegion(linear.findRegion("zoomDefault"));
		pause.setRegion(linear.findRegion("pause"));
		close.setRegion(linear.findRegion("close"));
		hudBackground.setRegion(linear.findRegion("hudBackground"));
		accLayer.setRegion(linear.findRegion("accLayer"));
		exit.setRegion(nearest.findRegion("exit"));
		whiteBG.setRegion(nearest.findRegion("whiteBG"));
	}
}
