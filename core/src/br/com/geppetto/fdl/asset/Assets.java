package br.com.geppetto.fdl.asset;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.utilities.asset.AssetInterface;

public class Assets implements AssetInterface{
	public static final String TAG = Assets.class.getSimpleName();
	public static final Assets INSTANCE = new Assets();
	
	private AssetManager assetManager;
	
	public final Stages stages;
	public final Avatares avatares;
	public final PowerUp powerUp;
	public final Fonts fonts;
	public final Menu menu;
	public final Button button;
	
	public final Digits digits;
	
	public final SFX sfx;
	public final Mp3 mp3;
	
	private Assets() {
		stages = new Stages();
		avatares = new Avatares();
		powerUp = new PowerUp();
		fonts = new Fonts();
		menu = new Menu();
		button = new Button();
		digits = new Digits();
		sfx = new SFX();
		mp3 = new Mp3();
	};
	@Override
	public void init(AssetManager assetManager) {
		dispose();
		Words.INSTANCE.init();
		this.assetManager = assetManager;
		assetManager.setErrorListener(this);
		assetManager.load(Constants.TEXTURE_ATLAS_NEAREST, TextureAtlas.class);
		assetManager.load(Constants.TEXTURE_ATLAS_LINEAR, TextureAtlas.class);
		
		assetManager.load("sfx/walk.wav", Sound.class);
		assetManager.load("sfx/roller.wav", Sound.class);
		assetManager.load("sfx/slipper.wav", Sound.class);
		assetManager.load("sfx/reverse.wav", Sound.class);
		assetManager.load("sfx/alert.mp3", Sound.class);
		assetManager.load("sfx/touchedQueue.mp3", Music.class);
		assetManager.load("sfx/newAvatar.wav", Music.class);
		assetManager.load("sfx/wrongLetter.mp3", Music.class);
		
		// Loading stages bgm
		for(String stage : Words.INSTANCE.getStages()) {
			assetManager.load("mp3/stages/" + stage, Music.class);
		}
		
		// Loading letters sounds
		for(char c = 'a'; c <= 'z'; c++) {
			assetManager.load("mp3/letters/" + c + ".mp3", Music.class);
		}
		
		// Loading word sounds
		for(String word : Words.INSTANCE.getWords()) {
			// I am splitting here to get only the file name
			assetManager.load("mp3/words/" + word.split(";")[0], Music.class);
		}
		assetManager.finishLoading();
		
		Gdx.app.debug(TAG, "assets loaded: " + assetManager.getAssetNames().size);
		for(String name : assetManager.getAssetNames()) {
			Gdx.app.debug(TAG, "asset: " + name);
		}
		
		TextureAtlas nearest = new TextureAtlas(Constants.TEXTURE_ATLAS_NEAREST);
		TextureAtlas linear = new TextureAtlas(Constants.TEXTURE_ATLAS_LINEAR);
		stages.init(nearest);
		avatares.init(nearest, linear);
		powerUp.init(nearest);
		menu.init(nearest, linear);
		button.init(nearest);
		digits.init(nearest);
		fonts.init();
		sfx.init(assetManager);
		mp3.init(assetManager);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception)throwable);
	}

	@Override
	public void dispose() {
		Gdx.app.debug(TAG, "Disposing...");
		fonts.dispose();
		if(assetManager != null)
			assetManager.dispose();
	}
}
