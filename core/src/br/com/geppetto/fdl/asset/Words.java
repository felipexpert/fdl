package br.com.geppetto.fdl.asset;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.geppetto.utilities.screen.Initializable;

import com.badlogic.gdx.Gdx;


public class Words implements Initializable {
	public static final Words INSTANCE = new Words();
	private String[] words;
	private String[] stages;
	private boolean initialized;
	
	private Words() {}
	
	@Override
	public void init() {
		if(!initialized) {
			{ // load words
				String content = Gdx.files.internal("mp3/words.txt").readString();
				words = content.split("\n");
			}
			{ // load stages
				String content = Gdx.files.internal("mp3/stages.txt").readString();
				stages = content.split("\n");
			}
			initialized = true;
		}
	}
	
	public List<String> getWords() {
		return Arrays.asList(words);
	}
	
	public List<String> getStages() {
		return Arrays.asList(stages);
	}
	
}
