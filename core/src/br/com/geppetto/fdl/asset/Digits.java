package br.com.geppetto.fdl.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;
import br.com.geppetto.utilities.digit.NumericElementKeeper;

public class Digits implements Initializable, NumericElementKeeper {
	private final RegionWrapper digit0Wrapper = new RegionWrapper();
	private final RegionWrapper digit1Wrapper = new RegionWrapper();
	private final RegionWrapper digit2Wrapper = new RegionWrapper();
	private final RegionWrapper digit3Wrapper = new RegionWrapper();
	private final RegionWrapper digit4Wrapper = new RegionWrapper();
	private final RegionWrapper digit5Wrapper = new RegionWrapper();
	private final RegionWrapper digit6Wrapper = new RegionWrapper();
	private final RegionWrapper digit7Wrapper = new RegionWrapper();
	private final RegionWrapper digit8Wrapper = new RegionWrapper();
	private final RegionWrapper digit9Wrapper = new RegionWrapper();

	@Override
	public void init(TextureAtlas atlas) {
		digit0Wrapper.setRegion(atlas.findRegion("digit0"));
		digit1Wrapper.setRegion(atlas.findRegion("digit1"));
		digit2Wrapper.setRegion(atlas.findRegion("digit2"));
		digit3Wrapper.setRegion(atlas.findRegion("digit3"));
		digit4Wrapper.setRegion(atlas.findRegion("digit4"));
		digit5Wrapper.setRegion(atlas.findRegion("digit5"));
		digit6Wrapper.setRegion(atlas.findRegion("digit6"));
		digit7Wrapper.setRegion(atlas.findRegion("digit7"));
		digit8Wrapper.setRegion(atlas.findRegion("digit8"));
		digit9Wrapper.setRegion(atlas.findRegion("digit9"));
	}
	
	@Override
	public RegionWrapper getImgDigit(int digit) {
		RegionWrapper imgDigit = null;
		switch (digit) {
		case 1:
			imgDigit = digit1Wrapper;
			break;
		case 2:
			imgDigit = digit2Wrapper;
			break;
		case 3:
			imgDigit = digit3Wrapper;
			break;
		case 4:
			imgDigit = digit4Wrapper;
			break;
		case 5:
			imgDigit = digit5Wrapper;
			break;
		case 6:
			imgDigit = digit6Wrapper;
			break;
		case 7:
			imgDigit = digit7Wrapper;
			break;
		case 8:
			imgDigit = digit8Wrapper;
			break;
		case 9:
			imgDigit = digit9Wrapper;
			break;
		default: // zero
			imgDigit = digit0Wrapper;
			break;
		}
		return imgDigit;
	}

}
