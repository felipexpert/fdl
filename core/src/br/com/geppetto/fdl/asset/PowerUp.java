package br.com.geppetto.fdl.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;

public class PowerUp implements Initializable {
	
	public final RegionWrapper roller = new RegionWrapper();
	public final RegionWrapper slipper = new RegionWrapper();
	
	@Override
	public void init(TextureAtlas atlas) {
		roller.setRegion(atlas.findRegion("roller"));
		slipper.setRegion(atlas.findRegion("slipper"));
	}

}
