package br.com.geppetto.fdl.asset;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import br.com.geppetto.utilities.asset.MusicWrapper;
import br.com.geppetto.utilities.asset.SoundWrapper;

public class SFX {
	public final SoundWrapper walk = new SoundWrapper();
	public final SoundWrapper roller = new SoundWrapper();
	public final SoundWrapper slipper = new SoundWrapper();
	public final SoundWrapper alert = new SoundWrapper();
	public final SoundWrapper reverse = new SoundWrapper();
	public final MusicWrapper touchedQueue = new MusicWrapper();
	public final MusicWrapper newAvatar = new MusicWrapper();
	public final MusicWrapper wrongLetter = new MusicWrapper();
	
	public void init(AssetManager am) {
		walk.setSound(am.get("sfx/walk.wav", Sound.class));
		roller.setSound(am.get("sfx/roller.wav", Sound.class));
		slipper.setSound(am.get("sfx/slipper.wav", Sound.class));
		reverse.setSound(am.get("sfx/reverse.wav", Sound.class));
		alert.setSound(am.get("sfx/alert.mp3", Sound.class));
		touchedQueue.setMusic(am.get("sfx/touchedQueue.mp3", Music.class));
		newAvatar.setMusic(am.get("sfx/newAvatar.wav", Music.class));
		wrongLetter.setMusic(am.get("sfx/wrongLetter.mp3", Music.class));
	}
}
