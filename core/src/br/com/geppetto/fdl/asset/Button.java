package br.com.geppetto.fdl.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import br.com.geppetto.utilities.asset.Initializable;
import br.com.geppetto.utilities.asset.RegionWrapper;

public class Button implements Initializable{

	public final RegionWrapper btnLayer = new RegionWrapper();
	
	@Override
	public void init(TextureAtlas atlas) {
		btnLayer.setRegion(atlas.findRegion("btnLayer"));
	}

}
