package br.com.geppetto.fdl;

public class Constants {
	private Constants() {}
	
	public static final float VIEWPORT_WIDTH = 15f;
	public static final float VIEWPORT_HEIGHT = 20f;
	
	public static final float HUD_FACTOR = .125f;
	public static final float HUD_VIEWPORT_HEIGHT = 4;
	
	public static final int MAX_ACCOUNTS = 3;
	
	public static final String TEXTURE_ATLAS_NEAREST = "images/nearest.pack";
	public static final String TEXTURE_ATLAS_LINEAR = "images/linear.pack";
	public static final String TEXTURE_ATLAS_STORYBOARD = "images/storyboard.pack";
	
	public static final String TEXTURE_ATLAS_LIBGDX_UI = "images/uiskin.atlas";
	public static final String SKIN_LIBGDX_UI = "images/uiskin.json";
}
