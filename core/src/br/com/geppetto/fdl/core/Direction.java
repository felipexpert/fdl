package br.com.geppetto.fdl.core;

public enum Direction {
	NORTH(0f, 1f, 90f, false),
	EAST(1f, 0f, 0f, true),
	SOUTH(0f, -1f, 270f, false),
	WEST(-1f, 0f, 180f, true);
	
	public final float x;
	public final float y;
	public final float rotation;
	public final boolean horizontal;
	
	private Direction(float x, float y, float rotation, boolean horizontal) {
		this.x = x;
		this.y = y;
		this.rotation = rotation;
		this.horizontal = horizontal;
	}
	
	public Direction plus(int plus) {
		Direction[] values = values();
		int pos = 0;
		for(int i  = 0; i < values.length; i++) {
			if(this == values[i]) {
				pos = i;
				break;
			}
		}
		plus += values.length;
		return values[(pos + plus) % values.length];
	}
	
	public Direction oposite() {
		return plus(values().length / 2);
	}
}
