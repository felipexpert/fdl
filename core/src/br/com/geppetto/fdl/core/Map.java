package br.com.geppetto.fdl.core;

import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.utilities.screen.Initializable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Map implements Initializable{
	private Vector2 avatarSpawn;
	private Array<Vector2> lettersSpawns;
	private Array<Vector2> powerUpSpawns;
	
	private StageFactory stageFactory;
	
	public Map(StageFactory stageFactory) {
		this.stageFactory = stageFactory;
	}

	@Override
	public void init() {
		avatarSpawn = new Vector2();
		lettersSpawns = new Array<>();
		powerUpSpawns = new Array<>();
		{  // Positioning avatar
			char[][] stage = stageFactory.getStageWorld();
			for(int line = 0; line < stage.length; line++) {
				for(int col = 0; col < stage[0].length; col++) {
					float centerX = 
					(col + 1) * StageFactory.GROUND_SPACE.x + 
					StageFactory.GROUND_SPACE.x / 2f;
					float centerY = 
					(line + 1) * StageFactory.GROUND_SPACE.y + 
					StageFactory.GROUND_SPACE.y / 2f;
					if(stage[line][col] == 'O') {
						avatarSpawn.x = centerX;
						avatarSpawn.y = centerY;
					} else if(stage[line][col] == '@') {
						lettersSpawns.add(new Vector2(centerX, centerY));
					} else if(stage[line][col] == '!') {
						powerUpSpawns.add(new Vector2(centerX, centerY));
					}
				}
			}
		}
	}

	public Vector2 getAvatarSpawn() {
		return avatarSpawn;
	}

	public Array<Vector2> getLettersSpawns() {
		return lettersSpawns;
	}

	public Array<Vector2> getPowerUpSpawns() {
		return powerUpSpawns;
	}
}
