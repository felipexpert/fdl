package br.com.geppetto.fdl.core.stage;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import br.com.geppetto.fdl.core.stage.storyboard.Storyboard;
import br.com.geppetto.fdl.core.stage.storyboard.StoryboardPage;
import br.com.geppetto.fdl.persistence.UserAccount;
import br.com.geppetto.fdl.persistence.UserPersistence;
import br.com.geppetto.fdl.stage.Stage;
import br.com.geppetto.fdl.stage.Time;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.asset.MusicWrapper;

public class StageUtilities {
	public static final StageUtilities INSTANCE = new StageUtilities();
	private Map<Integer, StageFactory> stages;
	private Map<Integer, Storyboard> storyboards;
	
	private StageUtilities() {
		init();
	}
	
	private void init() {
		stages = new TreeMap<>();
		storyboards = new TreeMap<>();
		initStage(1, BemVindoStage.class);
		initStage(2, TransitoStage.class);
		initStage(3, AmigosStage.class);
		initStage(4, CidadeStage.class);
		initStage(5, PonteStage.class);
		initStage(6, SerralheiriaStage.class);
		initStage(7, GeometriaStage.class);
		initStage(8, TesouroStage.class);
		initStage(9, SaudeStage.class);
		initStage(10, LazerStage.class);
	}
	
	private void initStage(int code, Class<? extends StageFactory> stage) {
		try {
			StageFactory sf = stage.newInstance();
			stages.put(code, sf);
			if(sf instanceof Storyboard) {
				storyboards.put(code, (Storyboard)sf);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	public Set<Integer> getStageCodes() {
		return stages.keySet();
	}
	
	public Set<Integer> getStoryboardsCodes() {
		return storyboards.keySet();
	}
	
	public StageFactory getStageFactory(Integer code) {
		return stages.get(code);
	}
	
	public MusicWrapper getBGM(Integer code) {
		return stages.get(code).getBGM();
	}
	
	public List<StoryboardPage> getStoryPages(Integer code) {
		return storyboards.get(code).getPages();
	}
	
	public Sprite getThumbNail(Integer code) {
		return stages.get(code).getThumbnail();
	}
	
	public boolean isLastStage(int currStage) {
		return currStage == stages.size();
	}
	
	public void update(int currStage, int seconds, int words, int letters) {
		UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
		Stage stage = user.getStage(currStage);
				
		stage.addWords(words);
		int minutes = seconds / 60;
		stage.calculateBestScore(Time.getTime(minutes), letters);
			
		user.addLetters(letters);
		UserPersistence.INSTANCE.save();
	}
	
	public int getPointsToNext(int currStage) {
		if(currStage == stages.size()) return 0;
		UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
		Stage stage = user.getStage(currStage);
		
		return stages.get(currStage + 1).getValue() - stage.getWords();
	}
	
	public Set<Integer> getOpenedStages() {
		Set<Integer> opened = new TreeSet<>();
		opened.add(1);
		UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
		for(Integer code : stages.keySet()) {
			// break for case stage is the last (remember here code starts from 1)
			if(code.intValue() == stages.size()) break;
			Stage stage = user.getStage(code);	
			if(stage.getWords() >= stages.get(code + 1).getValue()) {
				opened.add(code + 1);
			}
		}
		return opened;
	}
	
	public String getStageName(int currStage) {
		return stages.get(currStage).getName();
	}
	
	public int getBestScore(int stageCode, int seconds) {
		UserAccount user = UserPersistence.INSTANCE.getLastUserAccount();
		Stage stage = user.getStage(stageCode);
		int minutes = seconds / 60;
		return stage.getBestScore(Time.getTime(minutes));
	}
}
