package br.com.geppetto.fdl.core.stage;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.storyboard.Storyboard;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.helper.DrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;

public class PonteStage extends StageFactory {
	private final DrawableHelper wallDH = new NonAnimatedDrawableHelper(Assets.INSTANCE.stages.ponteWall);
	private final DrawableHelper groundDH = new NonAnimatedDrawableHelper(Assets.INSTANCE.stages.ponteGround);
		
	@Override
	public CollidableSprite buildWall() {
		return Factory.INSTANCE.buildDefaultWall(wallDH);
	}

	@Override
	public Sprite buildGround() {
		return Factory.INSTANCE.buildGround(groundDH);
	}

	@Override
	public void updateGroundDH(float delta) {
		groundDH.addTime(delta);
	}

	@Override
	public void updateWallDH(float delta) {
		wallDH.addTime(delta);
	}

	@Override
	public String getName() {
		return "Ponte";
	}

	@Override
	public Sprite getThumbnail() {
		return Factory.INSTANCE.buildThumbnail
				(Assets.INSTANCE.stages.ponteThumbnail);
	}
	
	@Override
	public int getValue() {
		return 90;
	}
	
}
