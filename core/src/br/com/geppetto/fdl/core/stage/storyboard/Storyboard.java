package br.com.geppetto.fdl.core.stage.storyboard;

import java.util.List;

public interface Storyboard {
	List<StoryboardPage> getPages();
}
