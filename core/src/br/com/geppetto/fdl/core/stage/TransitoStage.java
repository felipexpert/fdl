package br.com.geppetto.fdl.core.stage;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.helper.AnimatedDrawableHelper;
import br.com.geppetto.utilities.helper.DrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class TransitoStage extends StageFactory {
	private final DrawableHelper groundDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.stages.transitoGround);
	private final DrawableHelper wallDH =
			new AnimatedDrawableHelper(Assets.INSTANCE.stages.transitoWall);
	public static final Vector2 WALL_IWS = new Vector2(1f, 2.2f);
	public static final Vector2 WALL_IMG_OFFSET = new Vector2(0f, .6f);
	@Override
	public CollidableSprite buildWall() {
		return new CollidableSprite(new Rectangle(0f, 0f, StageFactory.WALL_WIDTH, StageFactory.WALL_HEIGHT),
				WALL_IMG_OFFSET, wallDH,
				WALL_IWS, _0X0, _1X1);
	}

	@Override
	public Sprite buildGround() {
		return Factory.INSTANCE.buildGround(groundDH);
	}

	@Override
	public void updateGroundDH(float delta) {
		groundDH.addTime(delta);
	}

	@Override
	public void updateWallDH(float delta) {
		wallDH.addTime(delta);
	}
	
	@Override
	public String getName() {
		return "Trânsito";
	}

	@Override
	public Sprite getThumbnail() {
		return Factory.INSTANCE.buildThumbnail
				(Assets.INSTANCE.stages.transitoThumbnail);
	}
	
	@Override
	public int getValue() {
		return 30;
	}
}
