package br.com.geppetto.fdl.core.stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.levelloader.LevelLoader;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.asset.MusicWrapper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

/**
 * ' ' --> empty space
 * # --> Walls
 * O --> Avatar spawn
 * @ --> Letter spawn
 * ! --> Mobile Item spawn
 * @author Felipe
 *
 */
public abstract class StageFactory {
	
	public static final Vector2 _1X1 = new Vector2(1f, 1f);
	public static final Vector2 _0X0 = new Vector2(0f, 0f);
	public static final Vector2 GROUND_SPACE = new Vector2(1f, 1f);
	public static final Vector2 GROUND_OFFSET = new Vector2(0.03125f, 0.03125f);
	public static final Vector2 GROUND_IWS = new Vector2(GROUND_SPACE.x * 4f + GROUND_OFFSET.x * 2f, 
			GROUND_SPACE.y * 4f + GROUND_OFFSET.y * 2f);
	public static final float WALL_WIDTH = GROUND_SPACE.x;
	public static final float WALL_HEIGHT = GROUND_SPACE.y;
	
	public static final Vector2 DEFAULT_WALL_IWS = new Vector2(1f, 2f);
	public static final Vector2 DEFAULT_WALL_IMG_OFFSET = new Vector2(0f, .5f);
	
	private char[][] stage;
	private List<String> words;
	private List<String> takenWords = new ArrayList<>();
	public char[][] getStageWorld() {
		if(stage == null) {
			stage = LevelLoader.INSTANCE.loadStage(this.getClass().getSimpleName());
		}
		return stage;
	}
	
	public MusicWrapper getBGM() {
		return Assets.INSTANCE.mp3.getStage(this.getClass().getSimpleName());
	}
	
	public List<String> getWords() {
		if(words == null)
			initWords();
		return words;
	}
	
	public String newWord() {
		if(words == null) {
			initWords();
		}
		if(takenWords.containsAll(words)) {
			String lastWord = takenWords.get(takenWords.size() - 1);
			takenWords.clear();
			takenWords.add(lastWord);
		}
		String random = null;
		A:
		while(true) {
			random = words.get((int) (Math.random() * words.size()));
			for(String taken : takenWords) {
				if(taken.equals(random)) continue A;
			}
			break A;
		}
		takenWords.add(random);
		return random;
	}
	
	public String newWord(String lastWord) {
		if(words == null) {
			initWords();
		}
		String newWord = null;
		do {
			newWord = words.get((int) (Math.random() * words.size()));
		} while(lastWord.equals(newWord));
		return newWord;
	}
	
	private void initWords() {
		FileHandle fh = Gdx.files.internal("levels/" + this.getClass().getSimpleName() + ".txt");
		String text = fh.readString();
		Scanner scan = new Scanner(text);
		words = new ArrayList<>();
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			words.add(line);
		}
		scan.close();
	}
	
	public abstract CollidableSprite buildWall();
	public abstract Sprite buildGround();
	public abstract void updateGroundDH(float delta);
	public abstract void updateWallDH(float delta);
	public abstract String getName();
	public abstract Sprite getThumbnail();
	public abstract int getValue();
}
