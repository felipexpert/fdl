package br.com.geppetto.fdl.core.stage;

import java.util.ArrayList;
import java.util.List;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.asset.storyboard.StoryboardAssets;
import br.com.geppetto.fdl.core.levelloader.LevelLoader;
import br.com.geppetto.fdl.core.stage.storyboard.Storyboard;
import br.com.geppetto.fdl.core.stage.storyboard.StoryboardPage;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.asset.RegionWrapper;
import br.com.geppetto.utilities.helper.DrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class BemVindoStage extends StageFactory implements Storyboard {
	private final DrawableHelper groundDH = 
			new NonAnimatedDrawableHelper(Assets.INSTANCE.stages.bemVindoGround);
	private final DrawableHelper wallDH =
			new NonAnimatedDrawableHelper(Assets.INSTANCE.stages.bemVindoWall);	
	
	@Override
	public CollidableSprite buildWall() {
		return Factory.INSTANCE.buildDefaultWall(wallDH);
	}

	@Override
	public Sprite buildGround() {
		return Factory.INSTANCE.buildGround(groundDH);
	}

	@Override
	public void updateGroundDH(float delta) {
		groundDH.addTime(delta);
	}

	@Override
	public void updateWallDH(float delta) {
		wallDH.addTime(delta);
	}
	
	@Override
	public String getName() {
		return "Bem-vindo";
	}

	@Override
	public Sprite getThumbnail() {
		return Factory.INSTANCE.buildThumbnail
				(Assets.INSTANCE.stages.bemVindoThumbnail);
	}
	
	@Override
	public int getValue() {
		return 0;
	}

	@Override
	public List<StoryboardPage> getPages() {
		List<StoryboardPage> pages = new ArrayList<>();
		pages = new ArrayList<>();
		RegionWrapper[] regions = StoryboardAssets.INSTANCE.storyboard.story1;
		StoryboardPage p1 = Factory.INSTANCE.buildPage(regions[0], 
			"Felipe: Olá Camila, como vai? Vamos chamar as crianças para "
					+ "começar a aula??\n\n"
					+ "Camila: Oi Felipe, vou bem... Vamos sim, hoje vamos falar "
					+ "sobre os objetos escolares");
		StoryboardPage p2 = Factory.INSTANCE.buildPage(regions[1],
			"Felipe/Camila: Organizem-se crianças...");
		StoryboardPage p3 = Factory.INSTANCE.buildPage(regions[2],
			"Crianças: BOM DIA QUERIDA PROFESSORA!!");
		pages.add(p1); pages.add(p2); pages.add(p3);
		return pages;
	}
}
