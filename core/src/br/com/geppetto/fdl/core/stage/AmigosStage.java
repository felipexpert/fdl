package br.com.geppetto.fdl.core.stage;

import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.storyboard.Storyboard;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.CollidableSprite;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.helper.AnimatedDrawableHelper;
import br.com.geppetto.utilities.helper.DrawableHelper;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;

public class AmigosStage extends StageFactory {
	private final DrawableHelper wallDH = new AnimatedDrawableHelper(Assets.INSTANCE.stages.amigosWall);
	private final DrawableHelper groundDH = new AnimatedDrawableHelper(Assets.INSTANCE.stages.amigosGround);

	@Override
	public CollidableSprite buildWall() {
		return Factory.INSTANCE.buildDefaultWall(wallDH);
	}

	@Override
	public Sprite buildGround() {
		return Factory.INSTANCE.buildGround(groundDH);
	}

	@Override
	public void updateGroundDH(float delta) {
		groundDH.addTime(delta);
	}

	@Override
	public void updateWallDH(float delta) {
		wallDH.addTime(delta);
	}

	@Override
	public String getName() {
		return "Amigos";
	}

	@Override
	public Sprite getThumbnail() {
		return Factory.INSTANCE.buildThumbnail
				(Assets.INSTANCE.stages.amigosThumbnail);
	}
	
	@Override
	public int getValue() {
		return 60;
	}
}
