package br.com.geppetto.fdl.core.stage.storyboard;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.Initializable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StoryboardPage implements Renderable, Initializable{
	private Sprite page;
	private Label label;
	private String text;
	public StoryboardPage() {}
	
	public StoryboardPage(Sprite page, String text) {
		this.page = page;
		this.text = text;
	}
	
	@Override
	public void init() {
		label = Factory.INSTANCE.buildStoryboardLabel(text, Constants.VIEWPORT_WIDTH - 1f);
		label.setPosition(.5f, .5f);
	}

	public Sprite getPage() {
		return page;
	}

	public void setPage(Sprite page) {
		this.page = page;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public void render(SpriteBatch batch) {
		page.render(batch);
		label.render(batch);
	}
	
	public void renderPic(SpriteBatch batch) {
		page.render(batch);
	}
	
	public void renderText(SpriteBatch batch) {
		label.render(batch);
	}
}
