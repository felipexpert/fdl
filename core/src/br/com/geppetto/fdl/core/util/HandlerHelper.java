package br.com.geppetto.fdl.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.core.Direction;
import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.SymbolicControll4Buttons;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.clickHandler.ClickAt;
import br.com.geppetto.utilities.clickHandler.ClickAtHandler;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.updatable.Updatable;

public class HandlerHelper extends InputAdapter implements Initializable, Updatable, ClickAt {
	
	private ClickAtHandler clickAtHandler;
	private Avatar avatar;
	private CameraHelper cameraHelper;
	
	private SymbolicControll symbolicControll;
	
	private boolean fourButtons;
	
	public HandlerHelper() {}
	
	public HandlerHelper(Avatar avatar, CameraHelper cameraHelper, SymbolicControll symbolicControll) {
		this.avatar = avatar;
		this.cameraHelper = cameraHelper;
		setSymbolicControll(symbolicControll);
	}
	
	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}



	public void setCameraHelper(CameraHelper cameraHelper) {
		this.cameraHelper = cameraHelper;
	}



	public void setSymbolicControll(SymbolicControll symbolicControll) {
		fourButtons = symbolicControll instanceof SymbolicControll4Buttons;
		this.symbolicControll = symbolicControll;
	}



	@Override
	public void init() {
		clickAtHandler = new ClickAtHandler(this);
	}

	@Override
	public void update(float delta) {
		clickAtHandler.update();
		handleDebugInput(delta);
	}

	@Override
	public void clickAt(float x, float y) {
		if(symbolicControll.clickedOnUp(x, y))
			avatar.turn(Direction.NORTH);
		else if(symbolicControll.clickedOnDown(x, y))
			avatar.turn(Direction.SOUTH);
		else if(symbolicControll.clickedOnLeft(x, y)) {
			if(fourButtons)
				avatar.turn(Direction.WEST);
			else
				avatar.turnLeft();
		}
		else if(symbolicControll.clickedOnRight(x, y)) {
			if(fourButtons)
				avatar.turn(Direction.EAST);
			else
				avatar.turnRight();
		} else if(symbolicControll.clickedOnZoomIn(x, y))
			zoomIn(-.125f);
		else if(symbolicControll.clickOnZoomOut(x, y))
			zoomOut(.125f);
		else if(symbolicControll.clickOnZoomDefault(x, y))
			zoomDefault();
		else if(symbolicControll.clickedOnPause(x, y)) 
			pause();
		else if(symbolicControll.clickedOnClose(x, y))
			close();
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if (Gdx.app.getType() != ApplicationType.Desktop) return false;
		switch(keycode) {
		case Keys.Z:
			avatar.turnLeft();
			break;
		case Keys.X:
			avatar.turnRight();
			break;
		case Keys.A:
			avatar.turn(Direction.WEST);
			break;
		case Keys.D:
			avatar.turn(Direction.EAST);
			break;
		case Keys.S:
			avatar.turn(Direction.SOUTH);
			break;
		case Keys.W:
			avatar.turn(Direction.NORTH);
			break;
		case Keys.SEMICOLON:
			zoomDefault();
			break;
		case Keys.ENTER:
			if(cameraHelper.hasTarget(avatar.getSprite()))
				cameraHelper.setTarget(null);
			else
				cameraHelper.setTarget(avatar.getSprite());
			break;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case Keys.ESCAPE:
		case Keys.BACK:
			pause();
			break;
		}
		return false;
	}
	
	private void handleDebugInput(float deltaTime) {
		if (Gdx.app.getType() != ApplicationType.Desktop) return;
		
		if(Gdx.input.isKeyPressed(Keys.COMMA)) {
			zoomIn(-deltaTime);
		} else if(Gdx.input.isKeyPressed(Keys.PERIOD)) {
			zoomOut(deltaTime);
		}
		
		if (!cameraHelper.hasTarget()) {
			// Camera Controls (move)
			float camMoveSpeed = 5f * deltaTime;
			float camMoveSpeedAccelerationFactor = 5f;
			float addX = 0f;
			float addY = 0f;
			if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) { 
				camMoveSpeed *= camMoveSpeedAccelerationFactor;
			}		
			if(Gdx.input.isKeyPressed(Keys.UP)) {
				addY = camMoveSpeed;
			} else if(Gdx.input.isKeyPressed(Keys.DOWN)) {
				addY = -camMoveSpeed;
			} 
			if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
				addX = camMoveSpeed;
			} else if(Gdx.input.isKeyPressed(Keys.LEFT)) {
				addX = -camMoveSpeed;
			}
			if(addX != 0f || addY != 0f)
				cameraHelper.addPosition(addX, addY);
		}
	}
	
	private void zoomIn(float amount) {
		cameraHelper.addZoom(amount);
		symbolicControll.zoomIn();
	}
	
	private void zoomOut(float amount) {
		cameraHelper.addZoom(amount);
		symbolicControll.zoomOut();
	}
	
	private void zoomDefault() {
		cameraHelper.setZoom(1f);
		symbolicControll.zoomDefault();
	}
	
	private void pause() {
		String text = "O jogo está pausado...\n\nVocê deseja voltar ao menu de ESTÁGIOS ou RESUMIR?";
		ScreenManager.INSTANCE.setConfirmPopUpScreen(text,
				"ESTÁGIOS", 
				new Function() {
					@Override
					public void run(Clickable source) {
						ScreenManager.INSTANCE.setStageScreen(avatar.getAvatarCode(), false);
					}
				},
				"RESUMIR", 
				Function.NULL_FUNCTION);
	}
	
	private void close() {
		String text = "Deseja realmente sair de FILA das LETRAS?";
		ScreenManager.INSTANCE.setConfirmPopUpScreen(text,
				"CANCELAR!", 
				Function.NULL_FUNCTION,
				"SAIR", 
				new Function() {
					@Override
					public void run(Clickable source) {
						Gdx.app.exit();
					}
				});
	}
}
