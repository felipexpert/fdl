package br.com.geppetto.fdl.core.util;

import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;

public class HandlerHelperPresentation extends HandlerHelper{

	public HandlerHelperPresentation() {
		super();
	}

	@Override
	public void clickAt(float x, float y) {}
	
	@Override
	public void update(float delta) {}
	
	@Override
	public boolean keyUp(int keycode) {return false;}
	
	@Override
	public boolean keyDown(int keycode) {return false;}
}
