package br.com.geppetto.fdl.core.util;

import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.screen.Initializable;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class CameraHelper implements Initializable{
	private final float MAX_ZOOM_IN = 0.25f;
	private final float MAX_ZOOM_OUT = 5f;
	private final float FOLLOW_SPEED = 4.0f;

	private OrthographicCamera camera;
	private Vector2 position;
	private Sprite target;
	private Vector2 targetCenter;
	private float maxX;
	private float maxY;
	
	private float zoom;
	
	public CameraHelper(OrthographicCamera camera, float maxX, float maxY) {
		this(camera, null, maxX, maxY);
	}
	
	public CameraHelper(OrthographicCamera camera, Sprite target, float maxX, float maxY) {
		this.camera = camera;
		this.target = target;
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	@Override
	public void init() {
		zoom = .875f;
		targetCenter = new Vector2();
		position = new Vector2(maxX / 2f, maxY / 2f);
		forceInsideViewport();
		apply();
	}

	public void update (float deltaTime) {
		if(target == null)
			return;
		targetCenter.x = target.getCenterX();
		targetCenter.y = target.getCenterY();
		position.lerp(targetCenter, FOLLOW_SPEED * deltaTime);
		forceInsideViewport();
	}
	
	public float getPositionX() {
		return position.x;
	}
	
	public float getPositionY() {
		return position.y;
	}
	
	public void setPosition(float x, float y) {
		if(target != null)
			return;
		position.x = x; position.y = y;
		forceInsideViewport();
	}
	
	public void addPosition(float x, float y) {
		setPosition(position.x + x, position.y + y);
	}
	
	public boolean hasTarget() {
		return target != null;
	}
	
	public boolean hasTarget(Sprite sprite) {
		return target == sprite;
	}
	
	public void setTarget(Sprite target) {
		this.target = target;
	}
	
	public Sprite getTarget() {
		return target;
	}

	public void addZoom (float amount) {
		setZoom(camera.zoom + amount);
	}

	public void setZoom (float zoom) {
		float maxZoomOutX = maxX / camera.viewportWidth;
		float maxZoomOutY = maxY / camera.viewportHeight;
		this.zoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, Math.min(Math.min(maxZoomOutX, maxZoomOutY), MAX_ZOOM_OUT));
		forceInsideViewport();
	}

	public float getZoom () {
		return camera.zoom;
	}
	
	private void forceInsideViewport() {
		// Prevent camera from moving too far
		float viewportWidth = camera.viewportWidth * zoom; 
		if(viewportWidth <= maxX) {
			float halfCameraViewportWidth = viewportWidth / 2f;
			position.x = MathUtils.clamp(position.x, halfCameraViewportWidth, maxX - halfCameraViewportWidth);
		}
		float viewportHeight = camera.viewportHeight * zoom; 
		if(viewportHeight <= maxY) {
			float halfCameraViewportHeight = viewportHeight / 2f;
			position.y = MathUtils.clamp(position.y, halfCameraViewportHeight, maxY - halfCameraViewportHeight);
		}
	}

	public void apply() {
		camera.position.x = position.x;
		camera.position.y = position.y;
		camera.zoom = zoom;
		camera.update();
	}

}
