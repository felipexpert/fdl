package br.com.geppetto.fdl.core.util;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.gameObjects.letter.LettersController;
import br.com.geppetto.fdl.screen.ScreenManager;
import br.com.geppetto.utilities.Renderable;
import br.com.geppetto.utilities.clickable.Clickable;
import br.com.geppetto.utilities.clickable.Function;
import br.com.geppetto.utilities.digit.Digits;
import br.com.geppetto.utilities.menuObjects.Label;
import br.com.geppetto.utilities.screen.Initializable;
import br.com.geppetto.utilities.updatable.Updatable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.StringBuilder;

public class Timer implements Initializable, Updatable, Renderable {
	private static final String TAG = Timer.class.getSimpleName();
	
	private Initializable initializable;
	
	private int stageCode;
	private int avatarCode;
	private final int initialTime;
	private float time;
	private Digits seconds;
	private Label label;
	private boolean completed;
	
	private LettersController lettersController;
	
	
	public Timer(int initialTime, LettersController lettersController, int stageCode, int avatarCode, Initializable initializable) {
		this.initialTime = initialTime;
		this.lettersController = lettersController;
		this.stageCode = stageCode;
		this.avatarCode = avatarCode;
		this.initializable = initializable;
	}

	@Override
	public void init() {
		time = initialTime;
		seconds = Factory.INSTANCE.buildTimerDigits();
		seconds.updateByRightDownCorner(initialTime,
				Constants.VIEWPORT_WIDTH - .55f, 
				.6f);
		label = new Label(Assets.INSTANCE.fonts.buttonBig, 
				"Tempo", 
				0, 0, 10f);
		label.setPosition(Constants.VIEWPORT_WIDTH - (label.getWidth() + .5f), 
				Constants.HUD_VIEWPORT_HEIGHT - (label.getHeight() + .5f));
		label.setColor(.125f, .125f, .375f, 1f);
	}

	@Override
	public void update(float delta) {
		if(completed) return;
		time -= delta;
		
		if(time <= 0f) {
			end();
		}
		
		seconds.arrangeDigits((int)time);
	}
	
	private void end() {
		Gdx.app.log(TAG, "Timer has come to end!");
		int oldBestScore = StageUtilities.INSTANCE.getBestScore(stageCode, initialTime);
		StageUtilities.INSTANCE.update(stageCode, initialTime, lettersController.getCompletedWords(), lettersController.getTakenLetters());
		String stageName = StageUtilities.INSTANCE.getStageName(stageCode);
		boolean isLast = StageUtilities.INSTANCE.isLastStage(stageCode);
		int left = StageUtilities.INSTANCE.getPointsToNext(stageCode);
		boolean hasOpenedNewStage = !isLast && left <= 0;
		int bestScore = StageUtilities.INSTANCE.getBestScore(stageCode, initialTime);
		completed = true;
		StringBuilder sb = new StringBuilder();
		String sec = Integer.toString(initialTime % 60);
		if(sec.length() == 1) sec = '0' + sec;
		sb.append("Fase ").append(stageName).append("\n")
		.append("Modalidade de tempo ").append(initialTime / 60).append(':').append(sec).append("\n\n");
		sb.append("Letras obtidas ").append(lettersController.getTakenLetters());
		if(lettersController.getTakenLetters() < bestScore) {
			sb.append(" (seu melhor foi ").append(bestScore).
			append(" letras)\n\n");
		} else if(lettersController.getTakenLetters() > oldBestScore) {
			sb.append(" (este é o máximo de letras que você já obteve!)\n\n");
		} else {
			sb.append("\n\n");
		}
		sb.append("Palavras formadas ").append(lettersController.getCompletedWords());
		if(hasOpenedNewStage) {
			sb.append("\n\nParabéns! Você abriu um novo estágio.");
		} else if(!isLast) {
			sb.append("\n(Faltam ").append(left).append(" palavra")
			.append((left > 1 ? "s" : "")).append(" para abrir o próximo estágio)");
		}
		sb.append('\n').append('\n');
		sb.append("Voltar ao menu de Estágios ou jogar NOVAMENTE?");
		ScreenManager.INSTANCE.setConfirmPopUpScreen(sb.toString(), 
				"ESTÁGIOS", 
				new Function() {
					@Override
					public void run(Clickable source) {
						ScreenManager.INSTANCE.setStageScreen(avatarCode, false);
					}
				},
				"NOVAMENTE", 
				new Function() {
					@Override
					public void run(Clickable source) {
						initializable.init();
					}
				});
	}

	@Override
	public void render(SpriteBatch batch) {
		label.render(batch);
		seconds.render(batch);
	}
}
