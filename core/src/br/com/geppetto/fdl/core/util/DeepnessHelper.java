package br.com.geppetto.fdl.core.util;


import br.com.geppetto.utilities.CoordnateAnalyzable;
import br.com.geppetto.utilities.Renderable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class DeepnessHelper implements Renderable {
	private Array<CoordnateAnalyzable> analyzables = new Array<>();
	@Override
	public void render(SpriteBatch batch) {
		for(CoordnateAnalyzable s : analyzables)
			s.render(batch);
	}
	
	private void arrange() {
		float previousY = 99999f;
		for(int i = 0; i < analyzables.size; i++) {
			float iY = analyzables.get(i).getY(); 
			if(iY > previousY) {
				CoordnateAnalyzable removed = analyzables.removeIndex(i);
				arrangeAnalyzable(removed);
				previousY = analyzables.get(i).getY();
			} else {
				previousY = iY;
			}
		}
	}
	
	private void arrangeAnalyzable(CoordnateAnalyzable sprite) {
		for(int i = 0; i < analyzables.size; i++) {
			if(sprite.getY() > analyzables.get(i).getY()) {
				analyzables.insert(i, sprite);
				return;
			}
		}
		analyzables.add(sprite);
	}
	
	public void addAnalybable(CoordnateAnalyzable analyzable) {
		analyzables.add(analyzable);
	}
	
	public void removeAnalyzable(CoordnateAnalyzable analyzable) {
		analyzables.removeValue(analyzable, true);
	}
	
	public boolean hasAnalyzable(CoordnateAnalyzable analyzable) {
		return analyzables.contains(analyzable, true);
	}

	public void update() {
		arrange();
	}
}
