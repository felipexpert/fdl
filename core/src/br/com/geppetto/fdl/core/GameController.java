package br.com.geppetto.fdl.core;

import br.com.geppetto.fdl.Constants;
import br.com.geppetto.fdl.asset.Assets;
import br.com.geppetto.fdl.core.stage.StageFactory;
import br.com.geppetto.fdl.core.stage.StageUtilities;
import br.com.geppetto.fdl.core.util.CameraHelper;
import br.com.geppetto.fdl.core.util.DeepnessHelper;
import br.com.geppetto.fdl.core.util.HandlerHelper;
import br.com.geppetto.fdl.core.util.Timer;
import br.com.geppetto.fdl.gameObjects.Grounds;
import br.com.geppetto.fdl.gameObjects.SymbolicControll;
import br.com.geppetto.fdl.gameObjects.SymbolicControll4Buttons;
import br.com.geppetto.fdl.gameObjects.Walls;
import br.com.geppetto.fdl.gameObjects.avatar.Avatar;
import br.com.geppetto.fdl.gameObjects.factory.Factory;
import br.com.geppetto.fdl.gameObjects.letter.Letter;
import br.com.geppetto.fdl.gameObjects.letter.LettersController;
import br.com.geppetto.fdl.gameObjects.mobile.PowerUpController;
import br.com.geppetto.fdl.persistence.ConfigPersistence;
import br.com.geppetto.utilities.Sprite;
import br.com.geppetto.utilities.helper.NonAnimatedDrawableHelper;
import br.com.geppetto.utilities.screen.templates.GenericHUDGameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class GameController implements GenericHUDGameController {
	private static final String TAG = GameController.class.getSimpleName();

	private OrthographicCamera camera;
	private int stageCode;
	private StageFactory stageFactory;
	private int seconds;
	private Timer timer;
	private DeepnessHelper deepnessHelper;
	private int avatarCode;
	private Avatar avatar;
	private LettersController lettersController;
	private Walls walls;
	private Grounds grounds;
	private PowerUpController powerUpController;
	private CameraHelper cameraHelper;
	private final HandlerHelper handlerHelper;
	
	private Sprite HUDBackground;
	private SymbolicControll symbolicControll;
	
	public GameController(int stageCode, int seconds, int avatarCode) {
		this.stageCode = stageCode;
		this.avatarCode = avatarCode;
		this.seconds = seconds;
		handlerHelper = buildHandlerHelper();
	}
	
	protected Avatar buildAvatar(int avatarCode, DeepnessHelper deepnessHelper, Map map, SymbolicControll symbolicControll) {
		return new Avatar(avatarCode, deepnessHelper, map, symbolicControll);
	}
	
	protected void setCameraHelperTarget() {
		cameraHelper.setTarget(avatar.getSprite());
	}
	
	protected void randomTarget() {
		cameraHelper.setTarget(lettersController.getLetters().random().getCollidableLetter().getSprite());
	}
	
	protected SymbolicControll buildSymbolicControll() {
		if(ConfigPersistence.INSTANCE.isFourButtons())
			return new SymbolicControll4Buttons();
		else
			return new SymbolicControll();
	}
	
	protected HandlerHelper buildHandlerHelper() {
		return new HandlerHelper();
	}
	
	@Override
	public void init() {
		stageFactory = StageUtilities.INSTANCE.getStageFactory(stageCode);
		deepnessHelper = new DeepnessHelper();
		Map map = new Map(stageFactory);
		map.init();
		symbolicControll = buildSymbolicControll();
		symbolicControll.init();
		avatar = buildAvatar(avatarCode, deepnessHelper, map, symbolicControll);
		avatar.init();
		powerUpController = new PowerUpController(avatar, map);
		powerUpController.init();
		lettersController = new LettersController(stageFactory, avatar, map, deepnessHelper);
		avatar.setLettersController(lettersController);
		lettersController.init();
		walls = new Walls(stageFactory, deepnessHelper, avatar, lettersController);
		walls.init();
		grounds = new Grounds(stageFactory);
		grounds.init();
		float maxX = (stageFactory.getStageWorld()[0].length + 2) * 
				StageFactory.GROUND_SPACE.x;
		float maxY = (stageFactory.getStageWorld().length + 3) *
				StageFactory.GROUND_SPACE.y;
		cameraHelper = new CameraHelper(camera, maxX, maxY);
		cameraHelper.init();
		
		handlerHelper.setAvatar(avatar);
		handlerHelper.setCameraHelper(cameraHelper);
		handlerHelper.setSymbolicControll(symbolicControll);
		handlerHelper.init();
		
		setCameraHelperTarget();
		timer = new Timer(seconds, lettersController, stageCode, avatarCode, this);
		timer.init();
		Gdx.app.log(TAG, "GameController has been initialized");
		HUDBackground = Factory.INSTANCE.buildHUDBackground();
	}
	
	@Override
	public void render(SpriteBatch batch) {
		grounds.render(batch);
		powerUpController.render(batch);
		walls.render(batch);
		deepnessHelper.render(batch);
		walls.renderBottomEdge(batch);
	}
	
	@Override
	public void renderDebug(ShapeRenderer shapeRenderer) {
		powerUpController.renderDebug(shapeRenderer);
		walls.renderDebug(shapeRenderer);
		lettersController.renderDebug(shapeRenderer);
		avatar.renderDebug(shapeRenderer);
	}
	
	@Override
	public void renderHUD(SpriteBatch batch) {
		HUDBackground.render(batch);
		lettersController.renderHUD(batch);
		timer.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		symbolicControll.render(batch);
		lettersController.renderGUI(batch);
	}
	
	@Override
	public void setCamera(OrthographicCamera camera) {
		this.camera = camera;
	}

	@Override
	public void applyToCamera() {}

	@Override
	public void resize(float width, float height) {
		camera.viewportWidth = (Constants.VIEWPORT_HEIGHT / (float)height) * (float)width;
		camera.update();
	}

	@Override
	public void update(float delta) {
		handlerHelper.update(delta);
		avatar.update(delta);
		lettersController.update(delta);
		powerUpController.update(delta);
		grounds.update(delta);
		walls.update(delta);
		cameraHelper.update(delta);
		cameraHelper.apply();
		deepnessHelper.update();
		symbolicControll.update(delta);
		timer.update(delta);
	}
	
	@Override
	public InputProcessor getInputProcessor() {
		return handlerHelper;
	}

	@Override
	public void hide() {}
}
