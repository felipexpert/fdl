package br.com.geppetto.fdl.core.levelloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public class LevelLoader {
	private static final String TAG = LevelLoader.class.getSimpleName();
	public static final LevelLoader INSTANCE = new LevelLoader();
	
	private LevelLoader() {}
	
	public enum BLOCK_TYPE {
		REAL_EMPTY(0, 0, 0), 
		EMPTY(125, 125, 125), 
		WALL(0, 255, 0), 
		PLAYER_SPAWNPOINT(255, 255, 255), 
		ITEM_SPAWNPOINT(255, 0, 255), 
		LETTER_SPAWNPOINT(255, 255, 0);

		private int color;

		private BLOCK_TYPE(int r, int g, int b) {
			color = r << 24 | g << 16 | b << 8 | 0xff;
		}

		public boolean sameColor(int color) {
			return this.color == color;
		}

		public int getColor() {
			return color;
		}
	}

	public char[][] loadStage(String levelName) {
		Array<Array<Character>> stage = new Array<>();
		// load image file that represents the level data
		Pixmap pixmap = new Pixmap(Gdx.files.internal("levels/" + levelName + ".png"));
		// scan pixels from top-left to bottom-right
		//int lastPixel = -1;
		for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
			Array<Character> line = new Array<>();
			for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
				char c = ' ';
				// get color of current pixel as 32-bit RGBA value
				int currentPixel = pixmap.getPixel(pixelX, pixelY);
				// find matching color value to identify block type at (x,y)
				// point and create the corresponding game object if there is
				// a match
				// real-empty space
				if(BLOCK_TYPE.REAL_EMPTY.sameColor(currentPixel)) {
					continue;
				}
				// empty
				else if(BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
					c = ' ';
				}
				
				else if(BLOCK_TYPE.WALL.sameColor(currentPixel)) {
					c = '#';
				}
				
				else if(BLOCK_TYPE.LETTER_SPAWNPOINT.sameColor(currentPixel)) {
					c = '@';
				}
				
				else if(BLOCK_TYPE.ITEM_SPAWNPOINT.sameColor(currentPixel)) {
					c = '!';
				}
				
				else if(BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
					c = 'O';
				}
				// unknown object/pixel color
				else {
					int r = 0xff & (currentPixel >>> 24); // red color channel
					int g = 0xff & (currentPixel >>> 16); // green color channel
					int b = 0xff & (currentPixel >>> 8); // blue color channel
					int a = 0xff & currentPixel;
					// alpha channel
					Gdx.app.error(TAG, "Unknown object at x<" + pixelX + "> y<"
							+ pixelY + ">: r<" + r + "> g<" + g + "> b<" + b
							+ "> a<" + a + ">");
					continue;
				}
				line.add(c);
			}
			if(line.size > 0)
				stage.add(line);
		}
		
		// Transform the Array<Array<Character>> into the correct char[][]
		stage.reverse();
		char[][] finalStage = new char[stage.size][stage.get(0).size];
		for(int line = 0; line < finalStage.length; line++) {
			for(int col = 0; col < finalStage[0].length; col++) {
				finalStage[line][col] = stage.get(line).get(col);
			}
		}
		
		// free memory
		pixmap.dispose();
		Gdx.app.debug(TAG, "level '" + levelName + "' loaded");
		
		return finalStage;
	}
}
