package br.com.geppetto.fdl.stage;

import java.util.HashMap;
import java.util.Map;


public class Stage {
	private int code;
	private Map<Time, Integer> bestScores = new HashMap<>();
	private int words;
	
	public Stage() {
		bestScores.put(Time.SHORT, 0);
		bestScores.put(Time.MEDIUM, 0);
		bestScores.put(Time.LONG, 0);
	}
	
	public Stage(int code, int shortBestScore, int mediumBestScore, int longBestScore, int words) {
		this.code = code;
		bestScores.put(Time.SHORT, shortBestScore);
		bestScores.put(Time.MEDIUM, mediumBestScore);
		bestScores.put(Time.LONG, longBestScore);
		this.words = words;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	public int getShortBestScore() {
		return bestScores.get(Time.SHORT);
	}
	
	public int getMediumBestScore() {
		return bestScores.get(Time.MEDIUM);
	}
	
	public int getLongBestScore() {
		return bestScores.get(Time.LONG);
	}
	
	public void calculateBestScore(Time t, int letters) {
		bestScores.put(t, Math.max(letters, bestScores.get(t)));
	}
	
	public int getBestScore(Time time) {
		return bestScores.get(time);
	}

	public void setShortBestScore(int bestScore) {
		bestScores.put(Time.SHORT, bestScore);
	}
	
	public void setMediumBestScore(int bestScore) {
		bestScores.put(Time.MEDIUM, bestScore);
	}
	
	public void setLongBestScore(int bestScore) {
		bestScores.put(Time.LONG, bestScore);
	}
	
	public int getWords() {
		return words;
	}
	
	public void setWords(int words) {
		this.words = words;
	}
	
	public void addWords(int words) {
		this.words += words;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		if(!(obj instanceof Stage))
			return false;
		return ((Stage) obj).code == this.code;
	}
	
	@Override
	public int hashCode() {
		return Integer.valueOf(code).hashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Stage code: " + code).append("|")
		.append("|").append("bestScore:").append(bestScores.toString())
		.append("|").append("words").append(words);
		return sb.toString();
	}
}
