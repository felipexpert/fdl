package br.com.geppetto.fdl.stage;

public enum Time {
	SHORT(5), MEDIUM(10), LONG(15);
	
	public final int minutes;
	
	Time(int minutes) {
		this.minutes = minutes;
	}
	
	public static Time getTime(int minutes) {
		for(Time t : values()) {
			if(t.minutes == minutes)
				return t;
		}
		throw new IllegalArgumentException("There is no such time");
	}
}
